# Boolean Office

This is a varsity project which is been developed to minimize the extra work of an office worker. More or less this project will reduce the paper work for a department office. Like as keep track of teachers leave of absence when they will get a leave, when their application approved. Also which student took a book from the seminar library and which office file is submitted in which office and what is the status of the file whether it is approved or not.

Based on all of these this project is been separated into three part/modules-

1. [Seminar Library](Tamanna/seminar-library): Here seminar library books is been managed by which student took which book, whether they reserved the book for long time without renewing the book.

2. [Leave of Absence](Nishad/leave-of-absence): Here teachers leave of absence application will be tracked. When an application is submitted, how long a teacher is in absence, whether an application approved or not.

3. [File Tracking](Asif/file-tracking): Here department office will keep track of files which are going offices to offices. Like a file submitted from this office to other office and when the file will arrive at this office if it's approved.

By all this modules combined this project is an Office automation which is using **HTML**, **PHP**, **CSS** languages and markups for designing and developing the website. Also **MySQL** is been used as the database support.

Read more in the project [Docs](Docs/).

This project is licensed under [LGPLv2.1](LICENSE).
