# File Tracking | Module

This is a module/part of the Boolean Office project.

![File Tracking | History]()

## This module contains

* [SRS](docs/srs/)
* [Analysis](docs/analysis/)
  * [Activity Diagram](docs/analysis/activity-diagram)
  * [Use Case Diagram](docs/analysis/use-case-diagram)
  * [Use Case Description](docs/analysis/use-case-description)
* [Database Design](database-design/)
* [Source](file-tracking)

## Things that used

* [Bootstrap 4.4.1](https://getbootstrap.com/docs/4.4/getting-started/download/)
* [jQuery 3.4.1](https://jquery.com/download/)
* [jQuery-easing 1.4.1](https://github.com/gdsmith/jquery.easing)
* [MySQL Workbench 8.0 Community](https://www.mysql.com/products/workbench/)
* [EDrawMax 9.3 (Unlicensed)](https://www.edrawsoft.com/edraw-max/)
