<?php
	$title = "SignIn";

	require_once 'welcome.php';
	require_once 'header.php';

	$sessData = !empty($_SESSION['sessData']) ? $_SESSION['sessData'] : '';
	if (!empty($sessData['status']['msg'])) {
		$statusMsg = $sessData['status']['msg'];
		$statusMsgType = $sessData['status']['type'];
		unset($_SESSION['sessData']['status']);
	}
?>

	<body>
	<div class="container">
		<div class="row justify-content-center mb-3">
			<div class="col-md-4 mt-4 pt-4 pl-2 pr-3 text-justify">
				<form class="shadow-lg rounded-xl mt-3 p-5" action="welcome.php" method="POST">
					<div class="col-form-label text-center pb-4">
						<h4>Sign in to<br>File Tracking System</h4>
					</div>
					<div class="form-group form-floating">
						<input type="email" class="form-control" id="user_name" name="user_name" placeholder=""
									 autofocus required>
						<label for="user_name">User email</label>
					</div>
					<div class="form-group form-floating">
						<input type="password" class="form-control" id="user_password" name="user_password"
									 placeholder="" required>
						<label for="user_password">User password</label>
					</div>
					<div class="pb-2">
						<span class="small text-danger">
							<?php
								if (isset($_SESSION['errorval']) and !empty($_SESSION['errorval'])) {
									echo $_SESSION['errorval'];
									unset($_SESSION['errorval']);
								}
							?>
						</span>
					</div>
					<?php if (!empty($statusMsg)) { ?>
						<span class="small text-<?php echo ($statusMsgType === 'error') ? 'danger' : 'success'; ?>">
						<?php echo '<p>' . $statusMsg . '</p>'; ?>
					</span>
					<?php } ?>
					<button type="submit" class="btn btn-primary btn-block">Sign In</button>
					<div class="pt-3 pb-3">
						<a class="link" href="forgetPassword.php">Forget password?</a>
					</div>
					<div class="pt-3 pb-3">
						<a class="link" href="activateSignin.php">Activate account</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	</body>

<?php require_once('footer.php'); ?>