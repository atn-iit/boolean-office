<?php
	function createNavItem($title, $titleTxt, $linkTxt)
	{
		$activeTxt = "";
		$tmpLink = $linkTxt;

		if (isset($title)) {
			if ($title === $titleTxt) {
				$activeTxt = "active";
				$tmpLink = '';
			}
		}

		echo "
    <li class='nav-item $activeTxt'>
      <a class='nav-link' href='$tmpLink'>$titleTxt</a>
    </li>
  ";
	}
?>

<nav class="navbar navbar-expand-lg navbar-light fixed-top border-bottom bg-light">
	<div class="container">
		<a class="navbar-brand page-scroll" href="#page-top">
			<img src="res/nstulogo.gif" alt="NSTU" height="32px" width="32px"><span class="brand-name">NSTU</span>
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" 
			data-target="#navbarSupportedContent" 
			aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav ml-auto mt-2 mt-lg-0">
				<!-- Dashboard -->
				<?php createNavItem($title, "Dashboard", "index.php"); ?>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="carrierDropdown" role="button" data-toggle="dropdown"
						 aria-haspopup="true" aria-expanded="false">
						Carrier
					</a>
					<div class="dropdown-menu dropdown-menu-right rounded-xl" aria-labelledby="carrierDropdown">
						<a class="dropdown-item" href="add-carrier.php">Add new carrier</a>
						<a class="dropdown-item" href="manage-carrier.php">Manage carrier</a>
					</div>
				</li>
				<!-- Create File -->
				<?php createNavItem($title, "Create File", "create-file.php"); ?>
				<!-- Created Files -->
				<?php createNavItem($title, "Created Files", "created-files.php"); ?>
				<!-- Received Files -->
				<?php createNavItem($title, "Received Files", "received-files.php"); ?>
				<!-- Profile -->
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
						 aria-haspopup="true" aria-expanded="false">
						<!-- Show username -->
						<?php
							$isuser = isset($_SESSION['officer_name']);
							if ($isuser) {
								$user = $_SESSION['officer_name'];
								if (!empty($user)) {
									echo $user;
									unset($user);
								} else echo "Profile";
							} else echo "Profile";
							unset($isuser);
						?>
					</a>
					<div class="dropdown-menu dropdown-menu-right rounded-xl" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="signout.php">Sign Out</a>
					</div>
				</li>
			</ul>
		</div>
	</div>
</nav>