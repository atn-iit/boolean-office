<?php

	require_once 'custom-functions.php';
	require_once 'dbconn.php';

	$location = 'index.php';
	$message = '';

	if (checkIsPOSTS(['fileid', 'filepath'])) {
		// Check the file existance first
		$query = "SELECT * FROM file_info WHERE file_number=? AND file_download_path IS NOT NULL AND file_download_path=?";
		$stmt = $db->query($query, [$_POST['fileid'],$_POST['filepath']], "ss");
		$result = $stmt->get_result();
		$stmt->close();

		if ($result->num_rows === 1) {
			$file = 'uploads/' . $_POST['filepath'];
			$filename = basename($file);
			$temp = explode('.', $filename);
			$ext = end($temp);

			$supported_exts = ['xls','xlsx','csv','pdf','doc','docx','ppt','pptx'];

			if (in_array($ext, $supported_exts)) {
				if (file_exists($file)) {
					//Define header information
					header('Content-Description: File Transfer');
					header('Content-Type: application/octet-stream');
					header("Cache-Control: no-cache, must-revalidate");
					header("Expires: 0");
					header('Content-Disposition: attachment; filename="' . basename($file) . '"');
					header('Content-Length: ' . filesize($file));
					header('Pragma: public');

					//Clear system output buffer
					flush();

					//Read the size of the file
					readfile($file);

					//Terminate from the script
					$message = 'Downloading...';
				} else {
					$message = 'File does not exist.';
				}
			} else {
				$message = 'Invalid file type requested';
			}
		} else {
			$message = 'No file found with attachment';
		}
	} else {
		$message = 'Cannot download';
	}

	echo "<script>alert('$message');window.location='$location';</script>";