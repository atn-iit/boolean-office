<?php
	function createNavItem($title, $titleTxt, $linkTxt, $dropdownItems = "")
	{
		$itemTxt = '';
		$activeTxt = '';
		$tmpLink = $linkTxt;

		if (isset($title)) {
			if ($title === $titleTxt) {
				$activeTxt = "active";
				$tmpLink = '';
			}
		}

		$itemTxt .= "<li class='nav-item $activeTxt ";

		if ($dropdownItems and is_array($dropdownItems)) {
			$itemTxt .= "dropdown'><a class='nav-link dropdown-toggle' href='$tmpLink' id=navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
			$itemTxt .= $titleTxt . "</a>";
			$dropdownTxt = "<div class='dropdown-menu dropdown-menu-right' aria-labelledby='navbarDropdown'>";

			// key = link, value = text
			foreach ($dropdownItems as $key => $value) {
				$dropdownTxt .= "<a class='dropdown-item' href='$key'>$value</a>";
			}
			$dropdownTxt .= "</div>";
			$itemTxt .= $dropdownTxt;
			$itemTxt .= "</li>";
		} else {
			$itemTxt .= "'><a class='nav-link' href='$tmpLink'>$titleTxt</a></li>";
		}

		echo $itemTxt;
	}
?>

<nav class="navbar navbar-expand-lg navbar-light fixed-top border-bottom bg-light">
	<div class="container">
		<a class="navbar-brand page-scroll" href="#page-top">
			<img src="../res/nstulogo.gif" alt="NSTU" height="32px" width="32px"><span class="brand-name">NSTU</span>
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav ml-auto mt-2 mt-lg-0">
				<!-- Dashboard -->
				<?php
				$titleStr = empty($title) ? '' : $title;
				createNavItem($titleStr, "Dashboard", "index.php"); ?>
				<!-- Office -->
				<?php
				$titleStr = empty($title) ? '' : $title;
				createNavItem($titleStr, 'Office', '', ['add-office.php' => 'Add new office', 
				'manage-office.php' => 'Manage Office']); ?>
				<!-- Officer -->
				<?php
				$titleStr = empty($title) ? '' : $title;
				createNavItem($titleStr, 'Officer', '', ['add-officer.php' => 'Add new officer', 
				'manage-officer.php' => 'Manage officer']); ?>
				<!-- User -->
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false">
						<!-- Show username -->
						<?php
							$isuser = isset($_SESSION['admin_name']);
							if ($isuser) {
								$user = $_SESSION['admin_name'];
								if (!empty($user)) {
									echo $user;
									unset($user);
								} else echo "Profile";
							} else echo "Profile";
							unset($isuser);
						?>
					</a>
					<div class="dropdown-menu dropdown-menu-right rounded-xl" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="signout.php">Sign Out</a>
					</div>
				</li>
			</ul>
		</div>
	</div>
</nav>