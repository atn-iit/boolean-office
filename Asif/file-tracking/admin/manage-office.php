<?php
	$title = "Office";

	require_once 'welcome.php';
	require_once 'header.php';

	$admin_id = $_SESSION['admin_id'];
	$admin_name = $_SESSION['admin_name'];

	if (checkIsPOSTS(['office_short_name', 'action'])) {
		$sessdata = array();
		try {
			// Check office short name contains no space
			if (strpos($_POST['office_short_name'], " ") === FALSE) {
				// Check office existence
				$query = "SELECT * FROM office_info WHERE office_short_name=?";
				$stmt = $db->query($query, [$_POST['office_short_name']], "s");
				$result = $stmt->get_result();
				$stmt->close();

				if ($result->num_rows > 0) {
					$row = $result->fetch_assoc();
					$office_id = $row['idoffice_info'];
					$status = $row['office_status'];
					$action = $_POST['action'];

					if ($status === $action or ($action !== 'Inactive' and $action !== 'Active')) {
						$sessdata['type'] = 'danger';
						$sessdata['message'] = 'Can not change the office status to ' . $_POST['action'];
					} else {
						if ($action === 'Inactive') {
							// Update the office status
							$query = "UPDATE office_info SET office_status='Inactive' WHERE idoffice_info=?";
							$stmt = $db->query($query, [$office_id], "i");
							$affected_rows = $stmt->affected_rows;
							$stmt->close();

							if ($affected_rows > 0) {
								// Update the officer status to inactive and save the current status
								$query = "UPDATE officer_info SET officer_previous_status=officer_status,officer_status='0' WHERE office_info_idoffice_info=?";
								$stmt = $db->query($query, [$office_id], "i");
								$affected_rows = $stmt->affected_rows;
								$stmt->close();

								$sessdata['type'] = 'success';
								$sessdata['message'] = 'Office status updated successfully';
							} else {
								$sessdata['type'] = 'danger';
								$sessdata['message'] = 'Can not update the office status';
							}
						} else if ($action === 'Active') {
							// Update the office status
							$query = "UPDATE office_info SET office_status='Active' WHERE idoffice_info=?";
							$stmt = $db->query($query, [$office_id], "i");
							$affected_rows = $stmt->affected_rows;
							$stmt->close();

							if ($affected_rows > 0) {
								// Update the officer status to inactive and save the current status
								$query = "UPDATE officer_info SET officer_status=officer_previous_status,officer_previous_status=NULL WHERE office_info_idoffice_info=?";
								$stmt = $db->query($query, [$office_id], "i");
								$affected_rows = $stmt->affected_rows;
								$stmt->close();

								$sessdata['type'] = 'success';
								$sessdata['message'] = 'Office status updated successfully';
							} else {
								$sessdata['type'] = 'danger';
								$sessdata['message'] = 'Can not update the office status';
							}
						}
					}
				} else {
					$sessdata['type'] = 'danger';
					$sessdata['message'] = 'No office found with the selected one.';
				}
			} else {
				$sessdata['type'] = 'danger';
				$sessdata['message'] = 'Please provide a short name with no space';
			}
		} catch (Exception $ex) {
			error_log($ex->getMessage());
			$sessdata['type'] = 'danger';
			$sessdata['message'] = 'Query error!!!<br>Cannot update office status.';
		}
		$_SESSION['admin_sessdata'] = $sessdata;
		// header("Location: manage-office.php");
	}
?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
	<!-- Navigation bar -->
	<?php require_once('navbar.php'); ?>

	<!-- Body -->
	<div class="container">
		<?php
		if (checkSessionValue('admin_sessdata')) {
			$sessdata = $_SESSION['admin_sessdata'];
		?>
		<div class="row no-gutters">
			<div class="col-lg-5 col-md-12 ml-auto">
				<div class="alert alert-<?php echo $sessdata['type']; ?> alert-dismissible fade show" role="alert">
					<strong><?php echo $sessdata['message']; ?></strong>
				</div>
			</div>
		</div>
		<?php
			unset($_SESSION['admin_sessdata']);
		}
		?>

		<div class="row mb-3">
			<div class="col mt-2 pt-2 pl-2 pr-3 text-justify">
				<div class="text-center">
					<h4>Manage office</h4>
				</div>
				<form id="form-office" class="p-2 mb-5" action="" method="post">
					<input type="hidden" name="office_short_name" id="office_short_name">
					<input type="hidden" name="action" id="action">
					<div class="mt-2 mb-2 pt-2">
						<p><strong>N.B. Setting an office to inactive will inactive all it's officers.</strong></p>
					</div>
					<table id="table-office" class="table table-bordered table-condensed text-center">
						<thead class="thead-light">
							<tr>
								<th>Office name</th>
								<th>Office short name</th>
								<th>Number of active officers in the system</th>
								<th>Number of inactive officers in the system</th>
								<th>Office status</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php
							function getActiveOfficerCount($db, $office_id): string {
								try {
									$query = "SELECT count(*) FROM officer_info WHERE officer_status='1' AND office_info_idoffice_info=?";
									$stmt = $db->query($query, [$office_id], "i");
									$result = $stmt->get_result();
									$stmt->close();

									if ($result->num_rows > 0) {
										$row = $result->fetch_row();
										return $row[0];
									} else {
										return "0";
									}
								} catch (Exception $ex) {
									error_log($ex->getMessage());
									return "-1";
								}
							}

							function getInactiveOfficerCount($db, $office_id): string {
								try {
									$query = "SELECT count(*) FROM officer_info WHERE officer_status!='1' AND office_info_idoffice_info=?";
									$stmt = $db->query($query, [$office_id], "i");
									$result = $stmt->get_result();
									$stmt->close();

									if ($result->num_rows > 0) {
										$row = $result->fetch_row();
										return $row[0];
									} else {
										return "0";
									}
								} catch (Exception $ex) {
									error_log($ex->getMessage());
									return "-1";
								}
							}

							try {
								$query = "SELECT * FROM office_info";
								$stmt = $db->query($query);
								$result = $stmt->get_result();
								$stmt->close();

								if ($result->num_rows > 0) {
									while ($row = $result->fetch_assoc()) {
										echo "<tr>" . 
												"<td>" . $row['office_name'] . "</td>" .
												"<td>" . $row['office_short_name'] . "</td>" .
												"<td>" . getActiveOfficerCount($db, $row['idoffice_info']) . "</td>" .
												"<td>" . getInactiveOfficerCount($db, $row['idoffice_info']) . "</td>" .
												"<td>" . $row['office_status'] . "</td>";
										if ($row['office_status'] === "Inactive") {
											echo "<td><button type='button' name='officeSubmit' class='btn btn-secondary' onclick='postAction(\"$row[office_short_name]\",\"Active\",this)'>Set Active</button></td>";
										} else {
											echo "<td><button type='button' name='officeSubmit' class='btn btn-danger' onclick='postAction(\"$row[office_short_name]\",\"Inactive\",this)'>Set Inactive</button></td>";
										}
										echo "</tr>";
									}
							?>
							<script>
							function postAction(shortName, action, item) {
								if (shortName.trim() !== null && action.trim() !== null) {
									if (action.trim() === "Inactive") {
										if (!confirm('This will set all the officer of this office to ' + action.trim() + '\nProceed?')) {
											// item.preventDefault();
											return;
										}
									}
									$('#office_short_name').val(shortName);
									$('#action').val(action);
									$('#form-office').submit();
								}
							}
							</script>
							<?php
								} else {
									echo "<tr><td colspan='6'>No office found <a class='btn btn-primary' href='add-office.php'>Add new office</a></td></tr>";
								}
							} catch (Exception $ex) {
								error_log($ex->getMessage());
								echo "<tr><td colspan='6'>Something went wrong while fetching the office info</td></tr>";
							}
							?>
						</tbody>
					</table>
				</form>
			</div>
		</div>
	</div>
</body>

<script>
document.addEventListener('DOMContentLoaded', function() {
	let table1 = new DataTable('#table-office');
});
</script>

<?php require_once 'footer.php' ?>