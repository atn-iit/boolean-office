<?php
	$title = 'Dashboard';
	
	require_once 'welcome.php';
	require_once 'header.php';

	$admin_id = $_SESSION['admin_id'];
	$admin_name = $_SESSION['admin_name'];

?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
	<?php 
	require_once('navbar.php'); 

	function getData($db, $query): string
	{
		try {
			$stmt = $db->query($query);
			$result = $stmt->get_result();
			$stmt->close();

			if ($result->num_rows > 0) {
				$row = $result->fetch_row();
				return $row[0];
			} else {
				return "0";
			}
		} catch (Exception $ex) {
			error_log($ex->getMessage());
			return "-1";
		}
	}
	?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="mt-1 pt-1 pl-2 pr-3">
				<div class="row">
					<h2 class="col text-center">Dashboard</h2>
				</div>

				<div class="row mt-4 mb-3">
					<div class="card-deck mb-3">
						<div class="card w-100 shadow-lg rounded-xl text-white bg-dark text-center">
							<div class="card-body border-0 mt-2 mb-n3">
								<h5 class="pb-2">
									<?php
									$query = "SELECT count(*) FROM office_info WHERE office_status='Active'";
									echo "<h3>" . getData($db, $query) . "</h3>";
									?> Office(s)
								</h5>
							</div>
							<div class="card-footer">
								<a class="btn text-white" href="manage-office.php">View details</a>
							</div>
						</div>

						<div class="card w-100 shadow-lg rounded-xl text-white bg-dark text-center">
							<div class="card-body border-0 mt-2 mb-n3">
								<h5 class="pb-2">
									<?php
									$query = "SELECT count(*) FROM officer_info";
									echo "<h3>" . getData($db, $query) . "</h3>";
									?> Officer(s)
								</h5>
							</div>
							<div class="card-footer">
								<a class="btn text-white" href="manage-officer">View details</a>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</body>

<?php require_once('footer.php'); ?>