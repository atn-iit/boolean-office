<?php

	function checkIsSignIn(): bool
	{
		$sessionValues = ['admin_id', 'admin_name'];
		foreach ($sessionValues as $key => $value) {
			if (checkSessionValue($value) === FALSE) {
				return FALSE;
			}
		}
		return TRUE;
	}
