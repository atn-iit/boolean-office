<!-- jQuery 3.4.1 -->
<script src="../js/jquery.min.js"></script>

<!-- Bootstrap bundle js -->
<script src="../js/bootstrap.bundle.min.js"></script>

<!-- jQuery easing 1.4.1 -->
<script src="../js/jquery.easing.min.js"></script>

<!-- DataTable -->
<script src="../vendor/DataTable/jquery.dataTables.min.js"></script>
<script src="../vendor/DataTable/dataTables.bootstrap4.min.js"></script>
<script src="../vendor/DataTable/dataTables.responsive.min.js"></script>
<script src="../vendor/DataTable/dataTables.searchBuilder.min.js"></script>

<!-- Custom JS -->
<script src="../js/customjs.js"></script>

<script>
$(".alert-dismissible").fadeTo(4000, 500).slideUp(500, function() {
	$(".alert-dismissible").alert('close');
});
</script>

<footer class="footer bg-light border-top">
	<div class="m-2 footer-bottom">
		<p>Copyright © IIT NSTU 2020 | Designed and Developed by <b><a class="link" href="#">IIT 1<sup>st</sup>
					batch</a></b>
		</p>
	</div>
</footer>

</html>