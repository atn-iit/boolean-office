<?php
	$title = "Officer";

	require_once 'welcome.php';
	require_once 'header.php';

	$admin_id = $_SESSION['admin_id'];
	$admin_name = $_SESSION['admin_name'];

	if (checkIsPOSTS(['officer_email', 'officer_mobile', 'action'])) {
		$sessdata = array();
		try {
			// Check officer existence
			$query = "SELECT * FROM officer_info WHERE officer_email=? AND officer_mobile_number=?";
			$stmt = $db->query($query, [$_POST['officer_email'],$_POST['officer_mobile']], "ss");
			$result = $stmt->get_result();
			$stmt->close();

			if ($result->num_rows > 0) {
				$row = $result->fetch_assoc();
				$officer_id = $row['idofficer_info'];
				$status = $row['officer_status'];
				$action = $_POST['action'];
				$actionStr = $action === 'Active' ? '1' : '0';

				if ($status === $actionStr or $status === '2' or ($action !== 'Inactive' and $action !== 'Active')) {
					$sessdata['type'] = 'danger';
					$sessdata['message'] = 'Can not change the office status to ' . $_POST['action'];
				} else {
					if ($action === 'Inactive') {
						// Update the officer status
						$query = "UPDATE officer_info SET officer_status='0' WHERE idofficer_info=?";
						$stmt = $db->query($query, [$officer_id], "i");
						$affected_rows = $stmt->affected_rows;
						$stmt->close();

						if ($affected_rows > 0) {
							$sessdata['type'] = 'success';
							$sessdata['message'] = 'Officer status updated successfully';
						} else {
							$sessdata['type'] = 'danger';
							$sessdata['message'] = 'Can not update the officer status';
						}
					} else if ($action === 'Active') {
						// Update the officer status
						$query = "UPDATE officer_info SET officer_status='1' WHERE idofficer_info=?";
						$stmt = $db->query($query, [$officer_id], "i");
						$affected_rows = $stmt->affected_rows;
						$stmt->close();

						if ($affected_rows > 0) {
							$sessdata['type'] = 'success';
							$sessdata['message'] = 'Officer status updated successfully';
						} else {
							$sessdata['type'] = 'danger';
							$sessdata['message'] = 'Can not update the officer status';
						}
					}
				}
			} else {
				$sessdata['type'] = 'danger';
				$sessdata['message'] = 'No officer found with the selected one.';
			}
		} catch (Exception $ex) {
			error_log($ex->getMessage());
			$sessdata['type'] = 'danger';
			$sessdata['message'] = 'Query error!!!<br>Cannot update officer status.';
		}
		$_SESSION['admin_sessdata'] = $sessdata;
		// header("Location: manage-officer.php");
	}
?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
	<!-- Navigation bar -->
	<?php require_once('navbar.php'); ?>

	<!-- Body -->
	<div class="container">
		<?php
		if (checkSessionValue('admin_sessdata')) {
			$sessdata = $_SESSION['admin_sessdata'];
		?>
		<div class="row no-gutters">
			<div class="col-lg-5 col-md-12 ml-auto">
				<div class="alert alert-<?php echo $sessdata['type']; ?> alert-dismissible fade show" role="alert">
					<strong><?php echo $sessdata['message']; ?></strong>
				</div>
			</div>
		</div>
		<?php
			unset($_SESSION['admin_sessdata']);
		}
		?>

		<div class="row mb-3">
			<div class="col mt-2 pt-2 pl-2 pr-3 text-justify">
				<div class="text-center">
					<h4>Manage officers</h4>
				</div>
				<form id="form-officer" class="p-2 mb-5" action="" method="post">
					<input type="hidden" name="officer_email" id="officer_email">
					<input type="hidden" name="officer_mobile" id="officer_mobile">
					<input type="hidden" name="action" id="action">
					<table id="table-officer" class="table table-bordered table-condensed text-center">
						<thead class="thead-light">
							<tr>
								<th>Officer from</th>
								<th>Officer name</th>
								<th>Officer email</th>
								<th>Officer mobile number</th>
								<th>Officer created at</th>
								<th>Officer last password changed at</th>
								<th>Officer status</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php
							function getOfficeName($db, $id): string {
								$ret = $id . "";
								try {
									$query = "SELECT * FROM office_info WHERE idoffice_info=?";
									$stmt = $db->query($query, [$id], "i");
									$result = $stmt->get_result();
									$stmt->close();

									$row = $result->fetch_assoc();
									$ret = $row['office_short_name'];
								} catch (Exception $ex) {
									error_log($ex);
								}
								return $ret;
							}

							function getOfficerStatus($status): string {
								if ($status === '0') {
									return "Inactive";
								} else if ($status === '1') {
									return "Active";
								} else if ($status === '2') {
									return "Pending activation";
								}
								return "Unknown";
							}

							try {
								$query = "SELECT * FROM officer_info";
								$stmt = $db->query($query);
								$result = $stmt->get_result();
								$stmt->close();

								if ($result->num_rows > 0) {
									while ($row = $result->fetch_assoc()) {
										echo "<tr>" . 
												"<td>" . getOfficeName($db,$row['office_info_idoffice_info']) . "</td>" .
												"<td>" . $row['officer_name'] . "</td>" .
												"<td>" . $row['officer_email'] . "</td>" .
												"<td>" . $row['officer_mobile_number'] . "</td>" .
												"<td>" . $row['officer_created'] . "</td>" .
												"<td>" . $row['officer_modified'] . "</td>" .
												"<td>" . getOfficerStatus($row['officer_status']) . "</td>";
										if ($row['officer_status'] === "0") {
											echo "<td><button type='button' name='officerSubmit' class='btn btn-secondary' onclick='postAction(\"$row[officer_email]\",\"$row[officer_mobile_number]\",\"Active\",this)'>Set Active</button></td>";
										} else if ($row['officer_status'] === "1") {
											echo "<td><button type='button' name='officerSubmit' class='btn btn-danger' onclick='postAction(\"$row[officer_email]\",\"$row[officer_mobile_number]\",\"Inactive\",this)'>Set Inactive</button></td>";
										}
										echo "</tr>";
									}
							?>
							<script>
							function postAction(email, mobile, action, item) {
								if (email.trim() !== null && action.trim() !== null && mobile.trim() !== null) {
									if (action.trim() === "Inactive") {
										if (!confirm('This will set the officer as ' + action.trim() + '\nAnd officer no longer can signin\nProceed?')) {
											// item.preventDefault();
											return;
										}
									}
									$('#officer_email').val(email);
									$('#officer_mobile').val(mobile);
									$('#action').val(action);
									$('#form-officer').submit();
								}
							}
							</script>
							<?php
								} else {
									echo "<tr><td colspan='6'>No officer found <a class='btn btn-primary' href='add-officer.php'>Add new officer</a></td></tr>";
								}
							} catch (Exception $ex) {
								error_log($ex->getMessage());
								echo "<tr><td colspan='6'>Something went wrong while fetching the officer info</td></tr>";
							}
							?>
						</tbody>
					</table>
				</form>
			</div>
		</div>
	</div>
</body>

<script>
document.addEventListener('DOMContentLoaded', function() {
	let table1 = new DataTable('#table-officer');
});
</script>

<?php require_once 'footer.php' ?>