<?php
	session_start();

	require_once '../custom-functions.php';
	require_once 'check-signin.php';
	require_once '../dbconn.php';

	function checkUser($email, &$uname, $upass): string
	{
		if (empty($uname) || empty($email) || empty($upass)) {
			return "Empty email or username or password";
		}

		$query = "SELECT * FROM admin_info WHERE admin_email=? AND admin_username=?";
		try {
			$db = new Database();
			$stmt = $db->query($query, [$email,$uname], "ss");
			$result = $stmt->get_result();
			$stmt->close();

			if ($result->num_rows === 1) {
				$row = $result->fetch_assoc();
				if (md5($upass) === $row['admin_password']) {
					$_SESSION['admin_id'] = $row['idadmin'];
					$_SESSION['admin_name'] = $row['admin_username'];
					return "";
				} else {
					return "Password not matched";
				}
			} else {
				return "You are not authorized";
			}
		} catch (Exception $ex) {
			error_log($ex->getMessage());
		}

		return "Invalid username or email or password.";
	}

	// Page requested from SignIn page if user_name and user_password contains.
	if (checkIsPOSTS(['user_name', 'user_email', 'user_password'])) {
		$user_name = $_POST['user_name'];
		$user_email = $_POST['user_email'];
		$user_pass = $_POST['user_password'];
		
		if (filter_var($_POST['user_email'], FILTER_VALIDATE_EMAIL)) {
			$retVal = checkUser($user_email, $user_name, $user_pass);
			$location = 'index.php';

			if (!empty($retVal)) {
				$_SESSION['admin_errorval'] = $retVal;
				$location = 'signin.php';
			}

			unset($user_email);
			unset($user_name);
			unset($user_pass);

			header('Location: ' . $location);
		} else {
			$_SESSION['admin_errorval'] = 'Please enter a valid email';
			header('Location: signin.php');
		}
	}

	// If session value found
	$isSignPage = (isset($title) and !empty($title) and $title === 'SignIn');
	if (checkIsSignIn()) {
		if ($isSignPage) {
			header('Location: index.php');
		}
	} else {
		if ($isSignPage) {
			return;
		}

		header('Location: signin.php');
	}
