<?php
	$title = 'Created Files';

	require_once 'welcome.php';
	require_once 'header.php';

	$officer_id = $_SESSION['officer_id'];
	$curr_office_id = $_SESSION['office_id'];
?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
	<?php require_once('navbar.php'); ?>
	<div class="container">
		<?php
		if (checkSessionValue('sessdata')) {
			$sessdata = $_SESSION['sessdata'];
		?>
		<div class="row no-gutters">
			<div class="col-lg-5 col-md-12 ml-auto">
				<div class="alert alert-<?php echo $sessdata['type']; ?> alert-dismissible fade show" role="alert">
					<strong><?php echo $sessdata['message']; ?></strong>
				</div>
			</div>
		</div>
		<?php
			unset($_SESSION['sessdata']);
		}
		?>
		<div class="row">
			<div class="col mt-1 pt-1 pl-2 pr-3">
				<div class="row">
					<div class="col">
						<h2>Created active files</h2>
					</div>
				</div>

				<div class="row">
					<div class="col pt-2">
						<form id="postFileView-active" action="view.php" method="post">
							<table id="table-active-files" class="table table-bordered table-condensed table-hover text-center">
								<thead class="thead-light">
									<tr class="clickable-table-row">
										<th>Created date</th>
										<th>File reference number</th>
										<th>File name</th>
										<th>Current office</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody id="table-active-files-body" class="text-center">
									<?php
									function getOfficeName($db, $id): string {
										$ret = $id . "";
										try {
											$query = "SELECT * FROM office_info WHERE idoffice_info=?";
											$stmt = $db->query($query, [$id], "i");
											$result = $stmt->get_result();
											$stmt->close();

											$row = $result->fetch_assoc();
											$ret = $row['office_short_name'];
										} catch (Exception $ex) {
											error_log($ex);
										}
										return $ret;
									}

									$query = "SELECT * FROM file_info WHERE officer_info_idofficer_info=? 
														AND (file_current_status!='Not Approved' OR file_current_status!='Approved')
														ORDER BY file_created_at DESC";
									try {
										$stmt = $db->query($query, [$officer_id], "i");
										$result = $stmt->get_result();
										$stmt->close();

										if ($result->num_rows === 0) {
											echo "<tr><td colspan='5'>No active files</td></tr>";
										} else {
											while ($row = $result->fetch_assoc()) {
												?>
									<tr class='clickable-table-row' onclick='postActiveFileInfo(this);'>
										<td><?php echo $row['file_created_at'];?></td>
										<td><?php echo $row['file_number'];?></td>
										<td><?php echo $row['file_name'];?></td>
										<td><?php echo getOfficeName($db, $row['file_current_office']);?></td>
										<td><?php echo $row['file_current_status'];?></td>
									</tr>
									<?php
											}
											?>
									<script>
									function postActiveFileInfo(clickedRow) {
										var filetd = $(clickedRow).find('td').eq(1).html();
										if (!(filetd.trim() === null)) {
											$('#selectedFileID').val(filetd);
											$('#request').val('created-files.php');
											$('#postFileView-active').submit();
										}
									}
									</script>
									<?php
										}
									} catch (Exception $ex) {
										error_log($ex->getMessage());
										echo "<tr><td colspan='5'>Error!!!</td></tr>";
									}
									?>
								</tbody>
							</table>
							<input type="hidden" name="selectedFileID" id="selectedFileID">
							<input type="hidden" name="request" id="request">
						</form>
					</div>
				</div>

				<div class="row">
					<div class="col mt-3 pt-3">
						<h2>Created inactive files</h2>
					</div>
				</div>

				<div class="row">
					<div class="col pt-2">
						<form id="postFileView-inactive" action="view.php" method="post">
							<table id="table-inactive-files" class="table table-bordered table-condensed table-hover">
								<thead class="thead-light">
									<tr class="clickable-table-row">
										<th>Created/Submitted Date</th>
										<th>File reference number</th>
										<th>File name</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody id="table-inactive-files-body" class="text-center">
									<?php
									$query = "SELECT * FROM file_info WHERE officer_info_idofficer_info=? 
														AND (file_current_status='Not Approved' OR file_current_status='Approved')
														ORDER BY file_created_at DESC";
									try {
										$stmt = $db->query($query, [$officer_id], "i");
										$result = $stmt->get_result();
										$stmt->close();

										if ($result->num_rows === 0) {
											echo "<tr><td colspan='4'>No inactive files</td></tr>";
										} else {
											while ($row = $result->fetch_assoc()) {
												?>
									<tr class='clickable-table-row' onclick='postInactiveFileInfo(this);'>
										<td><?php echo $row['file_created_at'];?></td>
										<td><?php echo $row['file_number'];?></td>
										<td><?php echo $row['file_name'];?></td>
										<td><?php echo $row['file_current_status'];?></td>
									</tr>
									<?php
											}
											?>
									<script>
									function postInactiveFileInfo(clickedRow) {
										var filetd = $(clickedRow).find('td').eq(1).html();
										if (!(filetd.trim() === null)) {
											$('#selectedFileID').val(filetd);
											$('#request').val('created-files.php');
											$('#postFileView-inactive').submit();
										}
									}
									</script>
									<?php
										}
									} catch (Exception $ex) {
										error_log($ex->getMessage());
										echo "<tr><td colspan='4'>Error!!!</td></tr>";
									}
									?>
								</tbody>
							</table>
							<input type="hidden" name="selectedFileID" id="selectedFileID">
							<input type="hidden" name="request" id="request">
						</form>
					</div>
				</div>

			</div>
		</div>
	</div>
</body>

<script>
document.addEventListener('DOMContentLoaded', function() {
	let table1 = new DataTable('#table-active-files');
	let table2 = new DataTable('#table-inactive-files');
});
</script>

<?php require_once('footer.php'); ?>