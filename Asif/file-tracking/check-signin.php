<?php

	function checkIsSignIn(): bool
	{
		$sessionValues = ['office_id', 'officer_id', 'officer_name'];
		foreach ($sessionValues as $key => $value) {
			if (checkSessionValue($value) === FALSE) {
				return FALSE;
			}
		}
		return TRUE;
	}
