<?php
	require_once 'custom-functions.php';

	if (!checkIsPOST('selectedFileID')) {
		header('Location: index.php');
	}

	$title = '';

	require_once 'welcome.php';
	require_once 'header.php';

	$officer_id = $_SESSION['officer_id'];
	$curr_office_id = $_SESSION['office_id'];

	$query = "SELECT * FROM file_info i, carrier c 
					WHERE i.file_number=?
					AND i.file_current_carrier_id=c.idcarrier";

	$rowLabels = array('file_number' => 'File Number', 
		'file_name' => 'File Name', 'file_subject' => 'File Subject',
		'file_category' => 'File Category', 'file_created_at' => 'File Created At',
		'file_current_status' => 'File Current Status', 
		'file_current_office' => 'File current office',
		'file_path_office_id' => 'File submitted to office list',
		'carrier_first_name' => 'File current carrier firstname',
		'carrier_last_name' => 'File current carrier lastname',
		'carrier_mobile_primary' => 'File current carrier mobile (Primary)',
		'carrier_mobile_secondary' => 'File current carrier mobile (Secondary)',
		'file_download_path' => 'File attachment'
	);
	
	$row = array();

	$valid = FALSE;
	$sessdata = array();

	try {
		$stmt = $db->query($query, [$_POST['selectedFileID']], "s");
		$result = $stmt->get_result();
		$stmt->close();

		if ($result->num_rows === 1) {
			$row = $result->fetch_assoc();
			$valid = TRUE;
		} else {
			$sessdata['type'] = 'warning';
			$sessdata['message'] = 'Invalid file info requested';
		}
	} catch (Exception $ex) {
		error_log($ex->getMessage());
		$sessdata['type'] = 'danger';
		$sessdata['message'] = 'Query error';
	}

	if (!$valid) {
		$_SESSION['sessdata'] = $sessdata;
		header("Location: $_POST[request]");
	}
?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
	<?php require_once('navbar.php'); ?>

	<section class="mt-2 pt-2">
		<div class="container">
			<div class="row justify-content-center">
				<div class="card-deck mb-3 col-md-8">
					<div class="card w-50 shadow rounded">
						<div class="card-header">
							<h5 class="card-title text-center">File View</h5>
						</div>

						<div class="card-body border-0">
							<form id="downloadFile-form" action="download.php" method="post">
								<table class="table">
									<thead></thead>
									<tbody>
										<?php
									function getOfficeName($db, $id): string {
										$ret = $id . "";
										try {
											$query = "SELECT * FROM office_info WHERE idoffice_info=?";
											$stmt = $db->query($query, [$id], "i");
											$result = $stmt->get_result();
											$stmt->close();

											$row = $result->fetch_assoc();
											$ret = $row['office_short_name'];
										} catch (Exception $ex) {
											error_log($ex);
										}
										return $ret;
									}

									foreach ($rowLabels as $key => $value) {
										$content = '';
										if ($key === 'file_path_office_id') {
											$temp = explode(',', $row[$key]);
											$count = count($temp);
											$i = 1;
											$office_str = '';
											foreach ($temp as $key => $value1) {
												if ($i !== $count) {
													$office_str .= getOfficeName($db, $value1) . ' -> ';
												} else {
													$office_str .= getOfficeName($db, $value1);
												}
												$i++;
											}
											$content = $office_str;
										} else if ($key === 'file_current_office') {
											$content = getOfficeName($db, $row[$key]);
										} else {
											$content = $row[$key];
										}
										?>
										<tr>
											<td class='text-right border-0 w-50 font-weight-bold'>
												<?php echo "$value:"; ?>
											</td>
											<?php
											if ($key === 'file_download_path') {
											?>
											<td class='border-0 <?php echo empty($content) ? 'text-muted' : '';?>'>
												<?php 
												if (empty($content)) {
													echo 'NULL';
												} else {
												?>
												<button class="btn btn-secondary btn-block"
													onclick="postDownloadFile('<?php echo $content; ?>','<?php echo $row['file_number'];?>',this);">Download</button>
												<?php
												}
												?>
											</td>
											<?php
											} else {
											?>
											<td class='border-0 <?php echo empty($content) ? 'text-muted' : '';?>'>
												<?php 
												echo !empty($content) ? $content : 'NULL';
												?>
											</td>
											<?php 
											}
											?>
										</tr>
										<?php
									}
									?>
										<script>
										function postDownloadFile(file, id, control) {
											if (!(file.trim() === null) && !(id.trim() === null)) {
												$('#downloadFile-form #fileid').val(id);
												$('#downloadFile-form #filepath').val(file);
												$('#downloadFile-form').submit();
											}
										}
										</script>
									</tbody>
								</table>
								<input type="hidden" name="fileid" id="fileid">
								<input type="hidden" name="filepath" id="filepath">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</body>

<?php require_once('footer.php'); ?>