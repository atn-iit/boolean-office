<?php
	$title = "Carrier";

	require_once 'welcome.php';
	require_once 'header.php';

	$officer_id = $_SESSION['officer_id'];
	$curr_office_id = $_SESSION['office_id'];

	if (isset($_POST['carrierSubmit'])) {
		$sessdata = array();
		if (checkIsPOSTS(['first_name', 'last_name', 'mobile_primary'])) {
			try {
				$query = "SELECT * FROM carrier WHERE carrier_mobile_primary=?";
				$stmt = $db->query($query, [$_POST['mobile_primary']], "s");
				$num_rows = $stmt->get_result()->num_rows;
				$stmt->close();

				if ($num_rows > 0) {
					$sessdata['type'] = 'danger';
					$sessdata['message'] = 'A carrier with that information<br>already exists';
				} else {
					$query = "INSERT INTO carrier VALUES (NULL,?,?,?,?,?,?,?)";

					$mobile_secondary = checkIsPOST('mobile_secondary') ? $_POST['mobile_secondary'] : NULL;
					$email = checkIsPOST('email') ? $_POST['email'] : NULL;
					$address = checkIsPOST('address') ? $_POST['address'] : NULL;

					$params = [$_POST['first_name'],$_POST['last_name'],$_POST['mobile_primary'], $mobile_secondary, $email, $address, $_SESSION['officer_id']];
					$stmt = $db->query($query,$params,"ssssssi");
					$insert = $stmt->insert_id;
					$stmt->close();

					if ($insert > 0) {
						$sessdata['type'] = 'success';
						$sessdata['message'] = 'New carrier information<br>inserted successfully.';
					} else {
						$sessdata['type'] = 'danger';
						$sessdata['message'] = 'Cannot insert new carrier information.';
					}
				}
			} catch (Exception $ex) {
				error_log($ex->getMessage());
				$sessdata['type'] = 'danger';
				$sessdata['message'] = 'Query error!!!<br>Cannot insert new carrier information.';
			}
		} else {
			$sessdata['type'] = 'warning';
			$sessdata['message'] = 'Please provide all the information requested';
		}

		$_SESSION['sessdata'] = $sessdata;
	}
?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
	<!-- Navigation bar -->
	<?php require_once('navbar.php'); ?>

	<!-- Body -->
	<div class="container">
		<div class="row justify-content-center mb-3">
			<div class="mt-2 pt-2 pl-2 pr-3 text-justify">
				<form class="shadow-lg rounded-xl p-5 mb-5" action="" method="post">
					<div class="col-form-label text-center pb-4">
						<h5>Add new carrier</h5>
					</div>

					<div class="form-group form-floating">
						<input type="text" name="first_name" id="first_name" class="form-control" placeholder=""
							required autofocus>
						<label for="first_name">Carrier first name</label>
					</div>

					<div class="form-group form-floating">
						<input type="text" name="last_name" id="last_name" class="form-control" placeholder=""
							required autofocus>
						<label for="last_name">Carrier last name</label>
					</div>

					<div class="form-group form-floating">
						<input type="tel" pattern="0[0-9]{10}" name="mobile_primary" id="mobile_primary"
							class="form-control" placeholder="" required>
						<label for="mobile_primary">Mobile number (Primary)</label>
					</div>

					<div class="form-group form-floating">
						<input type="tel" pattern="0[0-9]{10}" name="mobile_secondary" id="mobile_secondary"
							class="form-control" placeholder="">
						<label for="mobile_secondary">Mobile number (Secondary)</label>
					</div>

					<div class="form-group form-floating">
						<input type="email" name="email" id="email"
							class="form-control" placeholder="">
						<label for="email">Email address</label>
					</div>

					<div class="form-group form-floating">
						<textarea name="address" id="address" rows="3" cols="25" class="form-control"
							placeholder=""></textarea>
						<label for="address">Carrier Address</label>
					</div>

					<?php
					if (checkSessionValue('sessdata')) {
						$sessdata = $_SESSION['sessdata'];
					?>
					<div class="mt-2 mb-2">
						<span class="text-<?php echo $sessdata['type']; ?>">
							<p class="text-center"><b><?php echo $sessdata['message']; ?></b></p>
						</span>
					</div>
					<?php
						unset($_SESSION['sessdata']);
					}
					?>

					<div class="form-group">
						<input type="submit" class="btn btn-primary btn-block" value="Submit" name="carrierSubmit">
						<a class="btn btn-secondary btn-block" href="manage-carrier.php">Cancel</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>

<?php
require_once 'footer.php';
?>