<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">

	<!-- Metadata -->
	<meta name="author" content="Abrar">
	<meta name="application-name" content="Boolean Office - File Tracking">
	<meta name="description" content="File Tracking | This website is a part of Boolean Office Automation">
	<meta name="keywords" content="IIT,Office, Automation">
	<!-- <meta http-equiv="refresh" content="30"> -->

	<!-- Page resizing -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Page icon -->
	<link rel="icon" href="res/nstu.png" type="image/png">
	<link rel="shortcut icon" href="res/favicon.ico" type="image/jpg" />

	<!-- Bootstrap core 4.4.1 -->
	<link rel="stylesheet" href="css/bootstrap.min.css">

	<!-- DataTable -->
	<link rel="stylesheet" href="vendor/DataTable/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" href="vendor/DataTable/responsive.bootstrap4.min.css">
	<link rel="stylesheet" href="vendor/DataTable/searchBuilder.bootstrap4.min.css">

	<!-- Custom stylesheet -->
	<link rel="stylesheet" href="css/style.css">

	<title>File Tracking <?php if (isset($title)) echo '| ' . $title; ?></title>
</head>