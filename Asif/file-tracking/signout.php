<?php
	session_start();
	// session_unset();
	// session_destroy();

	if (isset($_SESSION['office_id'])) {
		unset($_SESSION['office_id']);
	}

	if (isset($_SESSION['officer_id'])) {
		unset($_SESSION['officer_id']);
	}

	if (isset($_SESSION['officer_name'])) {
		unset($_SESSION['officer_name']);
	}
	
	if (isset($_SESSION['sessdata'])) {
		unset($_SESSION['sessdata']);
	}

	header('Location: signin.php');
?>
