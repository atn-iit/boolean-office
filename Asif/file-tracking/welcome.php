<?php
	session_start();

	require_once 'custom-functions.php';
	require_once 'check-signin.php';
	require_once 'dbconn.php';

	function checkUser(&$uname, $upass): string
	{
		if (empty($uname) || empty($upass)) {
			return "Empty username and password";
		}

		$query = "SELECT * FROM officer_info WHERE officer_email = ?";
		try {
			$db = new Database();
			$stmt = $db->query($query, [$uname]);
			$result = $stmt->get_result();
			$stmt->close();

			if ($result->num_rows === 0) {
				return "You are not authorized";
			}

			$row = $result->fetch_assoc();
			if ($row['officer_status'] === '1') {
				if (md5($upass) === $row['officer_password']) {
					$_SESSION['office_id'] = $row['office_info_idoffice_info'];
					$_SESSION['officer_id'] = $row['idofficer_info'];
					$_SESSION['officer_name'] = $row['officer_name'];
					return "";
				} else {
					return "Password not matched";
				}
			} else {
				if ($row['officer_status'] === '0') {
					return "You are no longer authorized for the system";
				} else {
					return "Please activate your account before signin";
				}
			}
		} catch (Exception $ex) {
			error_log($ex->getMessage());
		}

		return "Invalid user mail or password.";
	}

	// Page requested from SignIn page if user_name and user_password contains.
	if (checkIsPOSTS(['user_name', 'user_password'])) {
		$user_name = $_POST['user_name'];
		$user_pass = $_POST['user_password'];
		if (filter_var($_POST['user_name'], FILTER_VALIDATE_EMAIL)) {
			$retVal = checkUser($user_name, $user_pass);
			$location = 'index.php';

			if (!empty($retVal)) {
				$_SESSION['errorval'] = $retVal;
				$location = 'signin.php';
			}

			unset($user_name);
			unset($user_pass);

			header('Location: ' . $location);
		} else {
			$_SESSION['errorval'] = 'Please enter a valid email';
			header('Location: signin.php');
		}
	}

	// If session value found
	$isSignPage = (isset($title) and !empty($title) and $title === 'SignIn');
	if (checkIsSignIn()) {
		if ($isSignPage) {
			header('Location: index.php');
		}
	} else {
		if ($isSignPage) {
			return;
		}

		header('Location: signin.php');
	}
