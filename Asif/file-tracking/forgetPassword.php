<?php

$title = "Forget Password";
require_once 'header.php';

session_start();

$sessData = !empty($_SESSION['sessData'])?$_SESSION['sessData']:'';
if(!empty($sessData['status']['msg'])){
    $statusMsg = $sessData['status']['msg'];
    $statusMsgType = $sessData['status']['type'];
    unset($_SESSION['sessData']['status']);
}

?>

<body>
	<div class="container">
		<div class="row justify-content-center mb-3">
			<div class="col-md-6 mt-4 pt-4 pl-2 pr-3 text-justify">
				<div class="shadow-lg mt-3 p-5 rounded-lg">
					<div class="text-center">
						<h5>Select an action</h5>
					</div>
					<div class="row">
						<div class="col p-3">
							<a href="requestReset.php" class="btn btn-secondary btn-lg btn-block" role="button"
								aria-pressed="true"><span class="small">Request for a password reset code</span></a>
						</div>
						<div class="col p-3">
							<a href="resetPassword.php" class="btn btn-secondary btn-lg btn-block" role="button"
								aria-pressed="true"><span class="small">Enter reset <br>code</span></a>
						</div>
					</div>
					<?php if (!empty($statusMsg)) { ?>
					<span class="small text-<?php echo ($statusMsgType === 'error') ? 'danger' : 'success'; ?>">
						<?php echo '<p>'.$statusMsg.'</p>'; ?>
					</span>
					<?php } ?>
					<a href="signin.php" class="btn btn-secondary btn-lg btn-block" role="button" aria-pressed="true">Go back</a>
				</div>
			</div>
		</div>
	</div>
</body>

<?php require_once('footer.php'); ?>