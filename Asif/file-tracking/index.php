<?php
	$title = 'Dashboard';
	
	require_once 'welcome.php';
	require_once 'header.php';

	$officer_id = $_SESSION['officer_id'];
	$curr_office_id = $_SESSION['office_id'];

?>

<link rel="stylesheet" href="css/Chart.min.css">

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
	<?php 
	require_once('navbar.php'); 

	function getData($db, $query, $params, $type): string
	{
		try {
			$stmt = $db->query($query, $params, $type);
			$result = $stmt->get_result();
			$stmt->close();

			if ($result->num_rows > 0) {
				$row = $result->fetch_row();
				return $row[0];
			} else {
				return "0";
			}
		} catch (Exception $ex) {
			error_log($ex->getMessage());
			return "-1";
		}
	}
	?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="mt-1 pt-1 pl-2 pr-3">
				<div class="row">
					<h2 class="col text-center">Dashboard</h2>
				</div>

				<div class="row">
					<div class="col-12">
						<div class="card border-0 shadow-sm rounded">
							<div class="card-body p-3">
								<div class="row align-items-center">
									<div class="col-lg-6 text-justify">
										<h4>Created files</h4>
										<p class="text-muted">
											Number of files created from this officer which includes active files and inactive files.
										</p>
										<p class="text-muted mb-3">
											Active files are files which is currently pending for approval. And Inactive files are files which
											is already approved or not approved.
										</p>
									</div>

									<div class="col-lg-6">
										<div class="position-relative">
											<canvas id="createdFilesChart"></canvas>
											<div class="position-absolute position-center text-center pt-3">
												<h2 class="text-uppercase mb-0">
													<?php
													$query = "SELECT count(*) FROM file_info WHERE officer_info_idofficer_info=?";
													$params = [$officer_id];
													$type = "i";
													echo getData($db, $query, $params, $type);
													?>
												</h2>
												<p class="text-muted text-uppercase mb-0">Created files</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-12">
						<div class="card border-0 shadow-sm rounded">
							<div class="card-body p-3">
								<div class="row align-items-center">
									<div class="col-lg-6 text-justify">
										<h4>Received files</h4>
										<p class="text-muted">
											Number of files received from other offices which includes pending files and forwarded files by this office.
										</p>
										<p class="text-muted mb-3">
											Pending files are files which is forwarded to this office by other offices.
											Forwarded files are files which are forwarded by this office to other offices. Which also contains approved and not approved files by this office.
										</p>
									</div>

									<div class="col-lg-6">
										<div class="position-relative">
											<canvas id="receivedFilesChart"></canvas>
											<div class="position-absolute position-center text-center pt-3">
												<h2 class="text-uppercase mb-0">
													<?php
													$query = "SELECT count(*) FROM file_log WHERE file_moved_to_office_id=?";
													$params = [$curr_office_id];
													$type = "i";
													echo getData($db, $query, $params, $type);
													?>
												</h2>
												<p class="text-muted text-uppercase mb-0">Received files</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</body>

<?php
$query = "SELECT count(*) FROM file_info WHERE officer_info_idofficer_info=? 
					AND (file_current_status='Created' OR file_current_status='Forwarded')";
$params = [$officer_id];
$type = "i";
$numPendingFiles = getData($db, $query, $params, $type);

$query = "SELECT count(*) FROM file_info WHERE officer_info_idofficer_info=? 
					AND (file_current_status='Approved')";
$params = [$officer_id];
$type = "i";
$numApprovedFiles = getData($db, $query, $params, $type);

$query = "SELECT count(*) FROM file_info WHERE officer_info_idofficer_info=? 
					AND (file_current_status='Not Approved')";
$params = [$officer_id];
$type = "i";
$numNotApprovedFiles = getData($db, $query, $params, $type);

$query = "SELECT count(*) FROM file_log l, file_info i WHERE l.file_moved_to_office_id=? 
				AND i.file_current_office=? 
				AND (l.file_status='Forwarded' OR l.file_status='Created')
				AND l.file_info_idfile_info=i.idfile_info";
$params = [$curr_office_id,$curr_office_id];
$type = "ii";
$numPendingFilesBy = getData($db, $query, $params, $type);

$query = "SELECT count(*) FROM file_log l, file_info i WHERE l.file_moved_from_office_id=? 
					AND (l.file_status='Approved')
					AND l.file_info_idfile_info=i.idfile_info";
$params = [$curr_office_id];
$type = "i";
$numApprovedFilesBy = getData($db, $query, $params, $type);

$query = "SELECT count(*) FROM file_log l, file_info i WHERE l.file_moved_from_office_id=? 
					AND (l.file_status='Not Approved')
					AND l.file_info_idfile_info=i.idfile_info";
$params = [$curr_office_id];
$type = "i";
$numNotApprovedFilesBy =  getData($db, $query, $params, $type);

$query = "SELECT count(*) FROM file_log l, file_info i WHERE l.file_moved_from_office_id=? 
					AND (l.file_status='Forwarded')
					AND l.file_info_idfile_info=i.idfile_info";
$params = [$curr_office_id];
$type = "i";
$numForwardedFilesBy =  getData($db, $query, $params, $type);
?>

<script src="js/Chart.bundle.min.js"></script>
<script>
Chart.defaults.RoundedDoughnut = Chart.helpers.clone(Chart.defaults.doughnut);
Chart.controllers.RoundedDoughnut = Chart.controllers.doughnut.extend({
	draw: function(ease) {
		var ctx = this.chart.ctx;
		var easingDecimal = ease || 1;
		var arcs = this.getMeta().data;
		Chart.helpers.each(arcs, function(arc, i) {
			arc.transition(easingDecimal).draw();

			var pArc = arcs[i === 0 ? arcs.length - 1 : i - 1];
			var pColor = pArc._view.backgroundColor;

			var vm = arc._view;
			var radius = (vm.outerRadius + vm.innerRadius) / 2;
			var thickness = (vm.outerRadius - vm.innerRadius) / 2;
			var startAngle = Math.PI - vm.startAngle - Math.PI / 2;
			var angle = Math.PI - vm.endAngle - Math.PI / 2;

			ctx.save();
			ctx.translate(vm.x, vm.y);

			ctx.fillStyle = i === 0 ? vm.backgroundColor : pColor;
			ctx.beginPath();
			ctx.arc(radius * Math.sin(startAngle), radius * Math.cos(startAngle), thickness, 0, 2 * Math.PI);
			ctx.fill();

			ctx.fillStyle = vm.backgroundColor;
			ctx.beginPath();
			ctx.arc(radius * Math.sin(angle), radius * Math.cos(angle), thickness, 0, 2 * Math.PI);
			ctx.fill();

			ctx.restore();
		});
	}
});
</script>
<script>
let c1 = new Chart(document.getElementById('createdFilesChart'), {
	type: 'RoundedDoughnut',
	data: {
		labels: [
			"Pending files",
			"Approved files",
			"Not approved files",
		],
		datasets: [{
			data: [
				<?php 
					echo $numPendingFiles. ',' . $numApprovedFiles . ',' . $numNotApprovedFiles;
					?>
			],
			borderWidth: 6,
			borderColor: '#fff',
			backgroundColor: [
				'#f9d65c',
				"#a9d876",
				"#0e8a73",
			],
			hoverBackgroundColor: [
				'#f9d65c',
				"#a9d876",
				"#0e8a73",
			]
		}]
	},
	options: {
		cutoutPercentage: 80,
		borderAlign: 'center',
		animation: {
			duration: 1000,
			easing: 'linear'
		},
		legend: {
			position: 'top',
			labels: {
				padding: 10,
				fontStyle: 'italic',
				fontColor: '#333',
				usePointStyle: true,
			}
		},
	}
});

let c2 = new Chart(document.getElementById('receivedFilesChart'), {
	type: 'RoundedDoughnut',
	data: {
		labels: [
			"Currently pending to the office",
			"Forwarded files by this office",
			"Approved files by this office",
			"Not Approved files by this office",
		],
		datasets: [{
			data: [
				<?php 
					echo $numPendingFilesBy . ',' . $numForwardedFilesBy . ',' . $numApprovedFilesBy . ',' . $numNotApprovedFilesBy;
					?>
			],
			borderWidth: 6,
			borderColor: '#fff',
			backgroundColor: [
				'#f9d65c',
				"#39be83",
				"#a9d876",
				"#0e8a73",
			],
			hoverBackgroundColor: [
				'#f9d65c',
        "#39be83",
				"#a9d876",
				"#0e8a73",
			]
		}]
	},
	options: {
		cutoutPercentage: 80,
		borderAlign: 'center',
		animation: {
			duration: 1000,
			easing: 'linear'
		},
		legend: {
			position: 'top',
			labels: {
				padding: 10,
				fontStyle: 'italic',
				fontColor: '#333',
				usePointStyle: true,
			}
		},
	}
});
</script>
<?php require_once('footer.php'); ?>