<?php

	function console_log($data)
	{
		echo '<script>';
		echo 'console.log(' . json_encode($data) . ')';
		echo '</script>';
	}

	function checkSessionValue($value): bool
	{
		return (isset($_SESSION[$value]) and !empty($_SESSION[$value]));
	}

	function checkIsPOST($value): bool
	{
		return (isset($_POST[$value]) && !empty($_POST[$value]));
	}

	function checkIsPOSTS($array): bool
	{
		foreach ($array as $key => $val) {
			if (checkIsPOST($val) === FALSE) {
				return FALSE;
			}
		}

		return TRUE;
	}

