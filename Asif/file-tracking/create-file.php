<?php
	$title = 'Create File';

	require_once 'welcome.php';
	require_once 'header.php';
	require_once 'table-schema.php';

	$officer_id = $_SESSION['officer_id'];
	$curr_office_id = $_SESSION['office_id'];

	// Check POST's
	$vals = array('file_number', 'file_name', 'subject', 'sending_location','file_carrier');

	$supported_exts = ['xls','xlsx','csv','pdf','doc','docx','ppt','pptx'];

	if (isset($_POST['submit'])) {
		$sessdata = array();

		if (checkIsPOSTS($vals)) {
			try {
				// Checking whether the file already exists
				$query = "SELECT * FROM file_info WHERE file_number=? AND officer_info_idofficer_info=?";
				$stmt = $db->query($query, [$_POST['file_number'], $officer_id], "si");
				$num_rows = $stmt->get_result()->num_rows;
				$stmt->close();

				if ($num_rows > 0) {
					$sessdata['type'] = 'warning';
					$sessdata['message'] = 'Given file number already exists.';
				} else {
					// Getting the office id
					$query = "SELECT * FROM office_info WHERE office_short_name=? AND office_status='Active'";		
					$stmt = $db->query($query, [$_POST['sending_location']], "s");
					$result = $stmt->get_result();
					$stmt->close();
					
					// Checking the sending office ID
					$sending_office_id = -1;
					if ($result->num_rows === 1) {
						$row = $result->fetch_assoc();
						$sending_office_id = $row['idoffice_info'];
					}

					if ($sending_office_id === -1) {
						$sessdata['type'] = 'danger';
						$sessdata['message'] = 'Sending office not found';
					} else {
						// Getting the carrier id
						$query = "SELECT * FROM carrier WHERE carrier_mobile_primary=? AND officer_info_idofficer_info=?";
						$stmt = $db->query($query, [$_POST['file_carrier'], $officer_id], "si");
						$result = $stmt->get_result();
						$stmt->close();

						// Checking the carrier ID
						if ($result->num_rows === 1) {
							$row = $result->fetch_assoc();
							$carrierID = $row['idcarrier'];

							$download_path = NULL;
							$doneFile = TRUE;
							$fileMessage = '';

							// Checking whether a file is provided
							if (isset($_FILES['document']) and $_FILES['document']['error'] === 0) {
								$file = $_FILES['document']['name'];
								if (preg_match('/^[^.][-a-z0-9_.]+[a-z]$/i', $file)) {
									$temp = explode('.', basename($file));
									$ext = end($temp);
									if (in_array($ext, $supported_exts)) {
										$fileSize = $_FILES['document']['size'];
										if ($fileSize < 10000000) { // 10MB
											$tmpName = $_FILES['document']['tmp_name'];
											
											$newName = '';
											$newTempName = $_POST['file_number'] . "." . $ext;

											$newFilePath = 'uploads/' . $curr_office_id;
											if (!mkdir($newFilePath, 0777, TRUE)) {
												$newFilePath = '';
												$newName = $curr_office_id . '_' . $newTempName;
											} else {
												$newName = $curr_office_id . '/' . $newTempName;
											}

											if (move_uploaded_file($tmpName, 'uploads/' . $newName)) {
												$download_path = $newName;
											} else {
												$doneFile = FALSE;
												$fileMessage = 'Could not store the uploaded file';
											}
										} else {
											$doneFile = FALSE;
											$fileMessage = 'File size is more than 10 MB';
										}
									} else {
										$doneFile = FALSE;
										$fileMessage = 'Given file not supported.<br>Provide ' . implode(' ', $supported_exts) . ' files';
									}
								} else {
									$doneFile = FALSE;
									$fileMessage = 'Invalid file name';
								}
							}

							if ($doneFile) {
								// Insert into file_info
								$file_category = isset($_POST['file_category']) ? strtolower($_POST['file_category']) : NULL;
								$path_office_id = $curr_office_id . "," . $sending_office_id;

								$query = "INSERT INTO `file_info` VALUES (NULL,?,?,?,?,current_timestamp(),?,'Created',?,?,?,?)";
								$params = array($_POST['file_number'], $_POST['file_name'], $_POST['subject'], $file_category,
													$download_path,$sending_office_id,$path_office_id,$carrierID,$officer_id);

								$stmt = $db->query($query, $params, "sssssisii");
								$insertID = $stmt->insert_id;
								$stmt->close();

								if ($insertID > 0) {
									// Insert into file_log
									$query = "INSERT INTO file_log VALUES (NULL,?,?,'Created','',current_timestamp(),?,?)";
									$params = array($curr_office_id, $sending_office_id, $insertID, $carrierID);
									$stmt = $db->query($query, $params, "iiii");
									$inserted = $stmt->insert_id;
									$stmt->close();

									if ($inserted > 0) {
										$sessdata['type'] = 'success';
										$sessdata['message'] = 'File created successfully';
									} else {
										$sessdata['type'] = 'danger';
										$sessdata['message'] = 'Can not create the file<br>and move to the requested office';

										// Delete the file info
										$query = "DELETE FROM file_info WHERE idfile_info=?";
										$stmt = $db->query($query, [$insertID], "i");
										$stmt->close();
									}
								} else {
									$sessdata['type'] = 'danger';
									$sessdata['message'] = 'Can not create the file.';
								}
							} else {
								$sessdata['type'] = 'warning';
								$sessdata['message'] = $fileMessage;
							}
						} else {
							$sessdata['type'] = 'danger';
							$sessdata['message'] = 'No carrier person found<br>with the selected carrier';
						}
					}
				}
			} catch (Exception $ex) {
				error_log($ex->getMessage());
				$sessdata['type'] = 'danger';
				$sessdata['message'] = 'Query error';
			}
		} else {
			$sessdata['type'] = 'warning';
			$sessdata['message'] = 'Please provide all the required fields';
		}

		$_SESSION['sessdata'] = $sessdata;
	}
?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
	<!-- Navigation bar -->
	<?php require_once('navbar.php'); ?>

	<!-- Body -->
	<div class="container">
		<?php
		if (checkSessionValue('sessdata')) {
			$sessdata = $_SESSION['sessdata'];
		?>
		<div class="row no-gutters">
			<div class="col-lg-5 col-md-12 ml-auto">
				<div class="alert alert-<?php echo $sessdata['type']; ?> alert-dismissible fade show" role="alert">
					<strong><?php echo $sessdata['message']; ?></strong>
				</div>
			</div>
		</div>
		<?php
			unset($_SESSION['sessdata']);
		}
		?>
		<div class="row justify-content-center mb-3">
			<div class="mt-4 pt-4 pl-2 pr-3 text-justify">
				<form class="shadow-lg rounded-xl p-5 mb-5" action="create-file.php" method="post" enctype="multipart/form-data"
					onsubmit="return checkFile();">
					<div class="col-form-label text-center pb-4">
						<h5>Create File and Submit It</h5>
					</div>

					<div class="form-group form-floating">
						<input type="text" name="file_number" id="file_number" class="form-control" placeholder="" required
							autofocus>
						<label for="file_number">File reference number</label>
					</div>

					<div class="form-group form-floating">
						<input type="text" name="file_name" id="file_name" class="form-control" placeholder="" required>
						<label for="file_name">File name</label>
					</div>

					<div class="form-group form-floating">
						<input type="text" name="file_category" id="file_category" class="form-control"
							placeholder="Comma separated text">
						<label for="file_category">File category (Optional)</label>
					</div>

					<div class="form-group form-floating">
						<textarea name="subject" id="subject" rows="5" cols="25" class="form-control" placeholder=""
							required></textarea>
						<label for="subject">File subject</label>
					</div>

					<div class="form-group form-floating">
						<select class="form-control custom-select" name="sending_location" id="sending_location" required>
							<option value="" selected disabled hidden>None</option>
							<?php
							$query = "SELECT office_short_name FROM office_info WHERE idoffice_info!=? AND office_status='Active' ORDER BY office_short_name";

							try {
								$stmt = $db->query($query, [$curr_office_id], "i");
								$result = $stmt->get_result();
								$stmt->close();

								if ($result->num_rows > 0) {
									while ($row = $result->fetch_row()) {
										echo "<option value='$row[0]'>$row[0]</option>";
									}
								}
							} catch (Exception $ex) {
								error_log($ex->get_message());
							}
						?>
						</select>
						<label for="sending_location">File sending location</label>
					</div>

					<div class="form-group form-floating">
						<select class="form-control custom-select" name="file_carrier" id="file_carrier" required>
							<option value="" selected disabled hidden>None</option>
							<?php
							$query = "SELECT * FROM carrier WHERE officer_info_idofficer_info=?";

							try {
								$stmt = $db->query($query, [$officer_id], "i");
								$result = $stmt->get_result();
								$stmt->close();

								if ($result->num_rows > 0) {
									while ($row = $result->fetch_assoc()) {
										echo "<option value='$row[carrier_mobile_primary]'>$row[carrier_first_name] $row[carrier_last_name]</option>";
									}
								}
							} catch (Exception $ex) {
								error_log($ex->get_message());
							}

							?>
						</select>
						<label for="file_carrier">Select a carrier person</label>
					</div>

					<div class="form-group">
						<label for="document">Upload any document file (Optional)</label>
						<input class="form-control border-0" type="file" name="document" id="document"
							accept=".xls,.xlsx,.csv,.pdf,.doc,.docx,.ppt,.pptx">
					</div>

					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-block" name="submit">Submit</button>
						<a class="btn btn-secondary btn-block" href="index.php">Cancel</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>

<script type="text/javascript">
function checkFile() {
	var exts = ['xls','xlsx','csv','pdf','doc','docx','ppt','pptx'];
	var fileElement = document.getElementById("document");
	var fileExtension = "";
	if (fileElement.value) {
		if (fileElement.value.lastIndexOf(".") > 0) {
			fileExtension = fileElement.value.substring(fileElement.value.lastIndexOf(".") + 1, fileElement.value.length);
		}
		if (exts.indexOf(fileExtension.toLowerCase()) >= 0) {
			return true;
		} else {
			alert("You must select specific file types to upload. (" + exts + ")");
			return false;
		}
	}
}
</script>

<?php require_once('footer.php'); ?>