<?php

	$table_office_info_columns = array('`idoffice_info`', '`office_name`', '`office_short_name`');

	$table_file_info_columns = array('`idfile_info`', '`file_number`', '`file_name`',
		'`file_subject`', '`file_category`', '`file_created_at`');
	$table_file_log_columns = array('`idfile_log`', '`file_moved_from_office_id`',
		'`file_moved_to_office_id`', '`file_status`', '`file_tags`',
		'`file_carrier_name`', '`file_carrier_mobile_number`', '`file_moved_at`',
		'`file_info_idfile_info`', '`officer_info_idofficer_info`');

	$table_office_info_columns_str = implode(", ", $table_office_info_columns);
	$table_file_info_columns_str = implode(", ", $table_file_info_columns);
	$table_file_log_columns_str = implode(", ", $table_file_log_columns);

	function get_table_column_name($table_name, $column_index)
	{
		global ${"table_" . $table_name . "_columns"};
		$val = ${"table_" . $table_name . "_columns"}[$column_index];
		return str_replace('`', '', $val);
	}
