<?php
require('header.php');
if ($_SESSION['ROLE'] != 1) {
   header('location:show_teacher_profile.php?id=' . $_SESSION['USER_ID']);
   die();
}
if ($_SESSION['ROLE'] == 1) {
   header('location:teacher.php');
   die();
}

$res = mysqli_query($con, "select * from department order by id desc");
?>
