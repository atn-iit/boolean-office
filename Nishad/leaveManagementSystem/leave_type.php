<?php
require('header.php');
if ($_SESSION['ROLE'] != 1) {
   header('location:add_teacher.php?id=' . $_SESSION['USER_ID']);
   die();
}
if (isset($_GET['type']) && $_GET['type'] == 'delete' && isset($_GET['id'])) {
   $id = mysqli_real_escape_string($con, $_GET['id']);
   mysqli_query($con, "delete from leave_type where id='$id'");
}
$res = mysqli_query($con, "select * from leave_type order by id desc");
?>


<div class="main-body">

   <div class="card-body">
      <h3>Leave Types</h3>
      <a href="add_leave_type.php"><button class="btn btn-sm btn-info">Add Leave-Type</button></a>
   </div>


   <div class="table-body">
      <table class="table table-striped">
         <thead class="table-head">
            <tr>
               <th scope="col" width="15%">SerialNo</th>
               <th scope="col" width="65%">Leave Type</th>
               <th scope="col" width="20%">Action</th>
            </tr>
         </thead>
         <tbody>
            <?php
            $i = 1;
            while ($row = mysqli_fetch_assoc($res)) { ?>
               <tr scope="row">
                  <td><?php echo $i ?></td>
                  <td><?php echo $row['leave_type'] ?></td>
                  <td><a href="add_leave_type.php?id=<?php echo $row['id'] ?>">Edit</a> <a href="leave_type.php?id=<?php echo $row['id'] ?>&type=delete">Delete</a></td>
               </tr>
            <?php
               $i++;
            } ?>
         </tbody>
      </table>
   </div>
</div>

<?php
require('footer.php');
?>