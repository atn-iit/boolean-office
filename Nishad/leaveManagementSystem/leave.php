<?php
$title = "List of Leave Apllications";
require('header.php');

if (isset($_GET['type']) && $_GET['type'] == 'delete' && isset($_GET['id'])) {
	$id = mysqli_real_escape_string($con, $_GET['id']);
	mysqli_query($con, "delete from `leave` where id='$id'");
}
if (isset($_GET['type']) && $_GET['type'] == 'update' && isset($_GET['id'])) {
	$id = mysqli_real_escape_string($con, $_GET['id']);
	$status = mysqli_real_escape_string($con, $_GET['status']);
	mysqli_query($con, "update `leave` set leave_status='$status' where id='$id'");
}
if ($_SESSION['ROLE'] == 1) {
	$sql = "select `leave`.*, teacher.name,teacher.id as eid from `leave`,teacher where `leave`.teacher_id=teacher.id order by `leave`.id desc";
} else {
	$eid = $_SESSION['USER_ID'];
	$sql = "select `leave`.*, teacher.name ,teacher.id as eid from `leave`,teacher where `leave`.teacher_id='$eid' and `leave`.teacher_id=teacher.id order by `leave`.id desc";
}
$res = mysqli_query($con, $sql);
?>


<div class="main-body">

	<div class="card-body">
		<h3>Leave Applications</h3>
		<?php if ($_SESSION['ROLE'] == 2) { ?>
			<a href="add_leave.php"><button class="btn btn-sm btn-info">Apply for Leave</button></a>
		<?php } ?>
	</div>
	<div class="table-body">
		<table class="table table-striped">
			<thead class="table-head">
				<tr>
					<th scope="col" width="3%">S.No</th>
					<th scope="col" width="20%">Teacher's Name</th>
					<th scope="col" width="8%">From</th>
					<th scope="col" width="8%">To</th>
					<th scope="col" width="20%">Description</th>
					<th scope="col" width="10%">File</th>
					<th scope="col" width="12%">Leave Status</th>
					<th scope="col" width="10%">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$i = 1;
				while ($row = mysqli_fetch_assoc($res)) { ?>
					<tr scope="row">
						<td><?php echo $i ?></td>
						<td><?php echo $row['name'] ?></td>
						<td><?php echo $row['leave_from'] ?></td>
						<td><?php echo $row['leave_to'] ?></td>
						<td><?php echo $row['leave_description'] ?></td>
						<td><a href="files/formats/<?php echo $row['application_pdf'] ?>" target="blank"><img src="images/pdf.png" width="40%" alt="pdf-icon"></a></td>
						<td>
							<?php
							if ($row['leave_status'] == 1) {
								echo "Applied";
							}
							if ($row['leave_status'] == 2) {
								echo "Approved";
							}
							if ($row['leave_status'] == 3) {
								echo "Rejected";
							}
							?>
							<?php if ($_SESSION['ROLE'] == 1) { ?>
								<select class="form-control" onchange="update_leave_status('<?php echo $row['id'] ?>',this.options[this.selectedIndex].value)">
									<option value="">Update Status</option>
									<option value="2">Approved</option>
									<option value="3">Rejected</option>
								</select>
							<?php } ?>
						</td>
						<td>
							<?php
							if ($row['leave_status'] == 1) { ?>
								<a href="leave.php?id=<?php echo $row['id'] ?>&type=delete">Delete</a>
							<?php } else {
								echo "Can't Change now.";
							} ?>
						</td>

					</tr>
				<?php
					$i++;
				} ?>
			</tbody>
		</table>
	</div>
</div>



<script>
	function update_leave_status(id, select_value) {
		window.location.href = 'leave.php?id=' + id + '&type=update&status=' + select_value;
	}
</script>
<?php
require('footer.php');
?>