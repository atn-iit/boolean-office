<?php
$title = "Add Application Format";
require("header.php");
// if ($_SESSION['ROLE'] == 2 && $_SESSION['USER_ID'] != $id) {
//   header("location:show_applications.php");
// }
if (isset($_POST['submit'])) {
  $title = $_POST['title'];
  $type = $_POST['type'];

  if($type == 'NOC'){
    $pdf_name = "NOC.pdf";
    $type = "NOC";

    $del_sql = "delete from pdf where type='NOC'"; 
    $query = mysqli_query($con, $del_sql);
  }else if($type == 'GO'){
    $pdf_name = "GO.pdf";
    $type = "GO";
    $del_sql = "delete from pdf where type='GO'"; 
    $query = mysqli_query($con, $del_sql);
  }else if($type == 'sabbatical'){
    $pdf_name = "sabbatical.pdf";
    $type = "sabbatical";
    $del_sql = "delete from pdf where type='sabbatical'"; 
    $query = mysqli_query($con, $del_sql);
  }else{
    header('location:show_applications.php');
    die();
  }
  
  $pdf_type = $_FILES['pdf']['type'];
  $pdf_size = $_FILES['pdf']['size'];
  $pdf_temp_loc = $_FILES['pdf']['tmp_name'];
  $pdf_store = "files/applications/" . $pdf_name;

  move_uploaded_file($pdf_temp_loc, $pdf_store);

  $sql = "insert into pdf(title, pdf_name, type) values('$title', '$pdf_name', '$type')";

  if (mysqli_query($con, $sql)) {
    header("location:show_applications.php");
  } else {
    echo "Error";
  }
}
?>
<div class="main-body">
  <div class="card-body">
    <h3>Application Forms</h3>

  </div>
  <div class="form-div d-flex justify-content-center">
    <div class="form-block">
      <form method="post" enctype="multipart/form-data">
        <div class="form-group">
          <label for="NOC_title" class="form-control-label">Enter Application Title</label>
          <input type="text" name="title" class="form-control" required>
        </div>
        <div class="form-group">
          <label for="NOC_pdf" class=" form-control-label">Select NOC Application PDF</label>
          <input type="file" name="pdf" class="form-control" required>
        </div>
        <div class="form-group">
          <input type="radio" name="type" value="NOC" id=""> NOC
          <input type="radio" name="type" value="GO" id=""> GO
          <input type="radio" name="type" value="sabbatical" id=""> Sabbatical
        </div>

        <button type="submit" name="submit" class="btn btn-lg btn-info btn-block">
          <span id="payment-button-amount">Submit</span>
        </button>
      </form>
    </div>
  </div>
</div>


<?php
require("footer.php");
?>