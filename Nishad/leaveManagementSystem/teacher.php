<?php
$title = "Edit Teacher's Profile";
require('header.php');
if ($_SESSION['ROLE'] != 1) {
   header('location:show_teacher_profile.php?id=' . $_SESSION['USER_ID']);
   die();
}
if (isset($_GET['type']) && $_GET['type'] == 'delete' && isset($_GET['id'])) {
   $id = mysqli_real_escape_string($con, $_GET['id']);
   mysqli_query($con, "delete from teacher where id='$id'");
}
$res = mysqli_query($con, "select * from teacher where role=2 order by id desc");
?>

<div class="main-body">

   <div class="card-body">
      <h3>Teachers</h3>
      <a href="add_teacher.php"><button class="btn btn-sm btn-info">Add Teacher</button></a>
   </div>

   <div class="table-body">
      <table class="table table-striped">
         <thead class="table-head">
            <tr>
               <th scope="col" width="5%">S.No</th>
               <th scope="col" width="25%">Name</th>
               <th scope="col" width="15%">Email</th>
               <th scope="col" width="15%">Mobile</th>
               <th scope="col" width="20%">Address</th>
               <th scope="col" width="20%">Action</th>
            </tr>
         </thead>
         <tbody>
            <?php
            $i = 1;
            while ($row = mysqli_fetch_assoc($res)) { ?>
               <tr scope="row">
                  <td><?php echo $i ?></td>
                  <td><?php echo $row['name'] ?></td>
                  <td><?php echo $row['email'] ?></td>
                  <td><?php echo $row['mobile'] ?></td>
                  <td><?php echo $row['address'] ?></td>
                  <td><a href="add_teacher.php?id=<?php echo $row['id'] ?>">Edit</a> <a href="teacher.php?id=<?php echo $row['id'] ?>&type=delete">Delete</a></td>
               </tr>
            <?php
               $i++;
            } ?>
         </tbody>
      </table>
   </div>

</div>

<?php
require('footer.php');
?>