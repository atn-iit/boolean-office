<?php
require('db_connection.php');
if (!isset($_SESSION['ROLE'])) {
  header('location:login.php');
  die();
}
?>
<!DOCTYPE html>
<html class="no-js" lang="">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php if (isset($title)) {
            echo $title;
          } else {
            echo "IIT Automation Application";
          } ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="images/nstu-logo.png" type="image/x-icon">
  <link rel="stylesheet" href="assets/css/bootstrap.css">
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <!-- <link rel="stylesheet" href="assets/css/style.css"> -->
  <link rel="stylesheet" href="assets/css/new_style.css">
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

</head>

<body>
  <div class="container">

    <header>
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#"><img src="images/nstu-logo.png" width="40px" alt="">NSTU</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse justify-content-between" id="navbarTogglerDemo02">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">

            <?php if ($_SESSION['ROLE'] == 1) { ?>
              <li class="nav-item">
                <a class="nav-link" href="teacher.php">Teachers</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="leave_type.php">Leave Types</a>
              </li>
            <?php } else { ?>
              <li class="nav-item">
                <a class="nav-link" href="show_teacher_profile.php?id=<?php echo $_SESSION['USER_ID'] ?>">Profile</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="change_password.php">Change Password</a>
              </li>
            <?php } ?>
            <li class="nav-item">
              <a class="nav-link" href="show_applications.php">Application Formats</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="leave.php">Leave</a>
            </li>
          </ul>
          <div class="dropdown">
            <button class="btn btn-outline-secondary dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false">
              <?php echo $_SESSION['USER_NAME'] ?>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="logout.php">Logout</a>
            </div>
          </div>

        </div>

      </nav>
    </header>