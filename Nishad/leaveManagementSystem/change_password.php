<?php
$title = "Change Password";
require('header.php');
$msg = "";

if (isset($_SESSION['USER_ID'])) {
	// function validate($data)
	// {
	// 	$data = trim($data);
	// 	$data = stripslashes($data);
	// 	$data = htmlspecialchars($data);
	// 	return $data;
	// }

	if (isset($_POST['submit'])) {
		$np = mysqli_real_escape_string($con, $_POST['np']);
		$op = mysqli_real_escape_string($con, $_POST['op']);
		$c_np = mysqli_real_escape_string($con, $_POST['c_np']);

		$id = $_SESSION['USER_ID'];

		if ($np == $c_np) {
			$sql = "SELECT password FROM teacher WHERE password=$op AND id='$id'";
			$result = mysqli_query($con, $sql);
			if (mysqli_num_rows($result) === 1) {
				$np = md5($np);
				$sql_2 = "UPDATE teacher SET password='$np' WHERE id='$id'";

				mysqli_query($con, $sql_2);
				$msg = 'Password is Changed Succesfully!';
			} else {
				$msg = 'could not update';
			}
		} else {
			$msg = "New Password and Confirm Password are not same!";
		}
	}
}

?>

<div class="main-body">
	<div class="card-body">
		<h3>Application Forms</h3>

	</div>
	<div class="form-div d-flex justify-content-center">
		<div class="form-block">
			<div class="result_msg"><?php echo $msg ?></div>
			<form method="post">
				<div class="form-group">
					<label class="form-control-label">Old Password</label><br>
					<input type="password" name="op" placeholder="Old Password" class="form-control">
				</div>

				<div class="form-group">
					<label class="form-control-label">New Password</label><br>
					<input type="password" name="np" placeholder="New Password" class="form-control">
				</div>

				<div class="form-group">
					<label class="form-control-label">Confirm New Password</label><br>
					<input type="password" name="c_np" placeholder="Confirm New Password" class="form-control">
				</div>

				<button type="submit" name="submit" class="btn btn-lg btn-info btn-block">
          <span id="payment-button-amount">Submit</span>
        </button>

			</form>
		</div>
	</div>

</div>


<?php
require('footer.php');
?>