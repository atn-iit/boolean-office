<?php
$title = "Add New Teacher";
require('header.php');
$name = '';
$email = '';
$mobile = '';
$department_id = '';
$address = '';
$id = '';
if (isset($_GET['id'])) {
	$id = mysqli_real_escape_string($con, $_GET['id']);
	if ($_SESSION['ROLE'] == 2 && $_SESSION['USER_ID'] != $id) {
		die('Access denied');
	}
	$res = mysqli_query($con, "select * from teacher where id='$id'");
	$row = mysqli_fetch_assoc($res);
	$name = $row['name'];
	$email = $row['email'];
	$mobile = $row['mobile'];
	$department_id = $row['department_id'];
	$address = $row['address'];
}
if (isset($_POST['submit'])) {
	$name = mysqli_real_escape_string($con, $_POST['name']);
	$email = mysqli_real_escape_string($con, $_POST['email']);
	$mobile = mysqli_real_escape_string($con, $_POST['mobile']);
	$password = mysqli_real_escape_string($con, $_POST['password']);
	$password = md5($password);
	$department_id = mysqli_real_escape_string($con, $_POST['department_id']);
	$address = mysqli_real_escape_string($con, $_POST['address']);
	if ($id > 0) {
		$sql = "update teacher set name='$name',email='$email',mobile='$mobile',address='$address',password='$password' where id='$id'";
	} else {
		$sql = "insert into teacher(name,email,mobile,address,password,role) values('$name','$email','$mobile','$address','$password','2')";
	}
	mysqli_query($con, $sql);
	echo "<script>location.href='teacher.php';</script>";
}
?>
<div class="main-body">
	<div class="card-body">
		<h3>Teacher's Profile</h3>

	</div>
	<div class="form-div d-flex justify-content-center">
		<div class="form-block">
			<form method="post">
				<div class="form-group">
					<label class=" form-control-label">Name</label>
					<input type="text" value="<?php echo $name ?>" name="name" placeholder="Enter Teacher's Name" class="form-control" required>
				</div>
				<div class="form-group">
					<label class=" form-control-label">Email</label>
					<input type="email" value="<?php echo $email ?>" name="email" placeholder="Enter teacher's Email" class="form-control" required>
				</div>
				<div class="form-group">
					<label class=" form-control-label">Mobile</label>
					<input type="text" value="<?php echo $mobile ?>" name="mobile" placeholder="Enter Teacher's Mobile" class="form-control" required>
				</div>
				<div class="form-group">
					<label class=" form-control-label">Address</label>
					<input type="text" value="<?php echo $address ?>" name="address" placeholder="Enter Teacher's Address" class="form-control" required>
				</div>
				<div class="form-group">
					<label class=" form-control-label">Password</label>
					<input type="password" name="password" placeholder="Enter Teacher's Password" class="form-control" required>
				</div>
			

				<?php if ($_SESSION['ROLE'] == 1) { ?>
					<button type="submit" name="submit" class="btn btn-lg btn-info btn-block">
						<span id="payment-button-amount">Submit</span>
					</button>
				<?php } ?>
			</form>

		</div>
	</div>
</div>


<?php
require('footer.php');
?>