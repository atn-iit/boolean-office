<?php
require('db_connection.php');
$msg = "";
if (isset($_POST['email']) && isset($_POST['password'])) {
   $email = mysqli_real_escape_string($con, $_POST['email']);
   $password = md5(mysqli_real_escape_string($con, $_POST['password']));
   $res = mysqli_query($con, "select * from teacher where email='$email' and password='$password'");
   $count = mysqli_num_rows($res);
   if ($count > 0) {
      $row = mysqli_fetch_assoc($res);
      $_SESSION['ROLE'] = $row['role'];
      $_SESSION['USER_ID'] = $row['id'];
      $_SESSION['USER_NAME'] = $row['name'];
      $_SESSION['USER_MAIL'] = $row['email'];
      header('location:index.php');
      die();
   } else {
      $msg = "Please enter correct login details";
   }
}
?>
<!DOCTYPE html>
<html class="no-js" lang="">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <title>Login Page</title>
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="assets/css/style.css">
   <!-- <link rel="stylesheet" href="new_style.css"> -->
   <link rel="stylesheet" href="assets/css/bootstrap.css">
</head>

<body id="page-top">

   <!-- Body -->
   <div class="container">
      <div class="row justify-content-center mb-3">
         <div class="mt-4 pt-4 pl-2 pr-3 text-justify">
            <form class="form shadow mt-3 p-5" method="POST">
               <div class="col-form-label text-center pb-4">
                  <h4>Sign in to<br>Leave Management System</h4>
               </div>
               <div class="form-group">
                  <input type="email" class="form-control" name="email" placeholder="Enter You Email" autofocus required>
               </div>
               <div class="form-group">
                  <input type="password" class="form-control" name="password" placeholder="Enter Password" required>
               </div>
               <button type="submit" class="btn btn-primary btn-block">Sign In</button>
               <div class="result_msg"><?php echo $msg ?></div>  
            </form>
         </div>
      </div>
   </div>

</body>

</html>