<?php
$title = "Apply Leave Application";
require('header.php');

if (isset($_POST['submit'])) {

	$leave_id = mysqli_real_escape_string($con, $_POST['leave_id']);
	$leave_from = mysqli_real_escape_string($con, $_POST['leave_from']);
	$leave_to = mysqli_real_escape_string($con, $_POST['leave_to']);
	$teacher_id = $_SESSION['USER_ID'];
	$leave_description = mysqli_real_escape_string($con, $_POST['leave_description']);

	$pdf_type = $_FILES['pdf']['type'];
	$pdf_size = $_FILES['pdf']['size'];
	$pdf_name = $_FILES['pdf']['name'];
	$pdf_temp_loc = $_FILES['pdf']['tmp_name'];
	$pdf_store = "files/formats/" . $pdf_name;

	move_uploaded_file($pdf_temp_loc, $pdf_store);


	$sql = "insert into `leave`(leave_id, leave_from, leave_to, teacher_id, leave_description, application_pdf, leave_status) values('$leave_id', '$leave_from', '$leave_to', '$teacher_id', '$leave_description', '$pdf_name', 1)";
	mysqli_query($con, $sql);
	echo "<script>location.href='leave.php';</script>";
}
?>
<div class="main-body">
	<div class="card-body">
		<h3>Leave Form</h3>

	</div>
	<div class="form-div d-flex justify-content-center">
		<div class="form-block">
			<form method="post" enctype="multipart/form-data">
				<div class="form-group">
					<label class=" form-control-label">Leave Type</label>
					<select name="leave_id" required class="form-control">
						<option value="">Select Leave</option>
						<?php
						$res = mysqli_query($con, "select * from leave_type order by leave_type desc");
						while ($row = mysqli_fetch_assoc($res)) {
							echo "<option value=" . $row['id'] . ">" . $row['leave_type'] . "</option>";
						}
						?>
					</select>
				</div>
				<div class="form-group">
					<label class=" form-control-label">From Date</label>
					<input type="date" name="leave_from" class="form-control" required>
				</div>
				<div class="form-group">
					<label class=" form-control-label">To Date</label>
					<input type="date" name="leave_to" class="form-control" required>
				</div>
				<div class="form-group">
					<label class=" form-control-label">Leave Description</label>
					<input type="text" name="leave_description" class="form-control">
				</div>
				<div class="form-group">
					<label class=" form-control-label">Application File</label>
					<input type="file" name="pdf" class="form-control">
				</div>

				<button type="submit" name="submit" class="btn btn-lg btn-info btn-block">
					<span id="payment-button-amount">Submit</span>
				</button>
			</form>

		</div>
	</div>
</div>



<?php
require('footer.php');
?>