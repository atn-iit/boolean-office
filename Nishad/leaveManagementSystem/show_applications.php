<?php
$title = "Application Formats";
require("header.php");
$res = mysqli_query($con, "select * from pdf order by id desc");

?>


<div class="main-body">

  <div class="card-body">
    <h3>Application Formats</h3>
    <?php if ($_SESSION['ROLE'] == 1) { ?>
      <a href="add_applications.php"><button class="btn btn-sm btn-info">Add Application</button></a>
    <?php }
    ?>
  </div>

  <div class="table-body">
    <table class="table table-striped">
      <thead class="table-head">
        <tr>
          <th scope="col" width="10%">S.No</th>
          <th scope="col" width="60%">Title</th>
          <th scope="col" width="30%">Application PDF Format</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $i = 1;
        while ($row = mysqli_fetch_assoc($res)) { ?>
          <tr scope="row">
            <td><?php echo $i ?></td>
            <td><?php echo $row['title'] ?></td>
            <td><a href="files/applications/<?php echo $row['pdf_name'] ?>" target="blank"><img src="images/pdf.png" width="15%" alt="pdf-icon"></a></td>
          </tr>
        <?php
          $i++;
        } ?>
      </tbody>
    </table>
  </div>

</div>


<?php
require("footer.php");
?>