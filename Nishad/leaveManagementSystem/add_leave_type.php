<?php
$title = "Add Leave Type";
require('header.php');
if ($_SESSION['ROLE'] != 1) {
	header('location:add_teacher.php?id=' . $_SESSION['USER_ID']);
	die();
}
$leave_type = '';
$id = '';
if (isset($_GET['id'])) {
	$id = mysqli_real_escape_string($con, $_GET['id']);
	$res = mysqli_query($con, "select * from leave_type where id='$id'");
	$row = mysqli_fetch_assoc($res);
	$leave_type = $row['leave_type'];
}
if (isset($_POST['leave_type'])) {
	$leave_type = mysqli_real_escape_string($con, $_POST['leave_type']);
	if ($id > 0) {
		$sql = "update leave_type set leave_type='$leave_type' where id='$id'";
	} else {
		$sql = "insert into leave_type(leave_type) values('$leave_type')";
	}
	mysqli_query($con, $sql);
	echo "<script>location.href='leave_type.php';</script>";
}
?>
<div class="main-body">
	<div class="card-body">
		<h3>Department Form</h3>

	</div>
	<div class="form-div d-flex justify-content-center">
		<div class="form-block">
			<form method="post">
				<div class="form-group">
					<label for="leave_type" class=" form-control-label">Leave-Type Name</label>
					<input type="text" value="<?php echo $leave_type ?>" name="leave_type" placeholder="Enter new Leave Type" class="form-control" required>
				</div>

				<button type="submit" name="submit" class="btn btn-lg btn-info btn-block">
					<span id="payment-button-amount">Submit</span>
				</button>
			</form>
		</div>
	</div>
</div>



<?php
require('footer.php');
?>