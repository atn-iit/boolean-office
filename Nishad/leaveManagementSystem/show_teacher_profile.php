<?php
$title = "Teacher's Profile.";
require('header.php');



if (isset($_GET['id'])) {
  $id = mysqli_real_escape_string($con, $_GET['id']);
  if ($_SESSION['ROLE'] == 2 && $_SESSION['USER_ID'] != $id) {
    die('Access denied');
  }
  $res = mysqli_query($con, "select * from teacher where id='$id'");
  $row = mysqli_fetch_assoc($res);
  $name = $row['name'];
  $email = $row['email'];
  $mobile = $row['mobile'];
  $address = $row['address'];
}

?>
<div class="main-body">
  <div class="card-body">
    <h3>Teacher's Profile</h3>

  </div>


  <div class="card text-center d-flex justify-content-center">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-3">
          <h6 class="mb-0">Full Name</h6>
        </div>
        <div class="col-sm-9 text-secondary">
          <?php echo $name ?>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-sm-3">
          <h6 class="mb-0">Email</h6>
        </div>
        <div class="col-sm-9 text-secondary">
          <?php echo $email ?>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-sm-3">
          <h6 class="mb-0">Phone</h6>
        </div>
        <div class="col-sm-9 text-secondary">
          <?php echo $mobile ?>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-sm-3">
          <h6 class="mb-0">Address</h6>
        </div>
        <div class="col-sm-9 text-secondary">
          <?php echo $address ?>
        </div>
      </div>


    </div>
  </div>

  <?php
  require('footer.php');
  ?>