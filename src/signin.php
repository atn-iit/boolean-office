<?php
	$title = "SignIn";

	require_once 'welcome.php';
	require_once 'header.php';

	function getUserTable($row, $db)
	{
		$urole = $row['user_role'];
		$message = '';
		$status = true;

		if ($urole !== 'Section Officer' && $urole !== 'Teacher' && $urole !== 'Student') {
			$message = "Invalid user role";
			$status = false;
		} else {
			$ustatus = $row['user_status'];
			if ($ustatus === 'Inactive') {
				$message = "You are no longer authorized for the system";
				$status = false;
			} else if ($ustatus === 'New') {
				$message = "Please activate your account before signin";
				$status = false;
			} else {
				$rarray = [
					'Section Officer' => [
						'table' => 'officer_info',
						'id' => 'idofficer_info',
						'office_id' => 'office_info_idoffice_info'
					],
					'Teacher' => [
						'table' => 'teacher_info',
						'id' => 'idteacher_info',
						'office_id' => 'office_info_idoffice_info'
					],
					'Student' => [
						'table' => 'student_info',
						'id' => 'idstudent_info',
						'office_id' => 'session_info_office_info_idoffice_info'
					]
				];

				$query = "SELECT * FROM " . $rarray[$urole]['table'] . " WHERE user_info_iduser_info=?";
				$stmt = $db->query($query, [$row['iduser_info']], "i");
				$result = $stmt->get_result();
				$stmt->close();

				if ($result->num_rows > 0) {
					$row2 = $result->fetch_assoc();

					$_SESSION['id'] = $row2[$rarray[$urole]['id']];
					$_SESSION['office_id'] = $row2[$rarray[$urole]['office_id']];
					$_SESSION['user_role'] = $urole;
				} else {
					$message = 'User not found at user specific table.';
					$status = false;
				}
			}
		}

		$info = [];
		$info['message'] = $message;
		$info['status'] = $status;
		return $info;
	}

	function checkUser(&$uname, $upass)
	{
		$ret = [];

		if (empty($uname) || empty($upass)) {
			$ret['status'] = false;
			$ret['message'] = "Empty user email and password";
			return $ret;
		}

		$query = "SELECT * FROM user_info WHERE user_email = ?";
		try {
			$db = new Database();
			$stmt = $db->query($query, [$uname], 's');
			$result = $stmt->get_result();
			$stmt->close();

			// Check user is in the system
			if ($result->num_rows === 0) {
				$ret['status'] = false;
				$ret['message'] = "You are not authorized";
				return $ret;
			}

			$row = $result->fetch_assoc();

			// Check user password matched or not
			if (md5($upass) !== $row['user_password']) {
				$ret['status'] = false;
				$ret['message'] = "Password not matched";
				return $ret;
			}

			// Get user status based on user role
			$user_table_info = getUserTable($row, $db);
			if ($user_table_info['status'] === false) {
				$ret['status'] = false;
				$ret['message'] = $user_table_info['message'];
				return $ret;
			}

			$_SESSION['user_id'] = $row['iduser_info'];
			$_SESSION['user_name'] = $row['user_name'];

			$ret['status'] = true;
			$ret['message'] = "Sign in successful";
			return $ret;
		} catch (Exception $ex) {
			error_log($ex->getMessage());
		}

		$ret['status'] = false;
		$ret['message'] = "Invalid user email or password.";
		return $ret;
	}

	// Page requested from SignIn page if user_email and user_password contains.
	if (checkIsPOSTS(['user_email', 'user_password'])) {
		$user_email = $_POST['user_email'];
		$user_pass = $_POST['user_password'];
		if (filter_var($_POST['user_email'], FILTER_VALIDATE_EMAIL)) {
			$retVal = checkUser($user_email, $user_pass);
			$location = 'index.php';

			if ($retVal['status'] === FALSE) {
				$_SESSION['errorval'] = $retVal['message'];
			} else {
				header('Location: ' . $location);
			}
		} else {
			$_SESSION['errorval'] = 'Please enter a valid email';
		}
	}

	$sessData = !empty($_SESSION['sessData']) ? $_SESSION['sessData'] : '';
	if (!empty($sessData['status']['msg'])) {
		$statusMsg = $sessData['status']['msg'];
		$statusMsgType = $sessData['status']['type'];
		unset($_SESSION['sessData']['status']);
	}
?>

	<body>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-4 pl-2 pr-2 mb-5 pb-4 text-justify">
				<form class="shadow-lg rounded-xl mt-3 p-5 bg-white" action="" method="POST">
					<div class="col-form-label text-center pb-4">
						<h4>Sign in to<br>IIT Office Automation</h4>
					</div>
					<div class="form-group form-floating">
						<input type="email" class="form-control" id="user_email" name="user_email" placeholder="" autofocus
									 required>
						<label for="user_email">User email</label>
					</div>
					<div class="form-group form-floating">
						<input type="password" class="form-control" id="user_password" name="user_password" placeholder="" required>
						<label for="user_password">User password</label>
					</div>

					<div class="pb-2">
						<span class="small text-danger">
							<?php
								if (isset($_SESSION['errorval']) and !empty($_SESSION['errorval'])) {
									echo $_SESSION['errorval'];
									unset($_SESSION['errorval']);
								}
							?>
						</span>
					</div>
					<?php if (!empty($statusMsg)) { ?>
						<span class="small text-<?php echo ($statusMsgType === 'error') ? 'danger' : 'success'; ?>">
						<?php echo '<p>' . $statusMsg . '</p>'; ?>
					</span>
					<?php } ?>
					<button type="submit" class="btn btn-primary btn-block">Sign In</button>
					<div class="pt-3 pb-3">
						<a class="btn btn-secondary btn-block" id="forget-password" href="forgetPassword.php">Forget password?</a>
						<a class="btn btn-secondary btn-block" id="activate-account" href="activateSignin.php">Activate
							account</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	</body>

<?php require_once('footer.php'); ?>