<?php
	require_once 'custom-functions.php';
	require_once 'dbconn.php';
	require_once 'sendEmail.php';

	session_start();

	try {
		if (isset($_POST['forgotSubmit'])) { // Password forgotten. Requesting for a reset code
			if (checkIsPOST('user_email')) {
				$query = "SELECT * FROM user_info WHERE user_email=?";

				// Check user email existence
				$stmt = $db->query($query, [$_POST['user_email']]);
				$result = $stmt->get_result();
				$stmt->close();

				if ($result->num_rows > 0) {
					$row = $result->fetch_assoc();

					// Check user status
					$query = "SELECT * FROM user_info WHERE user_status='Active'";
					$stmt = $db->query($query);
					$num_rows = $stmt->get_result()->num_rows;
					$stmt->close();

					if ($num_rows > 0) {
						$query = "SELECT * FROM user_info WHERE user_email=? AND user_forget_pass_identity IS NULL";
						$stmt = $db->query($query, [$_POST['user_email']], "s");
						$num_rows = $stmt->get_result()->num_rows;
						$stmt->close();

						if ($num_rows > 0) {
							$uniqidStr = md5(uniqid(mt_rand()));

							$query = "UPDATE user_info SET user_modified=?, user_forget_pass_identity=? WHERE user_email=?";
							$stmt = $db->query($query, [date("Y-m-d H:i:s"), $uniqidStr, $_POST['user_email']]);
							$affected_rows = $stmt->affected_rows;
							$stmt->close();

							if ($affected_rows > 0) {
								// $link = 'resetPassword.php?fp_code=' . $uniqidStr;
								$toEmail = $_POST['user_email'];
								$subject = 'Password reset request';
								$message = 'A forget password request was submitted for your account. If this was a mistake, just ignore this mail and nothing will happen.' .
									'<br/>To reset your password use this reset code: ' . $uniqidStr .
									'<br/><br/>Regards,<br/>IIT Office Info Center';

								$ret = sendMailByPHPMailer($toEmail, $subject, $message);
								if ($ret['status'] === 'OK') {
									$sessData['status']['type'] = 'success';
									$sessData['status']['msg'] = 'Please check your e-mail, we have sent a password reset code to your registered email.';
								} else {
									$sessData['status']['type'] = 'error';
									$sessData['status']['msg'] = $ret['result'];
								}
							} else {
								$sessData['status']['type'] = 'error';
								$sessData['status']['msg'] = 'Some problem occurred, please try again.';
							}
						} else {
							$sessData['status']['type'] = 'error';
							$sessData['status']['msg'] = 'You already requested for a password reset code';
						}
					} else {
						$sessData['status']['type'] = 'error';
						$sessData['status']['msg'] = 'Given email is not associated with any active account.';
					}
				} else {
					$sessData['status']['type'] = 'error';
					$sessData['status']['msg'] = 'No user found with the given email address.';
				}
			} else {
				$sessData['status']['type'] = 'error';
				$sessData['status']['msg'] = 'Enter email to create a new password for your account.';
			}

			$_SESSION['sessData'] = $sessData;
			header("Location: forgetPassword.php");
		} else if (isset($_POST['resetSubmit'])) {
			$reset_code = '';
			if (checkIsPOST('user_password') and checkIsPOST('user_confirm_password') and checkIsPOST('reset_code')) {
				$reset_code = $_POST['reset_code'];
				$password = $_POST['user_password'];
				$confirm_password = $_POST['user_confirm_password'];

				if ($password === $confirm_password) {
					$query = "SELECT * FROM user_info WHERE user_forget_pass_identity=?";
					$stmt = $db->query($query, [$reset_code]);
					$num_rows = $stmt->get_result()->num_rows;
					$stmt->close();

					if ($num_rows > 0) {
						$query = "UPDATE user_info SET user_password=?, user_forget_pass_identity=NULL WHERE user_forget_pass_identity=?";
						$password_hash = md5($password);
						$stmt = $db->query($query, [$password_hash, $reset_code]);
						$affected_rows = $stmt->affected_rows;
						$stmt->close();

						if ($affected_rows > 0) {
							$sessData['status']['type'] = 'success';
							$sessData['status']['msg'] = 'Your account password has been reset successfully. Please login with your new password.';
						} else {
							$sessData['status']['type'] = 'error';
							$sessData['status']['msg'] = 'Some problem occurred, please try again.';
						}
					} else {
						$sessData['status']['type'] = 'error';
						$sessData['status']['msg'] = 'You are not authorized to reset new password of this account.';
					}
				} else {
					$sessData['status']['type'] = 'error';
					$sessData['status']['msg'] = 'Confirm password must match with the password.';
				}
			} else {
				$sessData['status']['type'] = 'error';
				$sessData['status']['msg'] = 'All fields are mandatory, please fill all the fields.';
			}

			$_SESSION['sessData'] = $sessData;
			$redirectURL = ($sessData['status']['type'] == 'success') ? 'signin.php' : 'resetPassword.php';
			header("Location:" . $redirectURL);
		}
	} catch (Exception $ex) {
		error_log($ex->getMessage());
		$sessData['status']['type'] = 'error';
		$sessData['status']['msg'] = 'Query error';
	}
