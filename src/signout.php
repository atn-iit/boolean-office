<?php
	session_start();
	// session_unset();
	// session_destroy();

	if (isset($_SESSION['id'])) {
		unset($_SESSION['id']);
	}

	if (isset($_SESSION['office_id'])) {
		unset($_SESSION['office_id']);
	}

	if (isset($_SESSION['user_id'])) {
		unset($_SESSION['user_id']);
	}

	if (isset($_SESSION['user_name'])) {
		unset($_SESSION['user_name']);
	}

	if (isset($_SESSION['user_role'])) {
		unset($_SESSION['user_role']);
	}

	if (isset($_SESSION['sessdata'])) {
		unset($_SESSION['sessdata']);
	}

	header('Location: signin.php');
?>
