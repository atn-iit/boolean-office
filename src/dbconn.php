<?php
	if (basename($_SERVER['PHP_SELF']) === basename(__FILE__)) {
		header($_SERVER["SERVER_PROTOCOL"] . " 403 Forbidden");
		exit('Direct access not allowed');
	}

	require_once('config.php');

	class Database
	{
		private mysqli $connection;

		public function __construct()
		{
			mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

			try {
				$this->connection = new mysqli(Config::$dbHost, Config::$dbUser, Config::$dbPass, Config::$dbName);
				$this->connection->set_charset("utf8mb4");
			} catch (Exception $ex) {
				error_log($ex->getMessage());
				exit('Error connecting to database.');
			}
		}

		public function __destruct()
		{
			$this->connection->close();
		}

		/*
		 * Usage: $db = new Database();
		 *
		 * SELECT
		 * $row = $db->query("SELECT ? bar", ['foo'])->get_result()->fetch_assoc();
		 *
		 * INSERT
		 * $data = [$username, $email, $password];
		 * $row = $db->query("INSERT INTO users VALUES (NULL, ?,?,?)", $data);
		 */
		public function query($sql, $params = array(), $types = "")
		{
			$types = $types ?: str_repeat("s", count($params));
			try {
				$stmt = $this->connection->prepare($sql);
				if (count($params) > 0)
					$stmt->bind_param($types, ...$params);
				$stmt->execute();
			} catch (Exception $ex) {
				error_log($ex->getMessage());
				// echo ('Error querying the database. ' . $ex->getMessage() . ' ' . $sql . ' ' . implode(' ', $params) . ' ' . $types);
				throw new Exception('Error querying the database. ' . $ex->getMessage() . ' ' . $sql . ' ' . implode(' ', $params) . ' ' . $types);
			}
			return $stmt;
		}
	}

	$db = new Database();