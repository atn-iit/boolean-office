<?php
	if (basename($_SERVER['PHP_SELF']) === basename(__FILE__)) {
		header($_SERVER["SERVER_PROTOCOL"] . " 403 Forbidden");
		exit('Direct access not allowed');
	}
	
	class PageConfig
	{
		static string $leave_application_url = 'leave-management';
		static string $library_management_url = 'library';
		static string $file_tracking_url = 'file-tracking';
	}
