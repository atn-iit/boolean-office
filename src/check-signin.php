<?php
	if (basename($_SERVER['PHP_SELF']) === basename(__FILE__)) {
		header($_SERVER["SERVER_PROTOCOL"] . " 403 Forbidden");
		exit('Direct access not allowed');
	}

	/*
	A single session will store
	id				=> user role specific table id
	office_id => Office where the user located
	user_id 	=> user id from user table
	user_role => User role as string
	user_name => User real name
	 */

	function checkIsSignIn(): bool
	{
		$sessionValues = ['id', 'office_id', 'user_id', 'user_role', 'user_name'];
		foreach ($sessionValues as $key => $value) {
			if (checkSessionValue($value) === FALSE) {
				return FALSE;
			}
		}
		return TRUE;
	}
