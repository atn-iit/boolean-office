<?php
	if (basename($_SERVER['PHP_SELF']) === basename(__FILE__)) {
		header($_SERVER["SERVER_PROTOCOL"] . " 403 Forbidden");
		exit('Direct access not allowed');
	}

	if (!isset($cwd)) {
		$cwd = '';
	}

?>

<footer class="footer border-top mt-2">
	<div class="footer-big footer-bottom">
		<div class="container">
			<div class="row justify-content-center">

				<div class="col-md-3 col-sm-4">
					<div class="footer-widget">
						<div class="footer-menu">
							<h4 class="footer-widget-title">Center/Cell</h4>
							<ul class="m-0 p-0 list-unstyled">
								<li>
									<a href="https://nstu.edu.bd/research-cell" class="footer-link">Research Cell</a>
								</li>
								<li>
									<a href="https://nstu.edu.bd/cyber-center" class="footer-link">Cyber Center</a>
								</li>
								<li>
									<a href="https://www.iqac.nstu.edu.bd/" class="footer-link">IQAC</a>
								</li>
								<li>
									<a href="https://nstu.edu.bd/ict-cell" class="footer-link">ICT Cell</a>
								</li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-md-3 col-sm-4">
					<div class="footer-widget">
						<div class="footer-menu">
							<h4 class="footer-widget-title">Facilities</h4>
							<ul class="m-0 p-0 list-unstyled">
								<li>
									<a href="https://nstu.edu.bd/accommodation" class="footer-link">Hall of Residence</a>
								</li>
								<li>
									<a href="https://nstu.edu.bd/medical-center" class="footer-link">Medical Center</a>
								</li>
								<li>
									<a href="https://nstu.edu.bd/central-library" class="footer-link">Central Library</a>
								</li>
								<li>
									<a href="https://nstu.edu.bd/faculty-auditorium" class="footer-link">Auditorium</a>
								</li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-md-3 col-sm-4">
					<div class="footer-widget">
						<div class="footer-menu">
							<h4 class="footer-widget-title">Contact us</h4>
							<ul class="m-0 p-0 list-unstyled">
								<li>
									<a href="mailto:info.boolean.office@gmail.com" class="footer-link">info.boolean.office@gmail.com</a>
								</li>
							</ul>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="mini-footer footer-bottom">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="copyright-text">
						<p>&copy 2021 IIT NSTU | Designed and Developed by <b><a class="footer-link" href="#">IIT 1<sup>st</sup>
									batch</a></b>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>

<!-- JS FILES -->
<!-- jQuery 3.4.1 -->
<script src="<?= $cwd; ?>js/jquery.min.js"></script>
<!-- Bootstrap bundle js -->
<script src="<?= $cwd; ?>js/jquery.easing.min.js"></script>
<!-- jQuery easing 1.4.1 -->
<script src="<?= $cwd; ?>js/bootstrap.bundle.min.js"></script>
