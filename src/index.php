<?php
	$title = 'Dashboard';

	require_once 'welcome.php';
	require_once 'header.php';
	require_once 'page-config.php';
	require_once 'custom-functions.php';
?>

	<body id="page-top" data-spy="scroll" data-target=".fixed-top">
	<?php require_once('navbar.php'); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="mt-1 pt-1 pl-2 pr-3">
				<div class="row">
					<h1 class="col text-center">Go to</h1>
				</div>

				<div class="row mt-4 mb-3">
					<div class="card-deck mb-3">
						<?php
							if (checkSessionValue('user_role') &&
								($_SESSION['user_role'] === 'Section Officer' || $_SESSION['user_role'] === 'Teacher')) {
								?>
								<div class="card w-100 shadow-lg rounded-xl bg-light text-center">
									<div class="card-body border-0 mt-2 mb-n3">
										<h3 class="pb-2">
											Leave <?= ($_SESSION['user_role'] === 'Teacher' ? 'Application' : 'Management'); ?></h3>
									</div>
									<div class="card-footer">
										<a class="btn btn-block" href="<?php echo PageConfig::$leave_application_url; ?>">Visit here</a>
									</div>
								</div>
							<?php } ?>

						<div class="card w-100 shadow-lg rounded-xl bg-light text-center">
							<div class="card-body border-0 mt-2 mb-n3">
								<h3 class="pb-2">Library <?= ($_SESSION['user_role'] === 'Section Officer' ? 'Management' : ''); ?></h3>
							</div>
							<div class="card-footer">
								<a class="btn btn-block" href="<?php echo PageConfig::$library_management_url; ?>">Visit here</a>
							</div>
						</div>

						<?php
							if (checkSessionValue('user_role') && ($_SESSION['user_role'] === 'Section Officer')) {
								?>
								<div class="card w-100 shadow-lg rounded-xl bg-light text-center">
									<div class="card-body border-0 mt-2 mb-n3">
										<h3 class="pb-2">File Tracking</h3>
									</div>
									<div class="card-footer">
										<a class="btn btn-block" href="<?php echo PageConfig::$file_tracking_url; ?>">Visit here</a>
									</div>
								</div>
							<?php } ?>

					</div>
				</div>

			</div>
		</div>
	</div>
	</body>
<?php require_once('footer.php'); ?>