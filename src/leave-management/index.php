<?php
	$cwd = '../';
	require_once $cwd . 'welcome.php';

	if ($_SESSION['user_role'] === 'Teacher') {
		header('location:show_teacher_profile.php');
	} else if ($_SESSION['user_role'] === 'Section Officer') {
		header('location:leave_applications.php');
	} else {
		die("Invalid access");
	}

	// $res = mysqli_query($con, "select * from department order by id desc");
?>
