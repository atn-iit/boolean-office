<?php
	$title = "Leave type";

	$cwd = '../';
	require_once $cwd . 'welcome.php';

	if ($_SESSION['user_role'] !== 'Section Officer') {
		header('location: index.php');
	}

	require_once 'header.php';

	$user_id = $_SESSION['user_id'];
	$officer_id = $_SESSION['id'];
	$curr_office_id = $_SESSION['office_id'];

// $leave_type = '';
// $id = '';
// if (isset($_GET['id'])) {
// 	$id = mysqli_real_escape_string($con, $_GET['id']);
// 	$res = mysqli_query($con, "select * from leave_type where id='$id'");
// 	$row = mysqli_fetch_assoc($res);
// 	$leave_type = $row['leave_type'];
// }
// if (isset($_POST['leave_type'])) {
// 	$leave_type = mysqli_real_escape_string($con, $_POST['leave_type']);
// 	if ($id > 0) {
// 		$sql = "update leave_type set leave_type='$leave_type' where id='$id'";
// 	} else {
// 		$sql = "insert into leave_type(leave_type) values('$leave_type')";
// 	}
// 	mysqli_query($con, $sql);
// 	echo "<script>location.href='leave_type.php';</script>";
// }

	if (checkIsPOSTS(['leaveTypeSubmit'])) {
		$supported_exts = ['pdf', 'doc', 'docx'];
		$sessdata = array();
		if (checkIsPOSTS(['type_title', 'type_name'])) {
			// Check whether the type name is correct
			$type_name = $_POST['type_name'];
			if ($type_name === 'GO' || $type_name === 'NOC' || $type_name === 'Sabbatical') {
				// Checking whether a file is provided
				if (isset($_FILES['document']) and $_FILES['document']['error'] === 0) {
					$download_path = NULL;
					$doneFile = TRUE;
					$fileMessage = '';

					$file = $_FILES['document']['name'];
					// if (preg_match('/^[^.][-a-z0-9_.\(\)]+[a-z]$/i', $file)) {
					$temp = explode('.', basename($file));
					$ext = end($temp);
					if (in_array($ext, $supported_exts)) {
						$fileSize = $_FILES['document']['size'];
						if ($fileSize < 10000000) { // 10MB
							$tmpName = $_FILES['document']['tmp_name'];

							$newName = '';
							$newTempName = $curr_office_id . '-' . $_POST['type_name'] . "." . $ext;

							$newFilePath = 'uploads/';
							error_reporting(0);
							mkdir($newFilePath, 0777, TRUE);
							$newName = $curr_office_id . '_' . $newTempName;
							error_reporting(1);
							
							if (move_uploaded_file($tmpName, 'uploads/' . $newName)) {
								$download_path = $newName;
							} else {
								$doneFile = FALSE;
								$fileMessage = 'Could not store the uploaded file';
							}
						} else {
							$doneFile = FALSE;
							$fileMessage = 'File size is more than 10 MB';
						}
					} else {
						$doneFile = FALSE;
						$fileMessage = 'Given file not supported.<br>Provide ' . implode(' ', $supported_exts) . ' files';
					}
					// } else {
					// $doneFile = FALSE;
					// $fileMessage = 'Invalid file name';
					// }

					if ($doneFile === TRUE) {
						try {
							$query = "INSERT INTO leave_type_info VALUES(NULL,?,?,?,?,?)";
							$params = [$_POST['type_title'], $_POST['type_name'], $download_path, $officer_id, $curr_office_id];
							$stmt = $db->query($query, $params, 'sssii');
							$insertID = $stmt->insert_id;
							$stmt->close();

							if ($insertID > 0) {
								$sessdata['type'] = 'success';
								$sessdata['message'] = 'New leave type added';
							} else {
								$sessdata['type'] = 'danger';
								$sessdata['message'] = 'Can not insert leave type.';
							}
						} catch (Exception $ex) {
							error_log($ex->getMessage());
							$sessdata['type'] = 'danger';
							$sessdata['message'] = 'Query error';
						}
					} else {
						$sessdata['type'] = 'danger';
						$sessdata['message'] = $fileMessage;
					}
				} else {
					$sessdata['type'] = 'warning';
					$sessdata['message'] = 'Please provide the leave format file';
				}
			} else {
				$sessdata['type'] = 'warning';
				$sessdata['message'] = 'Invalid leave type';
			}
		} else {
			$sessdata['type'] = 'warning';
			$sessdata['message'] = 'Please provide all the required fields';
		}
		$_SESSION['sessdata'] = $sessdata;
	}
?>

	<body id="page-top" data-spy="scroll" data-target=".fixed-top">
	<!-- Navigation bar -->
	<?php require_once('navbar.php'); ?>

	<?php
		if (checkSessionValue('sessdata')) {
			$sessdata = $_SESSION['sessdata'];
			?>
			<div class="row no-gutters">
				<div class="col-lg-5 col-md-12 ml-auto">
					<div class="alert alert-<?php echo $sessdata['type']; ?> alert-dismissible fade show"
							 role="alert">
						<strong><?php echo $sessdata['message']; ?></strong>
					</div>
				</div>
			</div>
			<?php
			unset($_SESSION['sessdata']);
		}
	?>

	<!-- Body -->
	<div class="container">
		<div class="row justify-content-center mb-3">
			<div class="mt-2 pt-2 pl-2 pr-3 text-justify">
				<form class="shadow-lg rounded-xl p-5 mb-5 bg-white" action="" method="POST" enctype="multipart/form-data">
					<div class="col-form-label text-center pb-4">
						<h5>Add new leave type</h5>
					</div>

					<div class="form-group form-floating">
						<input type="text" name="type_title" id="type_title" class="form-control" placeholder=""
									 required autofocus>
						<label for="type_title">Type title</label>
					</div>
					
					<div class="form-group form-floating">
						<select class="form-control custom-select" name="type_name" id="type_name" required>
							<option value="" selected disabled hidden>None</option>
							<option value="NOC">NOC</option>
							<option value="GO">GO</option>
							<option value="Sabbatical">Sabbatical</option>
						</select>
						<label for="type_name">Type name</label>
					</div>

					<div class="form-group">
						<label for="document">Upload leave format PDF or Document</label>
						<input class="form-control border-0" type="file" name="document" id="document"
									 accept=".pdf,.doc,.docx" required>
					</div>

					<div class="form-group">
						<input type="submit" class="btn btn-primary btn-block" value="Submit" name="leaveTypeSubmit">
						<a class="btn btn-secondary btn-block" href="leave_type.php">Cancel</a>
					</div>
				</form>
			</div>
		</div>
	</div>

	</body>

<?php
	require('footer.php');
?>