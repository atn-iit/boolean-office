<?php
	$title = "My applications";

	$cwd = '../';
	require_once $cwd . 'welcome.php';

	// Check for user access by user role
	$user_has_access = FALSE;
	if ($_SESSION['user_role'] === 'Teacher') {
		$user_has_access = TRUE;
	}

	if ($user_has_access === FALSE) {
		header('location: index.php');
	}

	// Get session values
	$user_id = $_SESSION['user_id'];
	$teacher_id = $_SESSION['id'];
	$officer_id = $_SESSION['id'];
	$curr_office_id = $_SESSION['office_id'];

	$teacher_has_access = FALSE;

	// Check for teacher access if user is teacher
	if ($_SESSION['user_role'] === 'Teacher') {
		$teacher_role = 'None';
		try {
			$query = "SELECT * FROM teacher_info WHERE idteacher_info=?";
			$stmt = $db->query($query, [$teacher_id], 'i');
			$result = $stmt->get_result();
			$stmt->close();

			if ($result->num_rows > 0) {
				$row = $result->fetch_assoc();
				$teacher_role = $row['teacher_role'];
			}
		} catch (Exception $ex) {
			error_log($ex->getMessage());
		}
		if ($_SESSION['user_role'] === 'Teacher' && ($teacher_role === 'Director' || $teacher_role === 'Chairman')) {
			$teacher_has_access = TRUE;
		}
	}

	require_once 'header.php';
?>

<body class="bg-white" id="page-top" data-spy="scroll" data-target=".fixed-top">
<?php
	require_once('navbar.php');

	if (checkSessionValue('sessdata')) {
		$sessdata = $_SESSION['sessdata'];
		?>
		<div class="row no-gutters">
			<div class="col-lg-5 col-md-12 ml-auto">
				<div class="alert alert-<?php echo $sessdata['type']; ?> alert-dismissible fade show"
						 role="alert">
					<strong><?php echo $sessdata['message']; ?></strong>
				</div>
			</div>
		</div>
		<?php
		unset($_SESSION['sessdata']);
	}
?>

<div class="container">
	<div class="row mb-3">
		<div class="col mt-2 pt-2 pl-2 pr-3 text-justify">
			<div class="text-center">
				<h4>Leave Applications</h4>
			</div>
				<table id="table-applications" class="table table-bordered table-condensed text-center">
					<thead class="thead-light">
					<tr>
						<th>Applied at</th>
						<th>Applied for</th>
						<th>From</th>
						<th>To</th>
						<th>Name</th>
						<th>Mobile</th>
						<th>File</th>
						<th>Leave Status</th>
					</tr>
					</thead>
					<tbody>
					<?php
						try {
							$query = "SELECT * FROM leave_apply_info l, leave_type_info li, teacher_info t, user_info u 
											WHERE l.teachers_info_idteachers_info=t.idteacher_info 
												AND t.user_info_iduser_info=u.iduser_info 
												AND li.idleave_type_info=l.leave_type_info_idleave_type_info 
												AND t.office_info_idoffice_info=?";
							$params = [];
							$param_str = "";

							if ($_SESSION['user_role'] === 'Teacher') {
								$query .= " AND t.idteacher_info=?";
								$params = [$curr_office_id, $teacher_id];
								$param_str = "ii";
							}

							$stmt = $db->query($query, $params, $param_str);
							$result = $stmt->get_result();
							$stmt->close();
						} catch (Exception $ex) {
							error_log($ex->getMessage());
						}
						if ($result->num_rows > 0) {
						while ($row = $result->fetch_assoc()) { ?>
							<tr>
								<td><?= $row['leave_applied_at'] ?></td>
								<td><?= $row['leave_type_name'] ?></td>
								<td><?= $row['leave_apply_from'] ?></td>
								<td><?= $row['leave_apply_to'] ?></td>
								<td><?= $row['user_name'] ?></td>
								<td><?= $row['user_mobile'] ?></td>
								<td><a href="uploads/applications/<?= $row['leave_apply_pdf'];?>" target="blank"><img
											src="../res/pdf.png" width="32px" alt="pdf"></a></td>
								<td><?= $row['leave_apply_status'] === "Created" ? "Pending" : $row['leave_apply_status']  ?></td>
							</tr>
						<?php } ?>
						<?php } else { ?>
							<tr><td colspan="8">No applications</td></tr>
						<?php
						}
					?>
					</tbody>
				</table>
			</form>
		</div>
	</div>
</div>
</body>

<script>
	document.addEventListener('DOMContentLoaded', function () {
		let table1 = new DataTable('#table-applications', {"order": [[0, "desc"]]});
	});
</script>

<?php require('footer.php'); ?>
