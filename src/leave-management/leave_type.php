<?php
$title = "Leave type";

$cwd = '../';
require_once $cwd . 'welcome.php';

// Check for user access by user role
$user_has_access = FALSE;
if ($_SESSION['user_role'] === 'Section Officer' || $_SESSION['user_role'] === 'Teacher') {
	$user_has_access = TRUE;
}

if (!$user_has_access) {
	header('location: index.php');
}

// Get session values
$user_id = $_SESSION['user_id'];
$teacher_id = $_SESSION['id'];
$officer_id = $_SESSION['id'];
$curr_office_id = $_SESSION['office_id'];

$teacher_has_access = FALSE;
if ($_SESSION['user_role'] === 'Teacher') {
	$teacher_has_access = TRUE;
}

require_once 'header.php';

if (!$teacher_has_access && checkIsPOSTS(['id', 'action'])) {
	$sessdata = array();
	$status = '';
	if ($_POST['action'] === 'edit') {
		// $status = 'Approved';
	} else if ($_POST['action'] === 'delete') {
		$query = "SELECT * FROM leave_type_info WHERE idleave_type_info=?";
		try {
			$stmt = $db->query($query, [$_POST['id']], 'i');
			$result = $stmt->get_result();
			$stmt->close();

			if ($result->num_rows > 0) {
				// Delete leave type
				$query = "DELETE FROM leave_type_info WHERE idleave_type_info=?";
				$stmt = $db->query($query, [$_POST['id']], 'i');
				$affected_rows = $stmt->affected_rows;
				$stmt->close();

				if ($affected_rows > 0) {
					$sessdata['type'] = 'success';
					$sessdata['message'] = 'Removed leave type.';
				} else {
					$sessdata['type'] = 'warning';
					$sessdata['message'] = 'Can not delete leave type.';
				}
			} else {
				$sessdata['type'] = 'warning';
				$sessdata['message'] = 'Invalid leave type';
			}
		} catch (Exception $ex) {
			error_log($ex->getMessage());
			$sessdata['type'] = 'danger';
			$sessdata['message'] = 'Query error!!!<br>Can not perform the requested action.';
		}
	}
	$_SESSION['sessdata'] = $sessdata;
}
?>

<body class="bg-white" id="page-top" data-spy="scroll" data-target=".fixed-top">
	<?php
	require_once('navbar.php');

	if (checkSessionValue('sessdata')) {
		$sessdata = $_SESSION['sessdata'];
	?>
		<div class="row no-gutters">
			<div class="col-lg-5 col-md-12 ml-auto">
				<div class="alert alert-<?php echo $sessdata['type']; ?> alert-dismissible fade show" role="alert">
					<strong><?php echo $sessdata['message']; ?></strong>
				</div>
			</div>
		</div>
	<?php
		unset($_SESSION['sessdata']);
	}
	?>

	<div class="container">
		<div class="row mb-3">
			<div class="col mt-2 pt-2 pl-2 pr-3 text-justify">
				<div class="text-center">
					<h4>Leave Types</h4>
				</div>

				<?php if (!$teacher_has_access) { ?>
					<form id="form-applications" class="p-5 mb-5" action="leave_type.php" method="post">
						<input type="hidden" name="id" id="id">
						<input type="hidden" name="action" id="action">
					<?php } ?>

					<table id="table-applications" class="table table-bordered table-condensed text-center">
						<thead class="thead-light">
							<tr>
								<th>Leave Title</th>
								<th>Leave Type</th>
								<th></th>

								<?php if (!$teacher_has_access) { ?>
									<th></th>
								<?php } ?>
							</tr>
						</thead>
						<tbody>

							<?php
							$query = "SELECT * FROM leave_type_info";// WHERE officer_info_office_info_idoffice_info=?";
							try {
								$stmt = $db->query($query);//, [$curr_office_id], 'i');
								$result = $stmt->get_result();
								$stmt->close();

								if ($result->num_rows > 0) {
									while ($row = $result->fetch_assoc()) {
							?>
										<tr>
											<td><?= $row['leave_type_title'] ?></td>
											<td><?= $row['leave_type_name'] ?></td>
											<td><a href="uploads/<?php echo $row['leave_type_pdf'] ?>" target="blank"><img src="../res/pdf.png" width="32px" alt="pdf"></a></td>
											<!-- <td><button type='button' class='btn bg-light' -->
											<!-- onclick="postLeaveInfo(<?php //echo '\'' . $row['idleave_type_info'] . '\',\'edit\'' 
																									?>,this)"><span class='edit-icon mr-1'></span></button></td> -->

											<?php if (!$teacher_has_access) { ?>
												<td>
													<button type='button' class='btn bg-light' onclick="postLeaveInfo(<?= '\'' . $row['idleave_type_info'] . '\',\'delete\'' ?>,this)">
														<span class='delete-icon mr-1'></span></button>
												</td>
											<?php } ?>
										</tr>
									<?php }
									if (!$teacher_has_access) { ?>

										<script>
											function postLeaveInfo(id, action, item) {
												if (id.trim() !== null && action.trim() !== null) {
													if (action.trim() === "delete") {
														if (!confirm('Are you sure?')) {
															item.preventDefault();
															return;
														}
													}
													$('#id').val(id);
													$('#action').val(action);
													$('#form-applications').submit();
												}
											}
										</script>

							<?php
									}
								} else {
									echo "<tr><td colspan='4'>No leave type found";
									if ($_SESSION['user_role'] === 'Section Officer') {
										echo "<a href='add_leave_type.php'>Add new leave type</a>";
									}
									echo "</td></tr>";
								}
							} catch (Exception $ex) {
								error_log($ex->getMessage());
								echo "<tr><td colspan='4'>Something went wrong while fetching the leave type info</td></tr>";
							}
							?>
						</tbody>
					</table>
					<?php if (!$teacher_has_access) { ?>
					</form>
				<?php } ?>
			</div>
		</div>
	</div>
</body>

<script>
	document.addEventListener('DOMContentLoaded', function () {
		let table1 = new DataTable('#table-applications');
	});
</script>

<?php require('footer.php'); ?>
