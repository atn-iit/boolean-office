<?php
$title = "Teacher Profile";

$cwd = '../';
require_once $cwd . 'welcome.php';

if ($_SESSION['user_role'] !== 'Teacher') {
	header('Location: index.php');
}

require_once 'header.php';

$user_id = $_SESSION['user_id'];
$teacher_id = $_SESSION['id'];
$curr_office_id = $_SESSION['office_id'];

try {
	$query = "SELECT * FROM teacher_info t, user_info u WHERE u.iduser_info=? AND u.iduser_info=t.user_info_iduser_info AND u.user_role='Teacher'";
	$stmt = $db->query($query, [$user_id], "i");
	$result = $stmt->get_result();
	$stmt->close();

	if ($result->num_rows > 0) {
		$row = $result->fetch_assoc();

		$name = $row['user_name'];
		$email = $row['user_email'];
		$mobile = $row['user_mobile'];
		$designation = $row['teacher_designation'];
	} else {
		die('Unauthorized teacher.');
	}
} catch (Exception $ex) {
	error_log($ex->getMessage());
	die('Unauthorized teacher.');
}
?>

<body class="bg-white" id="page-top" data-spy="scroll" data-target=".fixed-top">
	<?php
	require_once('navbar.php');

	if (checkSessionValue('sessdata')) {
		$sessdata = $_SESSION['sessdata'];
	?>
		<div class="row no-gutters">
			<div class="col-lg-5 col-md-12 ml-auto">
				<div class="alert alert-<?php echo $sessdata['type']; ?> alert-dismissible fade show" role="alert">
					<strong><?php echo $sessdata['message']; ?></strong>
				</div>
			</div>
		</div>
	<?php
		unset($_SESSION['sessdata']);
	}
	?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="card-deck mb-3 col-md-8 mt-4">
				<div class="card w-50 shadow rounded">
					<div class="card-header">
						<h5 class="card-title text-center">Teacher's Profile</h5>
					</div>

					<?php
					function getOfficeName($db, $id): string
					{
						$ret = $id . "";
						try {
							$query = "SELECT * FROM office_info WHERE idoffice_info=?";
							$stmt = $db->query($query, [$id], "i");
							$result = $stmt->get_result();
							$stmt->close();

							$row = $result->fetch_assoc();
							$ret = $row['office_name'];
						} catch (Exception $ex) {
							error_log($ex);
						}
						return $ret;
					}
					?>
					<div class="card-body border-0">
						<table class="table">
							<thead>
								<tr>
									<td class="border-0 text-center" colspan="2">Teacher of '<?= getOfficeName($db, $row['office_info_idoffice_info']); ?>'</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-right border-0 w-50 font-weight-bold">Full Name</td>
									<td class="border-0 w-50"><?= $name ?></td>
								</tr>
								<tr>
									<td class="text-right border-0 w-50 font-weight-bold">Email</td>
									<td class="border-0 w-50"><?= $email ?></td>
								</tr>
								<tr>
									<td class="text-right border-0 w-50 font-weight-bold">Phone</td>
									<td class="border-0 w-50"><?= $mobile ?></td>
								</tr>
								<tr>
									<td class="text-right border-0 w-50 font-weight-bold">Designation</td>
									<td class="border-0 w-50"><?= $designation ?> <?= $row['teacher_role'] === 'None' ? "" : "(" . $row['teacher_role'] . ")" ?></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

<?php require_once 'footer.php'; ?>