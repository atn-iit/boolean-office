<?php
	if (basename($_SERVER['PHP_SELF']) === basename(__FILE__)) {
		header($_SERVER["SERVER_PROTOCOL"] . " 403 Forbidden");
		exit('Direct access not allowed');
	}

	function createNavItem($title, $titleTxt, $linkTxt)
	{
		$activeTxt = "";
		$tmpLink = $linkTxt;

		if (isset($title)) {
			if ($title === $titleTxt) {
				$activeTxt = "active";
				$tmpLink = '';
			}
		}

		echo "
    <li class='nav-item $activeTxt'>
      <a class='nav-link' href='$tmpLink'>$titleTxt</a>
    </li>
  ";
	}

	if ($_SESSION['user_role'] === 'Teacher') {
		$teacher_role = 'None';
		try {
			$query = "SELECT * FROM teacher_info WHERE idteacher_info=?";
			$stmt = $db->query($query, [$teacher_id], 'i');
			$result = $stmt->get_result();
			$stmt->close();

			if ($result->num_rows > 0) {
				$row = $result->fetch_assoc();
				$teacher_role = $row['teacher_role'];
			}
		} catch (Exception $ex) {
			error_log($ex->getMessage());
		}
	}
?>

<nav class="navbar navbar-expand-lg navbar-light fixed-top border-bottom bg-light">
	<div class="container">
		<a class="navbar-brand page-scroll" href="#page-top">
			<img src="../res/nstulogo.gif" alt="NSTU" height="32px" width="32px"><span class="brand-name">NSTU</span>
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
						data-target="#navbarSupportedContent"
						aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav ml-auto mt-2 mt-lg-0">
				<!-- Dashboard -->
				<?php createNavItem($title, "Dashboard", "index.php"); ?>
				<!-- Goto -->
				<?php
					if (checkSessionValue('user_role') &&
						($_SESSION['user_role'] === 'Section Officer' || $_SESSION['user_role'] === 'Teacher')) {
						?>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="carrierDropdown" role="button" data-toggle="dropdown"
								 aria-haspopup="true" aria-expanded="false">Go to
							</a>
							<div class="dropdown-menu dropdown-menu-right rounded-xl" aria-labelledby="carrierDropdown">

								<?php
									if (checkSessionValue('user_role') && ($_SESSION['user_role'] === 'Section Officer')) {
										?>
										<a class="dropdown-item" href="../file-tracking/index.php">File Tracking</a>
									<?php } ?>
								<a class="dropdown-item"
									 href="../library/index.php">Library <?= ($_SESSION['user_role'] === 'Teacher' ? '' : 'Management'); ?></a>
							</div>
						</li>
					<?php } ?>

				<?php
					if ($_SESSION['user_role'] === 'Section Officer') {
						?>
						<!-- Applications -->
						<?php createNavItem($title, "Applications", "leave_applications.php"); ?>
						<!-- Leave Type -->
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="carrierDropdown" role="button" data-toggle="dropdown"
								 aria-haspopup="true" aria-expanded="false">
								Leave type
							</a>
							<div class="dropdown-menu dropdown-menu-right rounded-xl" aria-labelledby="carrierDropdown">
								<a class="dropdown-item" href="add_leave_type.php">Add new leave type</a>
								<a class="dropdown-item" href="leave_type.php">Manage leave type</a>
							</div>
						</li>
					<?php } else if ($_SESSION['user_role'] === 'Teacher') { ?>
						<?php 
						if ($teacher_role === 'Director' || $teacher_role === 'Chairman') {
							// Applications
							createNavItem($title, "Applications", "leave_applications.php");
						}

						createNavItem($title, "My applications", "teacher_applications.php");
						?>

						<!-- Apply for leave -->
						<?php createNavItem($title, "Apply for leave", "apply_leave.php"); ?>
						<!-- View formats -->
						<?php createNavItem($title, "View available formats", "leave_type.php"); ?>
					<?php } ?>
				<!-- Profile -->
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
						 aria-haspopup="true" aria-expanded="false">
						<!-- Show username -->
						<?php
							$isuser = isset($_SESSION['user_name']);
							if ($isuser) {
								$user = $_SESSION['user_name'];
								if (!empty($user)) {
									echo $user;
									unset($user);
								} else echo "Profile";
							} else echo "Profile";
							unset($isuser);
						?>
					</a>
					<div class="dropdown-menu dropdown-menu-right rounded-xl" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="../signout.php">Sign Out</a>
					</div>
				</li>
			</ul>
		</div>
	</div>
</nav>
