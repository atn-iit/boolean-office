<?php
	$title = "Applications";

	$cwd = '../';
	require_once $cwd . 'welcome.php';

	// Check for user access by user role
	$user_has_access = FALSE;
	if ($_SESSION['user_role'] === 'Section Officer' || $_SESSION['user_role'] === 'Teacher') {
		$user_has_access = TRUE;
	}

	if ($user_has_access === FALSE) {
		header('location: index.php');
	}

	// Get session values
	$user_id = $_SESSION['user_id'];
	$teacher_id = $_SESSION['id'];
	$officer_id = $_SESSION['id'];
	$curr_office_id = $_SESSION['office_id'];

	$teacher_has_access = FALSE;

	// Check for teacher access if user is teacher
	if ($_SESSION['user_role'] === 'Teacher') {
		$teacher_role = 'None';
		try {
			$query = "SELECT * FROM teacher_info WHERE idteacher_info=?";
			$stmt = $db->query($query, [$teacher_id], 'i');
			$result = $stmt->get_result();
			$stmt->close();

			if ($result->num_rows > 0) {
				$row = $result->fetch_assoc();
				$teacher_role = $row['teacher_role'];
			}
		} catch (Exception $ex) {
			error_log($ex->getMessage());
		}
		if ($_SESSION['user_role'] === 'Teacher' && ($teacher_role === 'Director' || $teacher_role === 'Chairman')) {
			$teacher_has_access = TRUE;
		}
	}

	if ($_SESSION['user_role'] === 'Teacher' && !$teacher_has_access) {
		header("Location: teacher_applications.php");
	}

	require_once 'header.php';

	if (checkIsPOSTS(['id', 'action'])) {
		$sessdata = array();
		$status = '';
		if ($_POST['action'] === 'approve') {
			$status = 'Approved';
		} else if ($_POST['action'] === 'reject') {
			$status = 'Not Approved';
		}

		if (!empty($status)) {
			// Check leave application
			$query = "SELECT * FROM leave_apply_info WHERE idleave_apply_info=? AND leave_apply_status='Created'";
			try {
				$stmt = $db->query($query, [$_POST['id']], 'i');
				$result = $stmt->get_result();
				$stmt->close();

				if ($result->num_rows > 0) {
					// Update the status
					$query = "UPDATE leave_apply_info SET leave_apply_status=? WHERE idleave_apply_info=?";
					$stmt = $db->query($query, [$status, $_POST['id']], 'si');
					$affected_rows = $stmt->affected_rows;
					$stmt->close();

					if ($affected_rows > 0) {
						$sessdata['type'] = 'success';
						$sessdata['message'] = 'Updated application status.';
					} else {
						$sessdata['type'] = 'warning';
						$sessdata['message'] = 'Can not update status.';
					}
				} else {
					$sessdata['type'] = 'warning';
					$sessdata['message'] = 'Invalid application';
				}
			} catch (Exception $ex) {
				error_log($ex->getMessage());
				$sessdata['type'] = 'danger';
				$sessdata['message'] = 'Query error!!!<br>Can not update application status.';
			}
		} else {
			$sessdata['type'] = 'warning';
			$sessdata['message'] = 'Invalid action';
		}
		$_SESSION['sessdata'] = $sessdata;
	}

?>

<body class="bg-white" id="page-top" data-spy="scroll" data-target=".fixed-top">
<?php
	require_once('navbar.php');

	if (checkSessionValue('sessdata')) {
		$sessdata = $_SESSION['sessdata'];
		?>
		<div class="row no-gutters">
			<div class="col-lg-5 col-md-12 ml-auto">
				<div class="alert alert-<?php echo $sessdata['type']; ?> alert-dismissible fade show"
						 role="alert">
					<strong><?php echo $sessdata['message']; ?></strong>
				</div>
			</div>
		</div>
		<?php
		unset($_SESSION['sessdata']);
	}
?>

<div class="container">
	<div class="row mb-3">
		<div class="col mt-2 pt-2 pl-2 pr-3 text-justify">
			<div class="text-center">
				<h4>Leave Applications</h4>
			</div>
			<form id="form-applications" class="p-5 mb-5" action="leave_applications.php" method="post">
				<input type="hidden" name="id" id="id">
				<input type="hidden" name="action" id="action">
				<table id="table-applications" class="table table-bordered table-condensed text-center">
					<thead class="thead-light">
					<tr>
						<th>Applied at</th>
						<th>Applied for</th>
						<th>From</th>
						<th>To</th>
						<th>Name</th>
						<th>Mobile</th>
						<th>File</th>
						<th>Leave Status</th>
						<?php if ($teacher_has_access) { ?>
							<th></th>
							<th></th>
						<?php } ?>
					</tr>
					</thead>
					<tbody>
					<?php
						try {
							$query = "SELECT * FROM leave_apply_info l, leave_type_info li, teacher_info t, user_info u 
											WHERE l.teachers_info_idteachers_info=t.idteacher_info 
												AND t.user_info_iduser_info=u.iduser_info 
												AND li.idleave_type_info=l.leave_type_info_idleave_type_info 
												AND t.office_info_idoffice_info=?";
							$params = [];
							$param_str = "";

							$params = [$curr_office_id];
							$param_str = "i";
							
							// if ($_SESSION['user_role'] === 'Teacher') {
							// 	$query .= " AND t.idteacher_info=?";
							// 	$params = [$curr_office_id, $teacher_id];
							// 	$param_str = "ii";
							// }
							if ($teacher_has_access) {
								$query .= " AND t.teacher_role='None'";
							}

							$stmt = $db->query($query, $params, $param_str);
							$result = $stmt->get_result();
							$stmt->close();
						} catch (Exception $ex) {
							error_log($ex->getMessage());
						}
						if ($result->num_rows > 0) {
						while ($row = $result->fetch_assoc()) { ?>
							<tr>
								<td><?= $row['leave_applied_at'] ?></td>
								<td><?= $row['leave_type_name'] ?></td>
								<td><?= $row['leave_apply_from'] ?></td>
								<td><?= $row['leave_apply_to'] ?></td>
								<td><?= $row['user_name'] ?></td>
								<td><?= $row['user_mobile'] ?></td>
								<td><a href="uploads/applications/<?= $row['leave_apply_pdf'];?>" target="blank"><img
											src="../res/pdf.png" width="32px" alt="pdf"></a></td>
								<td><?= $row['leave_apply_status'] === "Created" ? "Pending" : $row['leave_apply_status']  ?></td>
								<?php
									if ($teacher_has_access && ($row['leave_apply_status'] !== 'Approved' && $row['leave_apply_status'] !== 'Not Approved')) {
										?>
										<td>
											<button type='button' class='btn bg-primary text-white'
															onclick="postApplicationInfo(<?= '\'' . $row['idleave_apply_info'] . '\',\'approve\''; ?>,this)">
												Approve
											</button>
										</td>
										<td>
											<button type='button' class='btn bg-danger text-white'
															onclick="postApplicationInfo(<?= '\'' . $row['idleave_apply_info'] . '\',\'reject\''; ?>,this)">
												Reject
											</button>
										</td>
									<?php
									} else { 
										// echo "<td colspan='2'></td>";
									}
									?>
							</tr>
						<?php } ?>

							<script>
								function postApplicationInfo(id, action, item) {
									if (id.trim() !== null && action.trim() !== null) {
										if (action.trim() === "reject") {
											if (!confirm('Are you sure?')) {
												item.preventDefault();
												return;
											}
										}
										$('#id').val(id);
										$('#action').val(action);
										$('#form-applications').submit();
									}
								}
							</script>
						<?php } else { ?>
							<tr><td colspan="<?= $teacher_has_access ? '10' : '8'; ?>">No applications</td></tr>
						<?php
						}
					?>
					</tbody>
				</table>
			</form>
		</div>
	</div>
</div>
</body>

<script>
	document.addEventListener('DOMContentLoaded', function () {
		let table1 = new DataTable('#table-applications', {"order": [[0, "desc"]]});
	});
</script>

<?php require('footer.php'); ?>
