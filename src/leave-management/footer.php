<?php
	if (basename($_SERVER['PHP_SELF']) === basename(__FILE__)) {
		header($_SERVER["SERVER_PROTOCOL"] . " 403 Forbidden");
		exit('Direct access not allowed');
	}

	require_once '../common-footer.php';
?>

<!-- DataTable -->
<script src="../vendor/DataTable/jquery.dataTables.min.js"></script>
<script src="../vendor/DataTable/dataTables.bootstrap4.min.js"></script>
<script src="../vendor/DataTable/dataTables.responsive.min.js"></script>
<script src="../vendor/DataTable/dataTables.searchBuilder.min.js"></script>

<!-- Custom JS -->
<script src="../js/customjs.js"></script>

<script>
$(".alert-dismissible").fadeTo(4000, 500).slideUp(500, function() {
	$(".alert-dismissible").alert('close');
});

$(document).on("click", "#callDispatchModal", function() {
	let row = $(this).parent('td').parent('tr');
	let fileNumber = $(row).find('td').eq(2).html();
	let fileName = $(row).find('td').eq(3).html();
	$(".modal-body #file_name").val(fileName);
	$(".modal-body #file_number").val(fileNumber);
});
</script>

</html>