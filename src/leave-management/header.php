<?php
	if (basename($_SERVER['PHP_SELF']) === basename(__FILE__)) {
		header($_SERVER["SERVER_PROTOCOL"] . " 403 Forbidden");
		exit('Direct access not allowed');
	}
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">

	<!-- Metadata -->
	<meta name="application-name" content="Boolean Office - Leave Application">
	<meta name="keywords" content="IIT,Office,Automation,Leave-Application">

	<!-- Page resizing -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Page icon -->
	<link rel="icon" href="../res/nstu.png" type="image/png">
	<link rel="shortcut icon" href="../res/favicon.ico" type="image/jpg"/>

	<!-- Bootstrap core 4.4.1 -->
	<link rel="stylesheet" href="../css/bootstrap.min.css">

	<!-- DataTable -->
	<link rel="stylesheet" href="../vendor/DataTable/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" href="../vendor/DataTable/responsive.bootstrap4.min.css">
	<link rel="stylesheet" href="../vendor/DataTable/searchBuilder.bootstrap4.min.css">

	<!-- Custom stylesheet -->
	<link rel="stylesheet" href="../css/style.css">

	<!-- Font -->
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
	<!-- <link rel="stylesheet" href="assets/css/new_style.css"> -->

	<title><?php if (isset($title)) echo $title . ' | '; ?>Leave Application</title>
</head>
