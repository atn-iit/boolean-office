<?php
$title = "Apply for Leave";

$cwd = '../';
require_once $cwd . 'welcome.php';

// Check for user access by user role
$user_has_access = FALSE;
if ($_SESSION['user_role'] === 'Teacher') {
	$user_has_access = TRUE;
}

if ($user_has_access === FALSE) {
	header('location: index.php');
}

require_once 'header.php';

// Get session values
$user_id = $_SESSION['user_id'];
$teacher_id = $_SESSION['id'];
$curr_office_id = $_SESSION['office_id'];

$teacher_can_apply = TRUE;

try {
	$query = "SELECT * FROM leave_apply_info WHERE teachers_info_idteachers_info=? AND leave_apply_status='Created'";
	$stmt = $db->query($query, [$teacher_id], "i");
	$num_rows = $stmt->get_result()->num_rows;
	$stmt->close();

	if ($num_rows > 0) {
		$teacher_can_apply = FALSE;
	}
} catch (Exception $ex) {
	error_log($ex->getMessage());
}

if (isset($_POST['submit']) && $teacher_can_apply) {
	$supported_exts = ['pdf', 'doc', 'docx'];
	$sessdata = array();
	if (checkIsPOSTS(['leave_id', 'leave_from', 'leave_to'])) {
		// Check the leave time
		$leave_from_time = strtotime($_POST['leave_from']);
		$leave_to_time = strtotime($_POST['leave_to']);
		$current_date = strtotime(date('Y-m-d'));

		$time_is_ok = TRUE;

		if ($leave_from_time < $current_date) {
			$time_is_ok = FALSE;
		} else {
			if ($leave_from_time >= $leave_to_time) {
				$time_is_ok = FALSE;
			}
		}

		if ($time_is_ok) {
			// Checking whether a file is provided
			if (isset($_FILES['pdf']) and $_FILES['pdf']['error'] === 0) {
				$download_path = NULL;
				$doneFile = TRUE;
				$fileMessage = '';

				$file = $_FILES['pdf']['name'];
				// if (preg_match('/^[^.][-a-z0-9_.\(\)]+[a-z]$/i', $file)) {
				$temp = explode('.', basename($file));
				$ext = end($temp);
				if (in_array($ext, $supported_exts)) {
					$fileSize = $_FILES['pdf']['size'];
					if ($fileSize < 10000000) { // 10MB
						$tmpName = $_FILES['pdf']['tmp_name'];

						$newName = '';
						$newTempName = $curr_office_id . '-' . $teacher_id . '-' . date('Y-m-d H-i-s', time()) . "." . $ext;

						$newFilePath = 'uploads/applications/';
						error_reporting(0);
						mkdir($newFilePath, 0777, TRUE);
						$newName = $newTempName;
						error_reporting(1);

						if (move_uploaded_file($tmpName, $newFilePath . $newName)) {
							$download_path = $newName;
						} else {
							$doneFile = FALSE;
							$fileMessage = 'Could not store the uploaded file';
						}
					} else {
						$doneFile = FALSE;
						$fileMessage = 'File size is more than 10 MB';
					}
				} else {
					$doneFile = FALSE;
					$fileMessage = 'Given file not supported.<br>Provide ' . implode(' ', $supported_exts) . ' files';
				}
				// } else {
				// $doneFile = FALSE;
				// $fileMessage = 'Invalid file name';
				// }

				if ($doneFile === TRUE) {
					$leave_from = $_POST['leave_from'];
					$leave_to = $_POST['leave_to'];

					try {
						$leave_description = isset($_POST['leave_description']) ? $_POST['leave_description'] : '';
						$query = "INSERT INTO leave_apply_info VALUES (NULL, ?, ?, ?, ?, current_timestamp(), 'Created', ?, ?, ?)";
						$params = [$_POST['leave_from'], $_POST['leave_to'], $leave_description, $download_path, $teacher_id, $curr_office_id, $_POST['leave_id']];
						$stmt = $db->query($query, $params, 'ssssiii');
						$insertID = $stmt->insert_id;
						$stmt->close();

						if ($insertID > 0) {
							$sessdata['type'] = 'success';
							$sessdata['message'] = 'Successfully applied for new leave application';
						} else {
							$sessdata['type'] = 'danger';
							$sessdata['message'] = 'Can not apply for leave application.';
						}
					} catch (Exception $ex) {
						error_log($ex->getMessage());
						$sessdata['type'] = 'danger';
						$sessdata['message'] = 'Query error';
					}
				} else {
					$sessdata['type'] = 'danger';
					$sessdata['message'] = $fileMessage;
				}
			} else {
				$sessdata['type'] = 'warning';
				$sessdata['message'] = 'Please provide the leave application file';
			}
		} else {
			$sessdata['type'] = 'warning';
			$sessdata['message'] = 'Invalid date selected';
		}
	} else {
		$sessdata['type'] = 'warning';
		$sessdata['message'] = 'Please provide all the required fields';
	}
	$_SESSION['sessdata'] = $sessdata;
}
?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
	<!-- Navigation bar -->
	<?php require_once('navbar.php'); ?>

	<?php
	if (checkSessionValue('sessdata')) {
		$sessdata = $_SESSION['sessdata'];
	?>
		<div class="row no-gutters">
			<div class="col-lg-5 col-md-12 ml-auto">
				<div class="alert alert-<?php echo $sessdata['type']; ?> alert-dismissible fade show" role="alert">
					<strong><?php echo $sessdata['message']; ?></strong>
				</div>
			</div>
		</div>
	<?php
		unset($_SESSION['sessdata']);
	}
	?>
	<div class="container">
		<div class="row justify-content-center mb-3">
			<div class="mt-2 pt-2 pl-2 pr-3 text-justify">
				<form class="shadow-lg rounded-xl p-5 mb-5 bg-white" action="" method="POST" enctype="multipart/form-data">
					<div class="col-form-label text-center pb-4">
						<h5>Leave application</h5>
					</div>

					<div class="form-group form-floating">
						<select class="form-control custom-select" name="leave_id" id="leave_id" required 
								<?php if (!$teacher_can_apply) { echo "disabled"; } ?>>
							<option value="" selected disabled hidden>None</option>
							<?php
							try {
								$query = "SELECT * FROM leave_type_info";
								$stmt = $db->query($query);
								$result = $stmt->get_result();
								if ($result->num_rows > 0) {
									while ($row = $result->fetch_assoc()) {
										echo "<option value=" . $row['idleave_type_info'] . ">" . $row['leave_type_name'] . "</option>";
									}
								}
								$stmt->close();
							} catch (Exception $ex) {
								error_log($ex->getMessage());
							}
							?>
						</select>
						<label for="leave_id">Leave Type</label>
					</div>
					<div class="form-group form-floating">
						<input type="date" name="leave_from" id="leave_from" class="form-control" required 
							<?php if (!$teacher_can_apply) { echo "disabled"; } ?> min="<?php echo date("Y-m-d"); ?>">
						<label for="">From Date</label>
					</div>
					<div class="form-group form-floating">
						<input type="date" name="leave_to" id="leave_to" class="form-control" required 
							<?php if (!$teacher_can_apply) { echo "disabled"; } ?> min="<?php echo date("Y-m-d"); ?>">
						<label for="leave_to">To Date</label>
					</div>
					<div class="form-group form-floating">
						<input type="text" name="leave_description" id="leave_description" class="form-control" 
							<?php if (!$teacher_can_apply) { echo "disabled"; } ?>>
						<label for="leave_description">Leave Description</label>
					</div>
					<div class="form-group">
						<label for="pdf">Application File</label>
						<input type="file" name="pdf" id="pdf" class="form-control border-0" 
							accept=".pdf,.doc,.docx" required <?php if (!$teacher_can_apply) { echo "disabled"; } ?>>
					</div>

					<div class="pb-2">
						<span class="small text-danger">
							<?php
							if (!$teacher_can_apply) {
								echo "You already applied for a leave";
							}
							?>
						</span>
					</div>

					<button type="submit" name="submit" class="btn btn-primary btn-block" 
						<?php if (!$teacher_can_apply) { echo "disabled"; } ?>>Submit</button>
				</form>

			</div>
		</div>
	</div>
</body>

<?php require('footer.php'); ?>
