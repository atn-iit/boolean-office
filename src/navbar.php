<?php
	if (basename($_SERVER['PHP_SELF']) === basename(__FILE__)) {
		header($_SERVER["SERVER_PROTOCOL"] . " 403 Forbidden");
		exit('Direct access not allowed');
	}

	function createNavItem($title, $titleTxt, $linkTxt)
	{
		$activeTxt = "";
		$tmpLink = $linkTxt;

		if (isset($title)) {
			if ($title === $titleTxt) {
				$activeTxt = "active";
				$tmpLink = '';
			}
		}

		echo "
    <li class='nav-item $activeTxt'>
      <a class='nav-link' href='$tmpLink'>$titleTxt</a>
    </li>
  ";
	}

?>

<nav class="navbar navbar-expand-lg navbar-light fixed-top border-bottom bg-light">
	<div class="container">
		<a class="navbar-brand page-scroll" href="#page-top">
			<img src="res/nstulogo.gif" alt="NSTU" height="32px" width="32px"><span class="brand-name">NSTU</span>
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
						data-target="#navbarSupportedContent"
						aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse ml-5 pl-5" id="navbarSupportedContent">
			<ul class="navbar-nav ml-auto mt-2 mt-lg-0">
				<!-- Profile -->
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
						 aria-haspopup="true" aria-expanded="false">
						<!-- Show username -->
						<?php
							$isuser = isset($_SESSION['user_name']);
							if ($isuser) {
								$user = $_SESSION['user_name'];
								if (!empty($user)) {
									echo $user;
									unset($user);
								} else echo "Profile";
							} else echo "Profile";
							unset($isuser);
						?>
					</a>
					<div class="dropdown-menu dropdown-menu-right rounded-xl" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="signout.php">Sign Out</a>
					</div>
				</li>
			</ul>
		</div>
	</div>
</nav>