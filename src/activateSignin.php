<?php

	$title = "Activate";
	require_once 'header.php';

	session_start();
	require_once 'custom-functions.php';
	require_once 'dbconn.php';

	if (isset($_POST['activateSubmit'])) {
		$sessdata = array();
		if (checkIsPOSTS(['user_email', 'user_mobile', 'user_password', 'user_confirm_password', 'user_id'])) {
			// Check email address
			if (filter_var($_POST['user_email'], FILTER_VALIDATE_EMAIL)) {
				// Check initial user
				$query = "SELECT * FROM user_info WHERE user_email=? AND user_mobile=? AND user_password=? AND user_status='New'";
				$stmt = $db->query($query, [$_POST['user_email'], $_POST['user_mobile'], $_POST['user_id']], "sss");
				$result = $stmt->get_result();
				$stmt->close();

				if ($result->num_rows === 1) {
					$row = $result->fetch_assoc();
					$user_id = $row['iduser_info'];
					$password = $_POST['user_password'];
					$confirm_password = $_POST['user_confirm_password'];

					if ($password === $confirm_password) {
						// Update password and status
						$user_password = md5($password);
						$query = "UPDATE user_info SET user_password=?,user_status='Active' WHERE iduser_info=?";
						$stmt = $db->query($query, [$user_password, $user_id], "si");
						$affected_rows = $stmt->affected_rows;
						$stmt->close();

						if ($affected_rows > 0) {
							$sessdata['type'] = 'success';
							$sessdata['message'] = 'Your account activated successfully<br>You can signin with your password';
						} else {
							$sessdata['type'] = 'danger';
							$sessdata['message'] = 'Cannot activate the account';
						}
					} else {
						$sessdata['type'] = 'error';
						$sessdata['message'] = 'Confirm password must match with the password.';
					}
				} else {
					$sessdata['type'] = 'danger';
					$sessdata['message'] = 'Invalid account activation request';
				}
			} else {
				$sessdata['type'] = 'warning';
				$sessdata['message'] = 'Please enter a valid email';
			}
		} else {
			$sessdata['type'] = 'warning';
			$sessdata['message'] = 'Please provide all the information requested';
		}
		$_SESSION['sessdata'] = $sessdata;
	}
?>

	<body>
	<div class="container">
		<div class="row justify-content-center">
			<div class="mt-1 pt-1 pl-2 pr-3 text-justify mb-3">
				<form class="shadow-lg rounded-xl p-5 mb-5 bg-white" action="" method="POST">
					<div class="col-form-label text-center pb-4">
						<h4>Activate new signin</h4>
					</div>

					<div class="form-group form-floating">
						<input type="email" name="user_email" id="user_email" class="form-control"
									 placeholder="">
						<label for="user_email">Email address</label>
					</div>

					<div class="form-group form-floating">
						<input type="tel" pattern="0[0-9]{10}" name="user_mobile" id="user_mobile" class="form-control"
									 placeholder="" required>
						<label for="user_mobile">Mobile number</label>
					</div>

					<div class="form-group form-floating">
						<input type="password" name="user_password" id="user_password" class="form-control"
									 placeholder="New Password" autofocus required>
						<label for="user_password">Enter new password</label>
					</div>

					<div class="form-group form-floating">
						<input type="password" name="user_confirm_password" id="user_confirm_password" class="form-control"
									 placeholder="Confirm Password" required>
						<label for="user_confirm_password">Confirm password</label>
					</div>

					<div class="form-group form-floating">
						<input type="text" class="form-control" id="user_id" name="user_id" placeholder=""
									 maxlength="32" required>
						<label for="user_id">Enter ID</label>
					</div>

					<?php
						if (checkSessionValue('sessdata')) {
							$sessdata = $_SESSION['sessdata'];
							?>
							<div class="mt-2 mb-2">
						<span class="text-<?php echo $sessdata['type']; ?>">
							<p class="text-center"><b><?php echo $sessdata['message']; ?></b></p>
						</span>
							</div>
							<?php
							unset($_SESSION['sessdata']);
						}
					?>

					<input type="submit" name="activateSubmit" class="btn btn-primary btn-block" value="Activate">
					<a class="btn btn-secondary btn-block" href="signin.php">Cancel</a>
				</form>
			</div>
		</div>
	</div>
	</body>

	<script>
		let password = document.getElementById("user_password"),
			confirm_password = document.getElementById("user_confirm_password");

		function validatePassword() {
			if (password.value !== confirm_password.value) {
				confirm_password.setCustomValidity("Passwords Don't Match");
			} else {
				confirm_password.setCustomValidity('');
			}
		}

		password.onchange = validatePassword;
		confirm_password.onkeyup = validatePassword;
	</script>

<?php require_once('footer.php'); ?>