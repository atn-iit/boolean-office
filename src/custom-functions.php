<?php
	if (basename($_SERVER['PHP_SELF']) === basename(__FILE__)) {
		header($_SERVER["SERVER_PROTOCOL"] . " 403 Forbidden");
		exit('Direct access not allowed');
	}

	function console_log($data)
	{
		echo '<script>';
		echo 'console.log(' . json_encode($data) . ')';
		echo '</script>';
	}

	function checkSessionValue(?string $value): bool
	{
		return (isset($_SESSION[$value]) and !empty($_SESSION[$value]));
	}

	function checkIsPOST($value): bool
	{
		return (isset($_POST[$value]) && !empty($_POST[$value]));
	}

	function checkIsPOSTS($array): bool
	{
		foreach ($array as $key => $val) {
			if (checkIsPOST($val) === FALSE) {
				return FALSE;
			}
		}

		return TRUE;
	}

