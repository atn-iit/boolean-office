<?php
	$title = "Received Files";
	
	$cwd = '../';
	require_once $cwd . 'welcome.php';
	require_once 'header.php';
	require_once '../custom-functions.php';

	$user_id = $_SESSION['user_id'];
	$officer_id = $_SESSION['id'];
	$curr_office_id = $_SESSION['office_id'];

	if (isset($_POST['submitDispatch'])) {
		$sessdata = array();
		if (checkIsPOSTS(['file_number', 'file_name', 'file_status', 'sending_location', 'file_carrier'])) {
			try {
				// Check file existence
				$query = "SELECT * FROM file_info WHERE file_number=? AND file_name=?";
				$stmt = $db->query($query, [$_POST['file_number'], $_POST['file_name']], "ss");
				$result = $stmt->get_result();
				$stmt->close();

				if ($result->num_rows === 1) {
					$row = $result->fetch_assoc();
					$file_id = $row['idfile_info'];
					$file_path_office_id = $row['file_path_office_id'];

					// Check file is not created for the current office
					$query = "SELECT * FROM file_info WHERE file_number=? AND file_name=? AND officer_info_office_info_idoffice_info=?";
					$stmt = $db->query($query, [$_POST['file_number'], $_POST['file_name'], $curr_office_id], "ssi");
					$result = $stmt->get_result();
					$stmt->close();

					if ($result->num_rows === 0) {
						// Getting the office id
						$query = "SELECT * FROM office_info WHERE office_short_name=? AND office_status='Active'";		
						$stmt = $db->query($query, [$_POST['sending_location']], "s");
						$result = $stmt->get_result();
						$stmt->close();
						
						// Checking the sending office ID
						$sending_office_id = -1;
						if ($result->num_rows === 1) {
							$row = $result->fetch_assoc();
							$sending_office_id = $row['idoffice_info'];
						}

						if ($sending_office_id === -1) {
							$sessdata['type'] = 'danger';
							$sessdata['message'] = 'Sending office not found';
						} else {
							// Getting the carrier id
							$query = "SELECT * FROM carrier_info WHERE carrier_mobile_primary=? AND officer_info_office_info_idoffice_info=?";
							$stmt = $db->query($query, [$_POST['file_carrier'], $curr_office_id], "si");
							$result = $stmt->get_result();
							$stmt->close();

							// Checking the carrier ID
							if ($result->num_rows === 1) {
								$row = $result->fetch_assoc();
								$carrierID = $row['idcarrier'];
								
								// Insert into file_log
								$query = "INSERT INTO file_log VALUES (NULL,?,?,?,'',current_timestamp(),?,?)";
								$params = array($curr_office_id, $sending_office_id, $_POST['file_status'], $carrierID, $file_id);
								$stmt = $db->query($query, $params, "iisii");
								$inserted = $stmt->insert_id;
								$stmt->close();

								if ($inserted > 0) {
									// Update the file_info
									$file_path = $file_path_office_id . ',' . $sending_office_id;
									//file_path_office_id=?,
									$query = "UPDATE file_info SET file_current_status=?,file_current_office=?,file_current_carrier_id=?
														WHERE idfile_info=?";
									$stmt = $db->query($query, [$_POST['file_status'],$sending_office_id,$carrierID,$file_id], "siii");
									$affected_rows = $stmt->affected_rows;
									$stmt->close();

									if ($affected_rows > 0) {
										$sessdata['type'] = 'success';
										$sessdata['message'] = 'File dispatched successfully';
									} else {
										$sessdata['type'] = 'danger';
										$sessdata['message'] = 'Can not dispatch the file<br>File info not updated';
									}
								} else {
									$sessdata['type'] = 'danger';
									$sessdata['message'] = 'Can not dispatch the file';
								}
							} else {
								$sessdata['type'] = 'danger';
								$sessdata['message'] = 'No carrier person found<br>with the selected carrier';
							}
						}
					} else {
						$sessdata['type'] = 'danger';
						$sessdata['message'] = 'You can not forward a file<br>which is created by you';
					}
				} else {
					$sessdata['type'] = 'danger';
					$sessdata['message'] = 'No file found with the given information';
				}
			} catch (Exception $ex) {
				error_log($ex->getMessage());
				$sessdata['type'] = 'danger';
				$sessdata['message'] = 'Query error';
			}
		} else {
			var_dump($_POST);
			exit;
			$sessdata['type'] = 'warning';
			$sessdata['message'] = 'Please fill up all the fields while dispatching.';
		}
		$_SESSION['sessdata'] = $sessdata;
		header("Location: received-files.php");
	}
?>

<body class="bg-white" id="page-top" data-spy="scroll" data-target=".fixed-top">
	<?php require_once('navbar.php'); ?>
	<div class="container">
		<?php
		if (checkSessionValue('sessdata')) {
			$sessdata = $_SESSION['sessdata'];
		?>
		<div class="row no-gutters">
			<div class="col-lg-5 col-md-12 ml-auto">
				<div class="alert alert-<?php echo $sessdata['type']; ?> alert-dismissible fade show" role="alert">
					<strong><?php echo $sessdata['message']; ?></strong>
				</div>
			</div>
		</div>
		<?php
			unset($_SESSION['sessdata']);
		}
		function getOfficeName($db, $id): string {
			$ret = $id . "";
			try {
				$query = "SELECT * FROM office_info WHERE idoffice_info=?";
				$stmt = $db->query($query, [$id], "i");
				$result = $stmt->get_result();
				$stmt->close();

				$row = $result->fetch_assoc();
				$ret = $row['office_short_name'];
			} catch (Exception $ex) {
				error_log($ex);
			}
			return $ret;
		}
	?>
		<div class="row">
			<div class="col mt-1 pt-1 pl-2 pr-3">
				<div class="row">
					<div class="col">
						<h2>Received pending files</h2>
					</div>
				</div>

				<div class="row">
					<div class="col pt-2">
						<form id="postFileView-pending" action="view.php" method="post">
							<table id="table-pending-files" class="table table-bordered tabel-condensed table-hover text-center">
								<thead class="thead-light">
									<tr class="clickable-table-row">
										<th>Received date</th>
										<th>Received from</th>
										<th>File reference number</th>
										<th>File name</th>
										<th>Status</th>
										<th>Action</th>
										<th></th>
									</tr>
								</thead>
								<tbody id="table-pending-files-body" class="text-center">
									<?php
									$query = "SELECT * FROM file_log l, file_info i WHERE l.file_moved_to_office_id=? 
														AND i.file_current_office=? 
														AND (l.file_status='Forwarded' OR l.file_status='Created')
														AND l.file_info_idfile_info=i.idfile_info
														ORDER BY l.file_moved_at ASC";
									try {
										$stmt = $db->query($query, [$curr_office_id,$curr_office_id], "ii");
										$result = $stmt->get_result();
										$stmt->close();

										if ($result->num_rows > 0) {
											while ($row = $result->fetch_assoc()) {
									?>
									<tr>
										<td><?php echo $row['file_moved_at']; ?></td>
										<td><?php echo getOfficeName($db, $row['file_moved_from_office_id']); ?></td>
										<td><?php echo $row['file_number']; ?></td>
										<td><?php echo $row['file_name']; ?></td>
										<td><?php echo $row['file_status']; ?></td>
										<td>
											<a id="callDispatchModal" data-toggle="modal" data-target="#dispatchModal"
												class="btn btn-primary btn-block text-white">Dispatch</a>
										</td>
										<td><button class="btn btn-secondary btn-block" onclick="postPendingFileInfo(this);">View</button></td>
									</tr>
									<?php 
											}
									?>
									<script>
									function postPendingFileInfo(control) {
										let clickedRow = $(control).parent("td").parent("tr");
										var filetd = $(clickedRow).find('td').eq(2).html();
										if (!(filetd.trim() === null)) {
											$('#postFileView-pending #selectedFileID').val(filetd);
											$('#postFileView-pending #request').val('received-files.php');
											$('#postFileView-pending').submit();
										}
									}
									</script>
									<?php
										} else {
											echo "<tr><td colspan='6'>No pending files</td></tr>";
										}
									} catch (Exception $ex) {
										error_log($ex->getMessage());
										echo "<tr><td colspan='6'>Error!!!</td></tr>";
									}
									?>
								</tbody>
							</table>
							<input type="hidden" name="selectedFileID" id="selectedFileID">
							<input type="hidden" name="request" id="request">
						</form>
					</div>
				</div>

				<div class="row">
					<div class="col mt-3 pt-3">
						<h2>Received forwarded files</h2>
					</div>
				</div>

				<div class="row">
					<div class="col pt-2">
						<form id="postFileView-forwarded" action="view.php" method="post">
							<table id="table-forwarded-files" class="table table-bordered tabel-condensed table-hover">
								<thead class="thead-light">
									<tr class="clickable-table-row">
										<th>Received date</th>
										<th>Submitted to</th>
										<th>File reference number</th>
										<th>File name</th>
										<th>Status</th>
										<th>Dispatched at</th>
									</tr>
								</thead>
								<tbody id="table-forwarded-files-body" class="text-center">
									<?php
									$query = "SELECT * FROM file_log l, file_info i WHERE l.file_moved_from_office_id=? 
														AND (l.file_status='Forwarded' OR l.file_status='Approved' OR l.file_status='Not Approved')
														AND l.file_info_idfile_info=i.idfile_info
														ORDER BY l.file_moved_at DESC";
									try {
										$stmt = $db->query($query, [$curr_office_id], "i");
										$result = $stmt->get_result();
										$stmt->close();

										if ($result->num_rows > 0) {
											while ($row = $result->fetch_assoc()) {
									?>
									<tr class="clickable-table-row" onclick="postForwardedFileInfo(this);">
										<?php
										$query = "SELECT * FROM file_log WHERE file_moved_to_office_id=? AND file_info_idfile_info=?";
										$stmt = $db->query($query, [$curr_office_id, $row['file_info_idfile_info']], "ii");
										$result2 = $stmt->get_result();
										$stmt->close();
										$row2 = $result2->fetch_assoc();
										?>
										<td><?php echo $row2['file_moved_at']; ?></td>
										<td><?php echo getOfficeName($db, $row['file_moved_to_office_id']); ?></td>
										<td><?php echo $row['file_number']; ?></td>
										<td><?php echo $row['file_name']; ?></td>
										<td><?php echo $row['file_status']; ?></td>
										<td><?php echo $row['file_moved_at']; ?></td>
									</tr>
									<?php
											}
									?>
									<script>
									function postForwardedFileInfo(clickedRow) {
										var filetd = $(clickedRow).find('td').eq(2).html();
										if (!(filetd.trim() === null)) {
											$('#postFileView-forwarded #selectedFileID').val(filetd);
											$('#postFileView-forwarded #request').val('received-files.php');
											$('#postFileView-forwarded').submit();
										}
									}
									</script>
									<?php
										} else {
											echo "<tr><td colspan='6'>No forwarded files</td></tr>";
										}
									} catch (Exception $ex) {
										error_log($ex->getMessage());
										echo "<tr><td colspan='6'>Error!!!</td></tr>";
									}
									?>
								</tbody>
							</table>
							<input type="hidden" name="selectedFileID" id="selectedFileID">
							<input type="hidden" name="request" id="request">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" role="dialog" id="dispatchModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Dispatch file</h5>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>

				<div class="row justify-content-center mb-3">
					<form action="" method="post">
						<div class="modal-body">
							<div class="form-group form-floating">
								<input type="text" name="file_number" id="file_number" class="form-control" placeholder="" required
									readonly>
								<label for="file_number">File reference number</label>
							</div>

							<div class="form-group form-floating">
								<input type="text" name="file_name" id="file_name" class="form-control" placeholder="" required
									readonly>
								<label for="file_name">File name</label>
							</div>

							<div class="form-group form-floating">
								<select class="form-control custom-select" name="file_status" id="file_status" required>
									<option value="" selected disabled hidden>None</option>
									<option value="Forwarded">Forwarded</option>
									<option value="Approved">Approved</option>
									<option value="Not Approved">Not Approved</option>
								</select>
								<label for="file_status">Select file status</label>
							</div>

							<div class="form-group form-floating">
								<select class="form-control custom-select" name="sending_location" id="sending_location" required>
									<option value="" selected disabled hidden>None</option>
									<?php
							$query = "SELECT office_short_name FROM office_info WHERE idoffice_info!=? AND office_status='Active' ORDER BY office_short_name";

							try {
								$stmt = $db->query($query, [$curr_office_id], "i");
								$result = $stmt->get_result();
								$stmt->close();

								if ($result->num_rows > 0) {
									while ($row = $result->fetch_row()) {
										echo "<option value='$row[0]'>$row[0]</option>";
									}
								}
							} catch (Exception $ex) {
								error_log($ex->getMessage());
							}
						?>
								</select>
								<label for="sending_location">File sending location</label>
							</div>

							<div class="form-group form-floating">
								<select class="form-control custom-select" name="file_carrier" id="file_carrier" required>
									<option value="" selected disabled hidden>None</option>
									<?php
							$query = "SELECT * FROM carrier_info WHERE officer_info_office_info_idoffice_info=?";

							try {
								$stmt = $db->query($query, [$curr_office_id], "i");
								$result = $stmt->get_result();
								$stmt->close();

								if ($result->num_rows > 0) {
									while ($row = $result->fetch_assoc()) {
										echo "<option value='$row[carrier_mobile_primary]'>$row[carrier_name]</option>";
									}
								}
							} catch (Exception $ex) {
								error_log($ex->getMessage());
							}

							?>
								</select>
								<label for="file_carrier">Select a carrier person</label>
							</div>
						</div>

						<div class="modal-footer">
							<div class="form-group">
								<button type="submit" class="btn btn-primary btn-block" name="submitDispatch">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>

<script>
document.addEventListener('DOMContentLoaded', function() {
	let table1 = new DataTable('#table-pending-files');
	let table2 = new DataTable('#table-forwarded-files');
});
</script>

<?php require_once('footer.php'); ?>