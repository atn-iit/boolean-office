<?php
	$cwd = '../';
	require_once $cwd . 'welcome.php';

	$user_id = $_SESSION['user_id'];
	$officer_id = $_SESSION['id'];
	$curr_office_id = $_SESSION['office_id'];

	$editPage = FALSE;
	$leavePage = FALSE;
	$row = array();
	if (checkIsPOSTS(['mobile_num','action'])) {
		$sessdata = array();
		if ($_POST['action'] === 'delete') {
			$query = "DELETE FROM carrier_info WHERE carrier_mobile_primary=? AND officer_info_idofficer_info=?";
			try {
				$stmt = $db->query($query, [$_POST['mobile_num'],$officer_id],"si");
				$affected_rows = $stmt->affected_rows;
				$stmt->close();
				
				if ($affected_rows > 0) {
					$sessdata['type'] = 'success';
					$sessdata['message'] = 'Successfully removed the carrier';
				} else {
					$sessdata['type'] = 'warning';
					$sessdata['message'] = 'No carrier to remove';
				}
			} catch (Exception $ex) {
				error_log($ex->getMessage());
				$sessdata['type'] = 'danger';
				$sessdata['message'] = 'Query error!!!<br>Cannot remove the carrier.';
			}
		} else if ($_POST['action'] === 'edit') {
			try {
				$query = "SELECT * FROM carrier_info WHERE carrier_mobile_primary=? AND officer_info_office_info_idoffice_info=?";
				$stmt = $db->query($query, [$_POST['mobile_num'],$curr_office_id],"si");
				$row = $stmt->get_result()->fetch_assoc();
				$stmt->close();
				$editPage = TRUE;
			} catch (Exception $ex) {
				error_log($ex->getMessage());
				$sessdata['type'] = 'danger';
				$sessdata['message'] = 'Query error!!!<br>Cannot edit the carrier.';
			}
		} else {
			$sessdata['type'] = 'danger';
			$sessdata['message'] = 'Invalid action';
		}

		$_SESSION['sessdata'] = $sessdata;
	} else if (checkIsPOSTS(['carrierSubmit','full_name', 'mobile_num', 'num'])) {
		$sessdata = array();

		try {
			$query = "UPDATE carrier_info SET carrier_name=?,
				carrier_mobile_secondary=?,carrier_email=?,carrier_address=? 
				WHERE idcarrier=? AND carrier_mobile_primary=? AND officer_info_idofficer_info=?";

// TODO CHECK EMAIL VALIDITY
			$mobile_secondary = checkIsPOST('mobile_secondary') ? $_POST['mobile_secondary'] : NULL;
			$email = checkIsPOST('email') ? $_POST['email'] : NULL;
			$address = checkIsPOST('address') ? $_POST['address'] : NULL;

			$params = [$_POST['full_name'],
				$mobile_secondary, $email, $address,
				$_POST['num'],$_POST['mobile_num'],$officer_id];

			$stmt = $db->query($query, $params, "ssssisi");
			$affected_rows = $stmt->affected_rows;
			$stmt->close();
			
			if ($affected_rows > 0) {
				$sessdata['type'] = 'success';
				$sessdata['message'] = 'Successfully edited the carrier';
			} else {
				$sessdata['type'] = 'warning';
				$sessdata['message'] = 'Nothing changes for the carrier.';
			}
		} catch (Exception $ex) {
			error_log($ex->getMessage());
			$sessdata['type'] = 'danger';
			$sessdata['message'] = 'Query error!!!<br>Cannot edit the carrier';
		}

		$_SESSION['sessdata'] = $sessdata;
	} else {
		$leavePage = TRUE;
	}

	if ($leavePage or !($editPage AND isset($row) AND !empty($row))) {
		header("Location: manage-carrier.php");
		exit;
	}

	$title = "Carrier";
	require_once 'header.php';

?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
	<!-- Navigation bar -->
	<?php require_once('navbar.php'); ?>

	<!-- Body -->
	<div class="container">
		<div class="row justify-content-center mb-3">
			<div class="mt-2 pt-2 pl-2 pr-3 text-justify">
				<form class="shadow-lg rounded-xl p-5 mb-5 bg-white" action="" method="post">
					<!-- TODO CHANGE id carrier-->
					<input type="hidden" name="num" value="<?php echo $row['idcarrier']; ?>">
					<input type="hidden" name="mobile_num" value="<?php echo $row['carrier_mobile_primary']; ?>">

					<div class="col-form-label text-center pb-4">
						<h5>Edit carrier</h5>
					</div>

					<div class="form-group form-floating">
						<input type="text" name="full_name" id="full_name" class="form-control" placeholder="<?php echo $row['carrier_first_name']; ?>"
							value="<?php echo $row['carrier_name']; ?>"
							required autofocus>
						<label for="full_name">Carrier full name</label>
					</div>

					<div class="form-group form-floating">
						<input type="tel" pattern="0[0-9]{10}" name="mobile_secondary" id="mobile_secondary"
							class="form-control" placeholder="<?php echo $row['carrier_mobile_secondary']; ?>"
							value="<?php echo $row['carrier_mobile_secondary']; ?>">
						<label for="mobile_secondary">Mobile number (Secondary)</label>
					</div>

					<div class="form-group form-floating">
						<input type="email" name="email" id="email"
							class="form-control" placeholder="<?php echo $row['carrier_email']; ?>"
							value="<?php echo $row['carrier_email']; ?>">
						<label for="mobile_secondary">Email address</label>
					</div>

					<div class="form-group form-floating">
						<textarea name="address" id="address" rows="3" cols="25" class="form-control"
							placeholder="<?php echo $row['carrier_address']; ?>" value="<?php echo $row['carrier_address']; ?>"></textarea>
						<label for="address">Carrier Address</label>
					</div>

					<div class="form-group">
						<input type="submit" class="btn btn-primary btn-block" value="Submit" name="carrierSubmit">
						<a class="btn btn-secondary btn-block" href="manage-carrier.php">Cancel</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>

<?php
require_once 'footer.php';
?>