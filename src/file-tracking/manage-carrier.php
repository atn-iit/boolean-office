<?php
	$title = "Carrier";

	$cwd = '../';
	require_once $cwd . 'welcome.php';
	require_once 'header.php';

	$user_id = $_SESSION['user_id'];
	$officer_id = $_SESSION['id'];
	$curr_office_id = $_SESSION['office_id'];

?>

<body class="bg-white" id="page-top" data-spy="scroll" data-target=".fixed-top">
	<!-- Navigation bar -->
	<?php require_once('navbar.php'); ?>

		<?php
		if (checkSessionValue('sessdata')) {
			$sessdata = $_SESSION['sessdata'];
		?>
		<div class="row no-gutters">
			<div class="col-lg-5 col-md-12 ml-auto">
				<div class="alert alert-<?php echo $sessdata['type']; ?> alert-dismissible fade show"
							role="alert">
					<strong><?php echo $sessdata['message']; ?></strong>
				</div>
			</div>
		</div>
		<?php
			unset($_SESSION['sessdata']);
		}
		?>

	<!-- Body -->
	<div class="container">
		<div class="row mb-3">
			<div class="col mt-2 pt-2 pl-2 pr-3 text-justify">
				<div class="text-center">
					<h4>Manage carrier</h4>
				</div>
				<form id="form-carrier" class="p-5 mb-5" action="update-carrier.php" method="post">
					<input type="hidden" name="mobile_num" id="mobile_num">
					<input type="hidden" name="action" id="action">
					<table id="table-carrier" class="table table-bordered table-condensed text-center">
						<thead class="thead-light">
							<tr>
								<th>Name</th>
								<th>Mobile (Primary)</th>
								<th>Mobile (Secondary)</th>
								<th>EMail</th>
								<th>Address</th>
								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php
							try {
								$query = "SELECT * FROM carrier_info WHERE officer_info_office_info_idoffice_info=?";
								$stmt = $db->query($query, [$curr_office_id], "i");
								$result = $stmt->get_result();
								$stmt->close();

								if ($result->num_rows > 0) {
									while ($row = $result->fetch_assoc()) {
										echo "<tr>" . 
												"<td>" . $row['carrier_name'] . "</td>" .
												"<td>" . $row['carrier_mobile_primary'] . "</td>" .
												"<td>" . $row['carrier_mobile_secondary'] . "</td>" .
												"<td>" . $row['carrier_email'] . "</td>" .
												"<td>" . $row['carrier_address'] . "</td>" .
												"<td><button type='button' class='btn bg-light' onclick='postCarrierInfo(\"$row[carrier_mobile_primary]\",\"edit\",this)'><span class='edit-icon mr-1'></span></button></td>" .
												"<td><button type='button' class='btn bg-light' onclick='postCarrierInfo(\"$row[carrier_mobile_primary]\",\"delete\",this)'><span class='delete-icon mr-1'></span></button></td>" .
												"</tr>";
									}
							?>
								<script>
									function postCarrierInfo(mobile,action,item) {
										if (mobile.trim() !== null && action.trim() !== null) {
											if (action.trim() === "delete") {
												if (!confirm('Are you sure?')) {
													item.preventDefault();
													return;
												}
											}
											$('#mobile_num').val(mobile);
											$('#action').val(action);
											$('#form-carrier').submit();
										}
									}
								</script>
							<?php
								} else {
									echo "<tr><td colspan='7'>No carrier found <a href='add-carrier.php'>Add new carrier</a></td></tr>";
								}
							} catch (Exception $ex) {
								error_log($ex->getMessage());
								echo "<tr><td colspan='7'>Something went wrong while fetching the carrier info</td></tr>";
							}
							?>
						</tbody>
					</table>
				</form>
			</div>
		</div>
	</div>
</body>

<script>
document.addEventListener('DOMContentLoaded', function() {
	let table1 = new DataTable('#table-carrier');
});
</script>

<?php require_once 'footer.php' ?>
