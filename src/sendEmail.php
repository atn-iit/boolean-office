<?php
	if (basename($_SERVER['PHP_SELF']) === basename(__FILE__)) {
		header($_SERVER["SERVER_PROTOCOL"] . " 403 Forbidden");
		exit('Direct access not allowed');
	}

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

if (!isset($cwd) || empty($cwd)) {
    $cwd = '';
}

require $cwd . 'vendor/PHPMailer/src/Exception.php';
require $cwd . 'vendor/PHPMailer/src/PHPMailer.php';
require $cwd . 'vendor/PHPMailer/src/SMTP.php';
require_once $cwd . 'config.php';

// Usage
// sendMailByPHPMailer('<mail>', '<subject>', '<message>');

function sendMailByPHPMailer($to, $subject, $message) 
{
    $ret = array();

    try {
        $mail = new PHPMailer();
        $mail->CharSet = 'UTF-8';
        $mail->isSMTP(); // Use SMTP protocol
        $mail->Host = 'smtp.gmail.com'; // Specify  SMTP server
        $mail->SMTPAuth = true; // Auth. SMTP
        $mail->Username = Config::$mailAcc; // Mail who send by PHPMailer
        $mail->Password = Config::$mailPass; // your pass mail box
        $mail->SMTPOptions = array(
            'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
            )
        );
        $mail->SMTPSecure = 'ssl'; // Accept SSL
        $mail->Port = 465; // port of your out server
        $mail->setFrom(Config::$mailAcc); // Mail to send at
        $mail->addAddress($to); // Add sender
        $mail->addReplyTo(Config::$mailAcc); // Adress to reply
        $mail->isHTML(true); // use HTML message
        $mail->Subject = $subject;
        $mail->Body = $message;

        // SEND
        if (!$mail->send()) {
            $ret = array('result' => 'Message not sent',
                        'description' => $mail->ErrorInfo,
                        'status' => 'Error');
        } else {
            $ret = array('result' => 'Message has been sent',
                        'description' => '',
                        'status' => 'OK');
        }
    } catch (Exception $ex) {
			error_log($ex->getMessage());
			error_log($mail->ErrorInfo);
        $ret = array('result' => 'Message not sent',
                    'description' => $mail->ErrorInfo,
                    'status' => 'Exception');
    }

    return $ret;
}
