<?php
	if (basename($_SERVER['PHP_SELF']) === basename(__FILE__)) {
		header($_SERVER["SERVER_PROTOCOL"] . " 403 Forbidden");
		exit('Direct access not allowed');
	}

	function checkIsSignIn(): bool
	{
		$sessionValues = ['admin_id', 'admin_name'];
		foreach ($sessionValues as $key => $value) {
			if (checkSessionValue($value) === FALSE) {
				return FALSE;
			}
		}
		return TRUE;
	}
