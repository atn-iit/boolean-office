<?php
$title = "Officer";

$cwd = '../';

require_once 'welcome.php';
require_once 'header.php';
require_once $cwd . 'sendEmail.php';

$admin_id = $_SESSION['admin_id'];
$admin_name = $_SESSION['admin_name'];

if (isset($_POST['officerSubmit'])) {
	$sessdata = array();
	if (checkIsPOSTS(['officer_name', 'officer_email', 'officer_mobile', 'officer_office'])) {
		if (filter_var($_POST['officer_email'], FILTER_VALIDATE_EMAIL)) {
			try {
				// Check officer existence
				$query = "SELECT * FROM user_info WHERE user_email=? AND user_mobile=?";
				$stmt = $db->query($query, [$_POST['officer_email'], $_POST['officer_mobile']], "ss");
				$num_rows = $stmt->get_result()->num_rows;
				$stmt->close();

				if ($num_rows > 0) {
					$sessdata['type'] = 'danger';
					$sessdata['message'] = 'An officer with that information<br>already exists';
				} else {
					// Get office id
					$query = "SELECT * FROM office_info WHERE office_short_name=? AND office_status='Active'";
					$stmt = $db->query($query, [$_POST['officer_office']], "s");
					$result = $stmt->get_result();
					$num_rows = $result->num_rows;
					$stmt->close();

					if ($num_rows > 0) {
						$row = $result->fetch_assoc();
						$office_id = $row['idoffice_info'];

						$uniqidStr = md5(uniqid(mt_rand()));

						// Insert to user
						$query = "INSERT INTO user_info VALUES(NULL,?,?,?,?,'Section Officer',NULL,current_timestamp(),NULL,'New')";
						$params = [$_POST['officer_name'], $_POST['officer_email'], $uniqidStr, $_POST['officer_mobile']];
						$stmt = $db->query($query, $params, "ssss");
						$insertId = $stmt->insert_id;
						$stmt->close();

						if ($insertId > 0) {
							$user_id = $insertId;
							// Insert officer
							$query = "INSERT INTO officer_info VALUES (NULL,NULL,'New',?,?)";
							$params = [$office_id, $user_id];
							$stmt = $db->query($query, $params, "ii");
							$insert = $stmt->insert_id;
							$stmt->close();

							if ($insert > 0) {
								$linkStr = "";
								$toEmail = $_POST['officer_email'];
								$subject = 'New user for file tracking system';
								$message = 'You are added as a new officer for the file tracking system' .
									'<br>Please activate your account by provide this id at activate new signin.' .
									'<br>ID: ' . $uniqidStr .
									'<br/><br/>Regards,<br/>IIT Office Info Center';

								$done = FALSE;
								$ret = sendMailByPHPMailer($toEmail, $subject, $message);
								if ($ret['status'] === 'OK') {
									$done = TRUE;
								}

								if ($done) {
									$sessdata['type'] = 'success';
									$sessdata['message'] = 'New officer information inserted successfully.<br>An activation email sent to the officer account.';
								} else {
									$sessdata['type'] = 'warning';
									$sessdata['message'] = 'New officer information inserted successfully.<br>Can not sent the activation mail.<br>Officer will remain inactive.';
								}
							} else {
								$sessdata['type'] = 'danger';
								$sessdata['message'] = 'Cannot insert new officer information.';
							}
						} else {
							$sessdata['type'] = 'danger';
							$sessdata['message'] = 'Cannot insert new officer information.';
						}
					} else {
						$sessdata['type'] = 'danger';
						$sessdata['message'] = 'Selected office not found.';
					}
				}
			} catch (Exception $ex) {
				error_log($ex->getMessage());
				$sessdata['type'] = 'danger';
				$sessdata['message'] = 'Query error!!!<br>Cannot insert new officer information.';
			}
		} else {
			$sessdata['type'] = 'warning';
			$sessdata['message'] = 'Please enter a valid email';
		}
	} else {
		$sessdata['type'] = 'warning';
		$sessdata['message'] = 'Please provide all the information requested';
	}

	$_SESSION['admin_sessdata'] = $sessdata;
}
?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
	<!-- Navigation bar -->
	<?php require_once('navbar.php'); ?>

	<!-- Body -->
	<div class="container">
		<div class="row justify-content-center mb-3">
			<div class="mt-2 pt-2 pl-2 pr-3 text-justify">
				<form class="shadow-lg rounded-xl p-5 mb-5 bg-white" action="" method="post">
					<div class="col-form-label text-center pb-4">
						<h5>Add new officer</h5>
					</div>

					<div class="form-group form-floating">
						<input type="text" name="officer_name" id="officer_name" class="form-control" placeholder="" required autofocus>
						<label for="officer_name">Officer name</label>
					</div>

					<div class="form-group form-floating">
						<input type="email" name="officer_email" id="officer_email" class="form-control" placeholder="">
						<label for="officer_email">Officer Email address</label>
					</div>

					<div class="form-group form-floating">
						<input type="tel" pattern="0[0-9]{10}" name="officer_mobile" id="officer_mobile" class="form-control" placeholder="" required>
						<label for="officer_mobile">Officer Mobile number</label>
					</div>

					<div class="form-group form-floating">
						<select class="form-control custom-select" name="officer_office" id="officer_office" required>
							<option value="" selected disabled hidden>None</option>
							<?php
							$query = "SELECT office_short_name FROM office_info WHERE office_status='Active' ORDER BY office_short_name";

							try {
								$stmt = $db->query($query);
								$result = $stmt->get_result();
								$stmt->close();

								if ($result->num_rows > 0) {
									while ($row = $result->fetch_row()) {
										echo "<option value='$row[0]'>$row[0]</option>";
									}
								}
							} catch (Exception $ex) {
								error_log($ex->getMessage());
							}
							?>
						</select>
						<label for="officer_office">Assign officer to</label>
					</div>

					<?php
					if (checkSessionValue('admin_sessdata')) {
						$sessdata = $_SESSION['admin_sessdata'];
					?>
						<div class="mt-2 mb-2">
							<span class="text-<?php echo $sessdata['type']; ?>">
								<p class="text-center"><b><?php echo $sessdata['message']; ?></b></p>
							</span>
						</div>
					<?php
						unset($_SESSION['admin_sessdata']);
					}
					?>

					<div class="form-group">
						<input type="submit" class="btn btn-primary btn-block" value="Submit" name="officerSubmit">
						<a class="btn btn-secondary btn-block" href="manage-officer.php">Cancel</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>

<?php require_once 'footer.php'; ?>