<?php
	$title = "SignIn";

	$cwd = '../';

	require_once 'welcome.php';
	require_once 'header.php';

	function checkUser($email, &$uname, $upass): string
	{
		if (empty($uname) || empty($email) || empty($upass)) {
			return "Empty email or username or password";
		}

		$query = "SELECT * FROM admin_info WHERE admin_email=? AND admin_username=?";
		try {
			$db = new Database();
			$stmt = $db->query($query, [$email,$uname], "ss");
			$result = $stmt->get_result();
			$stmt->close();

			if ($result->num_rows === 1) {
				$row = $result->fetch_assoc();
				if (md5($upass) === $row['admin_password']) {
					$_SESSION['admin_id'] = $row['idadmin'];
					$_SESSION['admin_name'] = $row['admin_username'];
					return "";
				} else {
					return "Password not matched";
				}
			} else {
				return "You are not authorized";
			}
		} catch (Exception $ex) {
			error_log($ex->getMessage());
		}

		return "Invalid username or email or password.";
	}

	// Page requested from SignIn page if user_name and user_password contains.
	if (checkIsPOSTS(['user_name', 'user_email', 'user_password'])) {
		$user_name = $_POST['user_name'];
		$user_email = $_POST['user_email'];
		$user_pass = $_POST['user_password'];
		
		if (filter_var($_POST['user_email'], FILTER_VALIDATE_EMAIL)) {
			$retVal = checkUser($user_email, $user_name, $user_pass);
			$location = 'index.php';

			if (!empty($retVal)) {
				$_SESSION['admin_errorval'] = $retVal;
				$location = 'signin.php';
			}

			unset($user_email);
			unset($user_name);
			unset($user_pass);

			header('Location: ' . $location);
		} else {
			$_SESSION['admin_errorval'] = 'Please enter a valid email';
			header('Location: signin.php');
		}
	}

	$sessData = !empty($_SESSION['admin_sessData']) ? $_SESSION['admin_sessData'] : '';
	if (!empty($sessData['status']['msg'])) {
		$statusMsg = $sessData['status']['msg'];
		$statusMsgType = $sessData['status']['type'];
		unset($_SESSION['admin_sessData']['status']);
	}
?>

	<body>
	<div class="container">
		<div class="row justify-content-center mb-5 pb-5">
			<div class="col-md-4 mt-2 pb-3 text-justify">
				<form class="shadow-lg rounded-xl mt-3 p-5 bg-white" action="" method="POST">
					<div class="col-form-label text-center pb-4">
						<h4>Sign in to<br>Admin<br>IIT Office Automation System</h4>
					</div>
					<div class="form-group form-floating">
						<input type="text" class="form-control" id="user_name" name="user_name" placeholder=""
									 autofocus required>
						<label for="user_name">User name</label>
					</div>
					<div class="form-group form-floating">
						<input type="email" class="form-control" id="user_email" name="user_email" placeholder=""
									 required>
						<label for="user_email">User email</label>
					</div>
					<div class="form-group form-floating">
						<input type="password" class="form-control" id="user_password" name="user_password"
									 placeholder="" required>
						<label for="user_password">User password</label>
					</div>
					<div class="pb-2">
						<span class="small text-danger">
							<?php
								if (isset($_SESSION['admin_errorval']) and !empty($_SESSION['admin_errorval'])) {
									echo $_SESSION['admin_errorval'];
									unset($_SESSION['admin_errorval']);
								}
							?>
						</span>
					</div>
					<?php if (!empty($statusMsg)) { ?>
						<span class="small text-<?php echo $statusMsgType; ?>">
						<?php echo '<p>' . $statusMsg . '</p>'; ?>
					</span>
					<?php } ?>
					<button type="submit" class="btn btn-primary btn-block">Sign In</button>
				</form>
			</div>
		</div>
	</div>
	</body>

<?php require_once('footer.php'); ?>
