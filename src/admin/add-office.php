<?php
	$title = "Office";

	$cwd = '../';

	require_once 'welcome.php';
	require_once 'header.php';

	$admin_id = $_SESSION['admin_id'];
	$admin_name = $_SESSION['admin_name'];

	if (isset($_POST['officeSubmit'])) {
		$sessdata = array();
		if (checkIsPOSTS(['office_name', 'office_short_name'])) {
			try {
				// Check office short name contains no space
				if (strpos($_POST['office_short_name'], " ") === FALSE) {
					// Check office existence
					$query = "SELECT * FROM office_info WHERE office_name=?";
					$stmt = $db->query($query, [$_POST['office_name']], "s");
					$num_rows = $stmt->get_result()->num_rows;
					$stmt->close();

					if ($num_rows > 0) {
						$sessdata['type'] = 'danger';
						$sessdata['message'] = 'An office with that information<br>already exists';
					} else {
						// Insert office
						$query = "INSERT INTO office_info VALUES (NULL,?,?,'Active')";
						$office_short = strtoupper($_POST['office_short_name']);
						$params = [$_POST['office_name'],$office_short];
						$stmt = $db->query($query,$params,"ss");
						$insert = $stmt->insert_id;
						$stmt->close();

						if ($insert > 0) {
							$sessdata['type'] = 'success';
							$sessdata['message'] = 'New office information<br>inserted successfully.';
						} else {
							$sessdata['type'] = 'danger';
							$sessdata['message'] = 'Cannot insert new office information.';
						}
					}
				} else {
					$sessdata['type'] = 'danger';
					$sessdata['message'] = 'Please provide a short name with no space';
				}
			} catch (Exception $ex) {
				error_log($ex->getMessage());
				$sessdata['type'] = 'danger';
				$sessdata['message'] = 'Query error!!!<br>Cannot insert new office information.';
			}
		} else {
			$sessdata['type'] = 'warning';
			$sessdata['message'] = 'Please provide all the information requested';
		}

		$_SESSION['admin_sessdata'] = $sessdata;
	}
?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
	<!-- Navigation bar -->
	<?php require_once('navbar.php'); ?>

	<!-- Body -->
	<div class="container">
		<div class="row justify-content-center mb-3">
			<div class="mt-2 pt-2 pl-2 pr-3">
				<form class="shadow-lg rounded-xl p-5 mb-5 bg-white" action="" method="post">
					<div class="col-form-label text-center pb-4">
						<h5>Add new office</h5>
					</div>

					<div class="form-group form-floating">
						<input type="text" name="office_name" id="office_name" class="form-control" placeholder=""
							required autofocus>
						<label for="office_name">Office full name</label>
					</div>

					<div class="form-group form-floating">
						<input type="text" name="office_short_name" id="office_short_name" class="form-control" placeholder="UPPERCASE"
							required pattern="[A-Z]{3,}">
						<label for="office_short_name">Office short name</label>
					</div>

					<?php
					if (checkSessionValue('admin_sessdata')) {
						$sessdata = $_SESSION['admin_sessdata'];
					?>
					<div class="mt-2 mb-2">
						<span class="text-<?php echo $sessdata['type']; ?>">
							<p class="text-center"><b><?php echo $sessdata['message']; ?></b></p>
						</span>
					</div>
					<?php
						unset($_SESSION['admin_sessdata']);
					}
					?>

					<div class="form-group">
						<input type="submit" class="btn btn-primary btn-block" value="Submit" name="officeSubmit">
						<a class="btn btn-secondary btn-block" href="manage-office.php">Cancel</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>

<?php require_once 'footer.php'; ?>
