<?php
	$title = 'Session';
	$cwd = '../';

	require_once 'welcome.php';
	require_once 'header.php';

	$admin_id = $_SESSION['admin_id'];
	$admin_name = $_SESSION['admin_name'];

	if (isset($_POST['submit'])) {
		$sessdata = [];
		if (checkIsPOSTS(['session', 'batch', 'session_office'])) {
			$session = $_POST['session'];
			$batch = $_POST['batch'];

			$t = explode('-', $session);
			$t1 = $t[0];
			$t2 = $t[1];
			$t = $t2 - $t1;
			if ($t === 1) {
				if (preg_match("/^20[0-9]{2}-20[0-9]{2}$/", $session) === 1 and
						preg_match("/^[0-9]{2}$/", $batch) === 1) {
					try {
						// Get office id
						$query = "SELECT * FROM office_info WHERE office_short_name=? AND office_status='Active'";
						$stmt = $db->query($query, [$_POST['session_office']], "s");
						$result = $stmt->get_result();
						$num_rows = $result->num_rows;
						$stmt->close();
						
						if ($num_rows > 0) {
							$row = $result->fetch_assoc();
							$office_id = $row['idoffice_info'];

							$query = "SELECT * FROM session_info WHERE session_name=? AND session_batch=? AND office_info_idoffice_info=?";
							$stmt = $db->query($query, [$session, $batch, $office_id], "sii");
							$num_rows = $stmt->get_result()->num_rows;
							$stmt->close();
							if ($num_rows > 0) {
								$sessdata['type'] = 'danger';
								$sessdata['message'] = 'There is already a session exists';
							} else {
								$query = "INSERT INTO session_info VALUES (NULL, ?, ?, ?)";
								$stmt = $db->query($query, [$session, $batch, $office_id], "sii");
								$affected_rows = $stmt->affected_rows;
								$stmt->close();
								if ($affected_rows > 0) {
									$sessdata['type'] = 'success';
									$sessdata['message'] = 'Successfully added new session';
								} else {
									$sessdata['type'] = 'danger';
									$sessdata['message'] = 'Cannot add new session';
								}
							}
						} else {
							$sessdata['type'] = 'danger';
							$sessdata['message'] = 'Selected office not found.';
						}
					} catch (Exception $ex) {
						error_log($ex->getMessage());
						$sessdata['type'] = 'danger';
						$sessdata['message'] = 'Query Error!!!';
					}
				} else {
					$sessdata['type'] = 'danger';
					$message = 'Please provide the correct session value';
				}
			} else {
				$sessdata['type'] = 'danger';
				$sessdata['message'] = 'Session difference is not correct';
			}
		} else {
			$sessdata['type'] = 'danger';
			$sessdata['message'] = 'Please provide all the information.';
		}
		$_SESSION['admin_sessdata'] = $sessdata;
	}
?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
	<!-- Navigation bar -->
	<?php require_once('navbar.php'); ?>

	<!-- Body -->
	<div class="container">
		<div class="row justify-content-center mb-3">
			<div class="mt-2 pt-2 pr-3 text-justify">
				<div class="mt-4 text-center">
					<h4>Add new session</h4>
				</div>
				<form class="shadow-lg rounded-xl p-5 mb-5 bg-white" action="" method="POST">
					<div class="form-group form-floating">
						<input type="text" name="session" id="session" class="form-control" placeholder="e.g. 2017-2018" required
							pattern="20[0-9]{2}-20[0-9]{2}" autofocus>
						<label for="session">Enter session</label>
					</div>
					<div class="form-group form-floating">
						<input type="text" name="batch" id="batch" class="form-control" placeholder="13" 
							pattern="[0-9]{2}" required>
						<label for="batch">Enter batch</label>
					</div>

					<div class="form-group form-floating">
						<select class="form-control custom-select" name="session_office" id="session_office" required>
							<option value="" selected disabled hidden>None</option>
							<?php
							$query = "SELECT office_short_name FROM office_info WHERE office_status='Active' ORDER BY office_short_name";

							try {
								$stmt = $db->query($query);
								$result = $stmt->get_result();
								$stmt->close();

								if ($result->num_rows > 0) {
									while ($row = $result->fetch_row()) {
										echo "<option value='$row[0]'>$row[0]</option>";
									}
								}
							} catch (Exception $ex) {
								error_log($ex->getMessage());
							}
						?>
						</select>
						<label for="session_office">Select office</label>
					</div>

					<?php
					if (checkSessionValue('admin_sessdata')) {
						$sessdata = $_SESSION['admin_sessdata'];
					?>
					<div class="mt-2 mb-2">
						<span class="text-<?php echo $sessdata['type']; ?>">
							<p class="text-center"><b><?php echo $sessdata['message']; ?></b></p>
						</span>
					</div>
					<?php
						unset($_SESSION['admin_sessdata']);
					}
					?>

					<div class="form-group">
						<input type="submit" name="submit" class="btn btn-primary btn-block" value="Submit">
						<a class="btn btn-secondary btn-block" href="manage-session.php">Cancel</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>

<?php require_once 'footer.php'; ?>
