<?php
	$title = 'Session';
	
	$cwd = '../';

	require_once 'welcome.php';
	require_once 'header.php';

	$admin_id = $_SESSION['admin_id'];
	$admin_name = $_SESSION['admin_name'];
?>

<body class="bg-white" id="page-top" data-spy="scroll" data-target=".fixed-top">
	<!-- Navigation bar -->
	<?php require_once('navbar.php'); ?>

	<!-- Body -->
	<div class="container">
		<div class="row mb-3">
			<div class="col mt-2 pt-2 pl-2 pr-3 text-justify">
				<h3>Session information</h3>
				<table id="table-session" class="table table-bordered table-condensed text-center">
					<thead class='thead-light text-center'>
						<tr>
							<th>Session</th>
							<th>Batch</th>
							<th>Office</th>
							<th>Total student</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<?php
							function getOfficeName($db, $id): string {
								$ret = $id . "";
								try {
									$query = "SELECT * FROM office_info WHERE idoffice_info=?";
									$stmt = $db->query($query, [$id], "i");
									$result = $stmt->get_result();
									$stmt->close();

									$row = $result->fetch_assoc();
									$ret = $row['office_short_name'];
								} catch (Exception $ex) {
									error_log($ex);
								}
								return $ret;
							}

							function getStudentCount($db, $session_id, $office_id): string {
								try {
									$query = "SELECT count(*) FROM student_info WHERE session_info_idsession_info=? AND session_info_office_info_idoffice_info=?";
									$stmt = $db->query($query, [$session_id, $office_id], "ii");
									$result = $stmt->get_result();
									$stmt->close();
									
									if ($result->num_rows) {
										$row = $result->fetch_row();
										return $row[0];
									}
								} catch (Exception $ex) {
									error_log($ex->getMessage());
								}
								return 0;
							}

							$query = "SELECT * FROM session_info";
							try {
								$stmt = $db->query($query);
								$result = $stmt->get_result();
								$stmt->close();
								if ($result->num_rows > 0) {
									while ($row = $result->fetch_assoc()) {
										?>
									<tr>
										<td><?= $row['session_name']; ?></td>
										<td><?= $row['session_batch']; ?></td>
										<td><?= getOfficeName($db, $row['office_info_idoffice_info']); ?></td>
										<td><?= getStudentCount($db, $row['idsession_info'], $row['office_info_idoffice_info']); ?></td>
									</tr>
										<?php
									}
								} else {
									echo "<tr colspan='4'>No session</tr>";
								}
							} catch (Exception $ex) {
								error_log($ex->getMessage());
								echo "<tr colspan='4'>No session</tr>";
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>

<script>
document.addEventListener('DOMContentLoaded', function() {
	let table1 = new DataTable('#table-session');
});
</script>

<?php require_once('footer.php'); ?>
