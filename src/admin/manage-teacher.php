<?php
$title = 'Teacher';

$cwd = '../';

require_once 'welcome.php';
require_once 'header.php';

$admin_id = $_SESSION['admin_id'];
$admin_name = $_SESSION['admin_name'];

if (checkIsPOSTS(['teacher_email', 'teacher_mobile', 'action'])) {
	$sessdata = array();
	try {
		// Check teacher existence
		$query = "SELECT * FROM teacher_info o, user_info u WHERE u.user_email=? AND u.user_mobile=? AND u.iduser_info=o.user_info_iduser_info";
		$stmt = $db->query($query, [$_POST['teacher_email'],$_POST['teacher_mobile']], "ss");
		$result = $stmt->get_result();
		$stmt->close();

		if ($result->num_rows > 0) {
			$row = $result->fetch_assoc();
			$user_id = $row['iduser_info'];
			$status = $row['user_status'];
			$action = $_POST['action'];

			if ($status === $action or $status === 'New' or ($action !== 'Inactive' and $action !== 'Active')) {
				$sessdata['type'] = 'danger';
				$sessdata['message'] = 'Can not change the office status to ' . $_POST['action'];
			} else {
				if ($action === 'Inactive') {
					// Update the teacher status
					$query = "UPDATE user_info SET user_status='Inactive' WHERE iduser_info=?";
					$stmt = $db->query($query, [$user_id], "i");
					$affected_rows = $stmt->affected_rows;
					$stmt->close();

					if ($affected_rows > 0) {
						$sessdata['type'] = 'success';
						$sessdata['message'] = 'teacher status updated successfully';
					} else {
						$sessdata['type'] = 'danger';
						$sessdata['message'] = 'Can not update the teacher status';
					}
				} else if ($action === 'Active') {
					// Update the teacher status
					$query = "UPDATE user_info SET user_status='Active' WHERE iduser_info=?";
					$stmt = $db->query($query, [$user_id], "i");
					$affected_rows = $stmt->affected_rows;
					$stmt->close();

					if ($affected_rows > 0) {
						$sessdata['type'] = 'success';
						$sessdata['message'] = 'teacher status updated successfully';
					} else {
						$sessdata['type'] = 'danger';
						$sessdata['message'] = 'Can not update the teacher status';
					}
				}
			}
		} else {
			$sessdata['type'] = 'danger';
			$sessdata['message'] = 'No teacher found with the selected one.';
		}
	} catch (Exception $ex) {
		error_log($ex->getMessage());
		$sessdata['type'] = 'danger';
		$sessdata['message'] = 'Query error!!!<br>Cannot update teacher status.';
	}
	$_SESSION['admin_sessdata'] = $sessdata;
}
?>

<body class="bg-white" id="page-top" data-spy="scroll" data-target=".fixed-top">
	<!-- Navigation bar -->
	<?php require_once('navbar.php'); ?>

	<!-- Body -->
	<div class="container">
		<?php
		if (checkSessionValue('admin_sessdata')) {
			$sessdata = $_SESSION['admin_sessdata'];
		?>
		<div class="row no-gutters">
			<div class="col-lg-5 col-md-12 ml-auto">
				<div class="alert alert-<?php echo $sessdata['type']; ?> alert-dismissible fade show" role="alert">
					<strong><?php echo $sessdata['message']; ?></strong>
				</div>
			</div>
		</div>
		<?php
			unset($_SESSION['admin_sessdata']);
		}
		?>

		<div class="row">
			<div class="col mt-2 pt-2 text-justify">
				<h1 class="mb-4">Teacher List</h1>

				<form class="row" action="" method="POST">
					<div class="form-group col-3 ml-2 mr-2">
						<label for="office">Select department</label>
						<select class="form-control custom-select" name="office" id="office" required>
							<option value="" selected disabled hidden>None</option>
							<?php
							$query = "SELECT office_short_name FROM office_info ORDER BY office_short_name";
							try {
								$stmt = $db->query($query);
								$result = $stmt->get_result();
								$stmt->close();

								if ($result->num_rows > 0) {
									while ($row = $result->fetch_row()) {
										if ($row[0] === "Registrar Office") continue;
										echo "<option value='$row[0]'>$row[0]</option>";
									}
								} else {
									echo "<option value='' selected disabled hidden>No office</option>";
								}
							} catch (Exception $ex) {
								error_log($ex->getMessage());
							}
							?>
						</select>
					</div>

					<div class="form-group mt-4">
						<button type="submit" class="btn btn-primary btn-block" name="submit">Search</button>
					</div>
				</form>
			</div>
		</div>

		<div class="row">
			<div class="col mt-2 pt-2 pr-3 text-justify" id="teacher-info">
				<?php
				$contains_teacher_info = FALSE;
				if (checkIsPOST('office')) {
					$query = "SELECT * FROM teacher_info t, user_info u WHERE office_info_idoffice_info=(SELECT idoffice_info FROM office_info WHERE office_short_name=?) AND u.iduser_info=t.user_info_iduser_info";
					$stmt = $db->query($query, [$_POST['office']], 's');
					$result = $stmt->get_result();
					$stmt->close();
					$contains_teacher_info = true;
				}

				if ($contains_teacher_info) {
				?>
				<form id="form-teacher" class="p-2 mb-5 bg-white" action="" method="post">
					<input type="hidden" name="teacher_email" id="teacher_email">
					<input type="hidden" name="teacher_mobile" id="teacher_mobile">
					<input type="hidden" name="action" id="action">
				<table id="table-teacher" class='table table-bordered table-condensed table-hover text-center'>
					<thead class="thead-light">
						<tr>
							<th>Teacher ID</th>
							<th>Teacher Name</th>
							<th>Designation</th>
							<th>Contact Number</th>
							<th>Email</th>
							<th>Status</th>
							<th>Role</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						if ($result->num_rows > 0) {
							while ($row = $result->fetch_assoc()) {
								echo "" .
									"<tr>" .
									"<td>" . $row['teacher_id'] . "</td>" .
									"<td>" . $row['user_name'] . "</td>" .
									"<td>" . $row['teacher_designation'] . "</td>" .
									"<td>" . $row['user_mobile'] . "</td>" .
									"<td>" . $row['user_email'] . "</td>" .
									"<td>" . $row['user_status'] . "</td>" .
									"<td>" . $row['teacher_role'] . "</td>";
								if ($row['user_status'] === "Inactive") {
									echo "<td><button type='button' name='teacherSubmit' class='btn btn-secondary' onclick='postAction(\"$row[user_email]\",\"$row[user_mobile]\",\"Active\",this)'>Set Active</button></td>";
								} else if ($row['user_status'] === "Active") {
									echo "<td><button type='button' name='teacherSubmit' class='btn btn-danger' onclick='postAction(\"$row[user_email]\",\"$row[user_mobile]\",\"Inactive\",this)'>Set Inactive</button></td>";
								} else {
									echo "<td></td>";
								}
								echo "</tr>";
							}
						?>
						<script>
							function postAction(email, mobile, action, item) {
								if (email.trim() !== null && action.trim() !== null && mobile.trim() !== null) {
									if (action.trim() === "Inactive") {
										if (!confirm('This will set the teacher as ' + action.trim() + '\nAnd teacher no longer can signin\nProceed?')) {
											// item.preventDefault();
											return;
										}
									}
									$('#teacher_email').val(email);
									$('#teacher_mobile').val(mobile);
									$('#action').val(action);
									$('#form-teacher').submit();
								}
							}
						</script>
						<?php
						} else {
							echo "<tr><td colspan='8' class='text-center'>No teacher found</td></tr>";
						}
						?>
					</tbody>
				</table>
				<?php
				}
				?>
			</div>
		</div>
	</div>
</body>

<script>
document.addEventListener('DOMContentLoaded', function() {
	let table1 = new DataTable('#table-teacher');
});
</script>

<?php require_once('footer.php'); ?>
