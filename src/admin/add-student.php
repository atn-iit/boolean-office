<?php
$title = 'Student';

$cwd = '../';

require_once 'welcome.php';
require_once 'header.php';
require_once $cwd . 'sendEmail.php';

$admin_id = $_SESSION['admin_id'];
$admin_name = $_SESSION['admin_name'];

if (isset($_POST['submit'])) {
	$sessdata = [];
	if (!checkIsPOSTS([
		'student_id', 'student_name', 'office', 'session',
		'student_email', 'student_mobile', 'student_parent_mobile',
		'student_bod', 'student_gender'
	])) {
		$sessdata['type'] = 'danger';
		$sessdata['message'] = "Please provide all the information";
	} else {
		$student_id = $_POST['student_id'];
		$student_name = $_POST['student_name'];
		$student_session = $_POST['session'];
		$student_email = $_POST['student_email'];
		$student_mobile = $_POST['student_mobile'];
		$student_parent_mobile = isset($_POST['student_parent_mobile']) ? $_POST['student_parent_mobile'] : "";
		$student_bod = isset($_POST['student_bod']) ? $_POST['student_bod'] : "";
		$student_gender = ucfirst(strtolower($_POST['student_gender']));
		$student_address = isset($_POST['student_address']) ? $_POST['student_address'] : "";
		$office_short_name = $_POST['office'];

		try {
			// Check student existence
			$query = "SELECT * FROM student_info WHERE student_roll=?";
			$stmt = $db->query($query, [$student_id], "s");
			$num_rows = $stmt->get_result()->num_rows;
			$stmt->close();
			if ($num_rows > 0) {
				$sessdata['type'] = 'danger';
				$sessdata['message'] = 'Student already registed.';
			} else {
				// Get session and office id
				$query = "SELECT * FROM session_info sess, office_info o WHERE sess.office_info_idoffice_info=o.idoffice_info AND o.office_short_name=? AND sess.session_name=?";
				$stmt = $db->query($query, [$office_short_name, $student_session], "ss");
				$result = $stmt->get_result();
				$stmt->close();

				if ($result->num_rows > 0) {
					$row = $result->fetch_assoc();
					$office_id = $row['idoffice_info'];
					$office_name = $row['office_name'];
					$session_id = $row['idsession_info'];
					$uniqidStr = md5(uniqid(mt_rand()));

					// Insert to user 
					$query = "INSERT INTO user_info VALUES(NULL,?,?,?,?,'Student',NULL,current_timestamp(),NULL,'New')";
					$stmt = $db->query($query, [$student_name,$student_email, $uniqidStr, $student_mobile], "ssss");
					$insertId = $stmt->insert_id;
					$stmt->close();

					if ($insertId > 0) {
						$user_id = $insertId;
						// Insert to student
						$query = "INSERT INTO student_info VALUES(NULL,?,?,?,?,?,'New',?,?,?)";
						$stmt = $db->query($query, 
							[$student_id, $student_gender, $student_address,$student_parent_mobile,$student_bod,$session_id, $office_id, $user_id], "sssssiii");
						$insertId = $stmt->insert_id;
						$stmt->close();

						if ($insertId > 0) {
							// Send mail notification
							$linkStr = "";
							$toEmail = $student_email;
							$subject = 'New student added for '. $office_short_name;
							$message = 'You are added as a new student for '. $office_name .
								'<br>Please activate your account by provide this id at activate new signin.' .
								'<br>ID: ' . $uniqidStr .
								'<br/><br/>Regards,<br/>IIT Office Info Center';

							$done = FALSE;
							// $ret = sendMailByPHPMailer($toEmail, $subject, $message);
							// if ($ret['status'] === 'OK') {
								$done = TRUE;
							// }

							if ($done) {
								$sessdata['type'] = 'success';
								$sessdata['message'] = 'Successfully registered a new student.';
							} else {
								$sessdata['type'] = 'danger';
								$sessdata['message'] = 'Could not register student.';
							}
						} else {
							$sessdata['type'] = 'danger';
							$sessdata['message'] = 'Could not register student.';
						}
					} else {
						$sessdata['type'] = 'danger';
						$sessdata['message'] = 'Could not register new student.';
					}
				} else {
					$sessdata['type'] = 'danger';
					$sessdata['message'] = 'Selected department and session not found.';
				}
			}
		} catch (Exception $ex) {
			error_log($ex->getMessage());
			$sessdata['type'] = 'danger';
			$sessdata['message'] = "Registration failed";
		}
	}
	$_SESSION['admin_sessdata'] = $sessdata;
}
?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
	<!-- Navigation bar -->
	<?php require_once('navbar.php'); ?>

	<!-- Body -->
	<div class="container">
		<div class="row justify-content-center mb-3">
			<div class="mt-2 pt-2 pl-2 pr-3">
				<form class="shadow-lg rounded-xl p-5 mb-5 bg-white" action="" method="post">
					<div class="col-form-label text-center pb-4">
						<h5>Register new student</h5>
					</div>

					<?php
					if (checkSessionValue('admin_sessdata')) {
						$sessdata = $_SESSION['admin_sessdata'];
					?>
						<div class="mt-2 mb-2">
							<span class="text-<?php echo $sessdata['type']; ?>">
								<p class="text-center"><b><?php echo $sessdata['message']; ?></b></p>
							</span>
						</div>
					<?php
						unset($_SESSION['admin_sessdata']);
					}
					?>

					<div class="row m-1">
						<div class="form-group form-floating m-2">
							<input type="text" name="student_id" id="student_id" class="form-control" placeholder="" required autofocus>
							<label for="student_id">Enter student id</label>
						</div>

						<div class="form-group form-floating m-2">
							<input type="text" name="student_name" id="student_name" class="form-control" placeholder="" required>
							<label for="student_name">Enter student name</label>
						</div>
					</div>

					<div class="row m-1">
						<div class="form-group form-floating w-45 m-2">
							<select class="form-control custom-select" name="office" id="office" required>
								<option value="" selected disabled hidden>None</option>
								<?php
								$query = "SELECT office_short_name FROM office_info ORDER BY office_short_name";
								try {
									$stmt = $db->query($query);
									$result = $stmt->get_result();
									$stmt->close();

									if ($result->num_rows > 0) {
										while ($row = $result->fetch_row()) {
											if ($row[0] === "Registrar Office") continue;
											echo "<option value='$row[0]'>$row[0]</option>";
										}
									} else {
										echo "<option value='' selected disabled hidden>No office</option>";
									}
								} catch (Exception $ex) {
									error_log($ex->getMessage());
								}
								?>
							</select>
							<label for="office">Select department</label>
						</div>

						<div class="form-group form-floating w-45 m-2">
							<select class="form-control custom-select" name="session" id="session" required>
								<option value="" selected disabled hidden>None</option>
							</select>
							<label for="session">Select Session</label>
						</div>
					</div>

					<div class="row m-1">
						<div class="form-group form-floating m-2 w-100">
							<input type="email" name="student_email" id="student_email" class="form-control" placeholder="" required>
							<label for="student_email">Enter student email</label>
						</div>
					</div>

					<div class="row m-1">
						<div class="form-group form-floating w-45 m-1">
							<input type="tel" pattern="0[0-9]{10}" name="student_mobile" id="student_mobile" class="form-control" placeholder="" required>
							<label for="student_mobile">Enter student mobile number</label>
						</div>

						<div class="form-group form-floating w-45 m-1">
							<input type="tel" pattern="0[0-9]{10}" name="student_parent_mobile" id="student_parent_mobile" class="form-control" placeholder="" required>
							<label for="student_parent_mobile">Enter parent mobile number</label>
						</div>
					</div>

					<div class="row m-1">
						<div class="form-group form-floating w-100 m-2">
							<input type="date" name="student_bod" id="student_bod" class="form-control custom-select" max="<?= date('Y-m-d')?>" required>
							<label for="student_bod">Date of birth: </label>
						</div>
					</div>

					<div class="row m-1">
						<div class="form-group form-floating w-100 m-2">
							<select class="form-control custom-select" name="student_gender" id="student_gender" required>
								<option value="" selected disabled hidden>None</option>
								<option value="Male">Male</option>
								<option value="Female">Female</option>
							</select>
							<label for="student_gender">Select gender</label>
						</div>
					</div>

					<div class="row m-1">
						<div class="form-group form-floating w-100 m-2">
							<textarea name="student_address" id="student_address" class="form-control" rows="4" placeholder=""></textarea>
							<label for="student_address">Write your address</label>
						</div>
					</div>

					<div class="form-group">
						<input type="submit" name="submit" class="btn btn-primary btn-block" value="Submit">
					</div>
				</form>
			</div>
		</div>
	</div>

	<script src="<?= $cwd; ?>js/jquery.min.js"></script>
	<script>
		$(document).ready(function() {
			$('#office').on('change', function() {
				let office_short_name_v = $(this).val();
				if (office_short_name_v) {
					$.ajax({
						type: "POST",
						url: "getSession.php",
						data: {
							office_short_name: office_short_name_v
						},
						success: function(html) {
							$('#session').html(html);
						}
					});
				} else {
					$('#session').html("<option value='' selected disabled hidden>None</option>");
				}
			});
		})
	</script>
</body>

<?php require_once('footer.php'); ?>
