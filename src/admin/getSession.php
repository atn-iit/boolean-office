<?php
$cwd = '../';
require_once $cwd . 'custom-functions.php';
require_once $cwd . 'dbconn.php';

if (checkIsPOSTS(['office_short_name'])) {
	$query = "SELECT session_name FROM session_info WHERE office_info_idoffice_info=(SELECT idoffice_info FROM office_info WHERE office_short_name=?) ORDER BY session_name";
	try {
		$stmt = $db->query($query, [$_POST['office_short_name']], "s");
		$result = $stmt->get_result();
		$stmt->close();

		// echo "<option value='' selected disabled hidden>None</option>";
		if ($result->num_rows > 0) {
			while ($row = $result->fetch_row()) {
				echo "<option value='$row[0]'>$row[0]</option>";
			}
		}
	} catch (Exception $ex) {
		error_log($ex->getMessage());
	}
} else {
	echo "<option value='' selected disabled hidden>None</option>";
}
