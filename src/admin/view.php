<?php
$title = 'Student';
$cwd = '../';

require_once 'welcome.php';
require_once 'header.php';

$admin_id = $_SESSION['admin_id'];
$admin_name = $_SESSION['admin_name'];

if (!checkIsPOST('selectedStudent')) {
	header('Location: manage-student.php');
}

?>

<body class="bg-white" id="page-top" data-spy="scroll" data-target=".fixed-top">
	<!-- Navigation bar -->
	<?php require_once('navbar.php'); ?>

	<!-- Body -->
	<div class="container">
		<div class="row">
			<div class="col mt-4 pt-4 pl-2 pr-3 mb-4 text-center">
				<h4>Student's Profile</h4>
			</div>
		</div>

		<div class="row justify-content-center">
			<div class="mb-4">
				<!-- <img class="img-fluid" src="profile.png" alt="Student Pic"> -->
				<!-- <a class="btn w-100" href="#"><i class="fa fa-pencil-alt"></i> Change Picture</a> -->
			</div>
			<div class="col mt-2 mb-5">
				<?php
				$query = "SELECT * FROM student_info s, user_info u, session_info sess WHERE s.student_roll=? AND u.iduser_info=s.user_info_iduser_info AND s.session_info_idsession_info=sess.idsession_info";
				try {
					$stmt = $db->query($query, [$_POST['selectedStudent']], "s");
					$result = $stmt->get_result();
					$stmt->close();

					if ($result->num_rows > 0) {
						$row = $result->fetch_assoc();
				?>
						<table class='table table-borderless'>
							<tbody>
								<tr>
									<td class="text-right"><i class="far fa-id-card fa-fw mr-2"></i>ID</td>
									<td><?php echo $row['student_roll']; ?></td>
								</tr>
								<tr>
									<td class="text-right"><i class="far fa-user fa-fw mr-2"></i>Name</td>
									<td><?php echo $row['user_name']; ?></td>
								</tr>
								<tr>
									<td class="text-right"><i class="far fa-calendar fa-fw mr-2"></i>Session</td>
									<td><?php echo $row['session_name']; ?></td>
								</tr>
								<tr>
									<td class="text-right"><i class="far fa-address-book fa-fw mr-2"></i>Mobile</td>
									<td><?php echo $row['user_mobile']; ?></td>
								</tr>
								<tr>
									<td class="text-right"><i class="far fa-envelope fa-fw mr-2"></i>Email</td>
									<td><?php echo $row['user_email']; ?></td>
								</tr>
								<tr>
									<td class="text-right"><i class="far fa-venus-mars fa-fw mr-2"></i>Gender</td>
									<td><?php echo $row['student_gender']; ?></td>
								</tr>
								<tr>
									<td class="text-right"><i class="far fa-birthday-cake fa-fw mr-2"></i>Date of Birth</td>
									<td><?php echo $row['student_date_of_birth']; ?></td>
								</tr>
								<tr>
									<td class="text-right"><i class="far fa-venus-mars fa-fw mr-2"></i>Parent Contact</td>
									<td><?php echo $row['student_parent_contact']; ?></td>
								</tr>
								<tr>
									<td class="text-right"><i class="far fa-location-arrow fa-fw mr-2"></i>Address</td>
									<td><?php echo $row['student_address']; ?></td>
								</tr>
							</tbody>
						</table>
				<?php
					} else {
						echo "<h4>Invalid student info</h4>";
					}
				} catch (Exception $ex) {
					error_log($ex->getMessage());
				}
				?>
			</div>
		</div>
	</div>
</body>

<?php require_once('footer.php'); ?>
