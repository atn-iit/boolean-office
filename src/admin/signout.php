<?php
	session_start();
	// session_unset();
	// session_destroy();

	if (isset($_SESSION['admin_id'])) {
		unset($_SESSION['admin_id']);
	}

	if (isset($_SESSION['admin_name'])) {
		unset($_SESSION['admin_name']);
	}

	if (isset($_SESSION['admin_sessdata'])) {
		unset($_SESSION['admin_sessdata']);
	}

	header('Location: signin.php');
?>
