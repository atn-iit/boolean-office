<?php
$title = 'Teacher';

$cwd = '../';

require_once 'welcome.php';
require_once 'header.php';
require_once $cwd . 'sendEmail.php';

$admin_id = $_SESSION['admin_id'];
$admin_name = $_SESSION['admin_name'];

if (isset($_POST['submit'])) {
	$sessdata = [];
	if (!checkIsPOSTS([
		'teacher_id', 'teacher_name', 'office',
		'teacher_email', 'teacher_mobile',
		'teacher_role', 'teacher_designation'
	])) {
		$sessdata['type'] = 'danger';
		$sessdata['message'] = "Please provide all the information";
	} else {
		$teacher_id = $_POST['teacher_id'];
		$teacher_name = $_POST['teacher_name'];
		$teacher_email = $_POST['teacher_email'];
		$teacher_mobile = $_POST['teacher_mobile'];
		$teacher_role = $_POST['teacher_role'];
		$teacher_designation = $_POST['teacher_designation'];
		$office_short_name = $_POST['office'];

		try {
			// Check teacher existence
			$query = "SELECT * FROM teacher_info WHERE teacher_id=?";
			$stmt = $db->query($query, [$teacher_id], "s");
			$num_rows = $stmt->get_result()->num_rows;
			$stmt->close();
			if ($num_rows > 0) {
				$sessdata['type'] = 'danger';
				$sessdata['message'] = 'Teacher already registed.';
			} else {
				// Get office id
				$query = "SELECT * FROM office_info WHERE office_short_name=?";
				$stmt = $db->query($query, [$office_short_name], "s");
				$result = $stmt->get_result();
				$stmt->close();

				if ($result->num_rows > 0) {
					$row = $result->fetch_assoc();
					$office_id = $row['idoffice_info'];
					$office_name = $row['office_name'];
					$uniqidStr = md5(uniqid(mt_rand()));

					// Insert to user 
					$query = "INSERT INTO user_info VALUES(NULL,?,?,?,?,'Teacher',NULL,current_timestamp(),NULL,'New')";
					$stmt = $db->query($query, [$teacher_name,$teacher_email, $uniqidStr, $teacher_mobile], "ssss");
					$insertId = $stmt->insert_id;
					$stmt->close();

					if ($insertId > 0) {
						$user_id = $insertId;
						// Insert to teacher
						$query = "INSERT INTO teacher_info VALUES(NULL,?,?,?,'New',?,?)";
						$stmt = $db->query($query, 
							[$teacher_id, $teacher_designation, $teacher_role, $office_id, $user_id], "sssii");
						$insertId = $stmt->insert_id;
						$stmt->close();

						if ($insertId > 0) {
							// Send mail notification
							$linkStr = "";
							$toEmail = $teacher_email;
							$subject = 'New teacher added for '. $office_short_name;
							$message = 'You are added as a new teacher for '. $office_name .
								'<br>Please activate your account by provide this id at activate new signin.' .
								'<br>ID: ' . $uniqidStr .
								'<br/><br/>Regards,<br/>IIT Office Info Center';

							$done = FALSE;
							// $ret = sendMailByPHPMailer($toEmail, $subject, $message);
							// if ($ret['status'] === 'OK') {
								$done = TRUE;
							// }

							if ($done) {
								$sessdata['type'] = 'success';
								$sessdata['message'] = 'Successfully registered a new teacher.';
							} else {
								$sessdata['type'] = 'danger';
								$sessdata['message'] = 'Could not register teacher.';
							}
						} else {
							$sessdata['type'] = 'danger';
							$sessdata['message'] = 'Could not register teacher.';
						}
					} else {
						$sessdata['type'] = 'danger';
						$sessdata['message'] = 'Could not register new teacher.';
					}
				} else {
					$sessdata['type'] = 'danger';
					$sessdata['message'] = 'Selected department and session not found.';
				}
			}
		} catch (Exception $ex) {
			error_log($ex->getMessage());
			$sessdata['type'] = 'danger';
			$sessdata['message'] = "Registration failed";
		}
	}
	$_SESSION['admin_sessdata'] = $sessdata;
}
?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
	<!-- Navigation bar -->
	<?php require_once('navbar.php'); ?>

	<!-- Body -->
	<div class="container">
		<div class="row justify-content-center mb-3">
			<div class="mt-2 pt-2 pl-2 pr-3">
				<form class="shadow-lg rounded-xl p-5 mb-5 bg-white" action="" method="post">
					<div class="col-form-label text-center pb-4">
						<h5>Register new teacher</h5>
					</div>

					<?php
					if (checkSessionValue('admin_sessdata')) {
						$sessdata = $_SESSION['admin_sessdata'];
					?>
						<div class="mt-2 mb-2">
							<span class="text-<?php echo $sessdata['type']; ?>">
								<p class="text-center"><b><?php echo $sessdata['message']; ?></b></p>
							</span>
						</div>
					<?php
						unset($_SESSION['admin_sessdata']);
					}
					?>

					<div class="row m-1">
						<div class="form-group form-floating m-2">
							<input type="text" name="teacher_id" id="teacher_id" class="form-control" placeholder="" required autofocus>
							<label for="teacher_id">Enter teacher id</label>
						</div>

						<div class="form-group form-floating m-2">
							<input type="text" name="teacher_name" id="teacher_name" class="form-control" placeholder="" required>
							<label for="teacher_name">Enter teacher name</label>
						</div>
					</div>

					<div class="row m-1">
						<div class="form-group form-floating w-100 m-2">
							<select class="form-control custom-select" name="office" id="office" required>
								<option value="" selected disabled hidden>None</option>
								<?php
								$query = "SELECT office_short_name FROM office_info ORDER BY office_short_name";
								try {
									$stmt = $db->query($query);
									$result = $stmt->get_result();
									$stmt->close();

									if ($result->num_rows > 0) {
										while ($row = $result->fetch_row()) {
											if ($row[0] === "Registrar Office") continue;
											echo "<option value='$row[0]'>$row[0]</option>";
										}
									} else {
										echo "<option value='' selected disabled hidden>No office</option>";
									}
								} catch (Exception $ex) {
									error_log($ex->getMessage());
								}
								?>
							</select>
							<label for="office">Select department</label>
						</div>
					</div>

					<div class="row m-1">
						<div class="form-group form-floating m-2 w-100">
							<input type="email" name="teacher_email" id="teacher_email" class="form-control" placeholder="" required>
							<label for="teacher_email">Enter teacher email</label>
						</div>
					</div>

					<div class="row m-1">
						<div class="form-group form-floating w-100 m-2">
							<input type="tel" pattern="0[0-9]{10}" name="teacher_mobile" id="teacher_mobile" class="form-control" placeholder="" required>
							<label for="teacher_mobile">Enter teacher mobile number</label>
						</div>
					</div>

					<div class="row m-1">
						<div class="form-group form-floating w-45 m-2">
							<select class="form-control custom-select" name="teacher_designation" id="teacher_designation" required>
								<option value="" selected disabled hidden>None</option>
								<option value="Lecturer">Lecturer</option>
								<option value="Assistant Professor">Assistant Professor</option>
								<option value="Associate Professor">Associate Professor</option>
								<option value="Professor">Professor</option>
							</select>
							<label for="teacher_designation">Select designation</label>
						</div>

						<div class="form-group form-floating w-45 m-2">
							<select class="form-control custom-select" name="teacher_role" id="teacher_role" required>
								<option value="None" selected>None</option>
								<option value="Director">Director</option>
								<option value="Chairman">Chairman</option>
							</select>
							<label for="teacher_role">Select role</label>
						</div>
					</div>

					<div class="form-group">
						<input type="submit" name="submit" class="btn btn-primary btn-block" value="Submit">
					</div>
				</form>
			</div>
		</div>
	</div>
</body>

<?php require_once('footer.php'); ?>
