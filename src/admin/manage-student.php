<?php
$title = 'Student';
$cwd = '../';

require_once 'welcome.php';
require_once 'header.php';

$admin_id = $_SESSION['admin_id'];
$admin_name = $_SESSION['admin_name'];

?>

<body class="bg-white" id="page-top" data-spy="scroll" data-target=".fixed-top">
	<!-- Navigation bar -->
	<?php require_once('navbar.php'); ?>

	<!-- Body -->
	<div class="container">
		<div class="row">
			<div class="col mt-2 mb-4 text-justify">
				<h4 class="mb-5">Get Student List</h4>
				<form class="row" action="" method="POST">
					<div class="form-group col-3 ml-2 mr-2">
						<label for="office">Select department</label>
						<select class="form-control custom-select" name="office" id="office" required>
							<option value="" selected disabled hidden>None</option>
							<?php
							$query = "SELECT office_short_name FROM office_info ORDER BY office_short_name";
							try {
								$stmt = $db->query($query);
								$result = $stmt->get_result();
								$stmt->close();

								if ($result->num_rows > 0) {
									while ($row = $result->fetch_row()) {
										if ($row[0] === "Registrar Office") continue;
										echo "<option value='$row[0]'>$row[0]</option>";
									}
								} else {
									echo "<option value='' selected disabled hidden>No office</option>";
								}
							} catch (Exception $ex) {
								error_log($ex->getMessage());
							}
							?>
						</select>
					</div>

					<div class="form-group col-3 ml-2 mr-2">
						<label for="session">Select Session</label>
						<select class="form-control custom-select" name="session" id="session" required>
							<option value="" selected disabled hidden>Select an office first&nbsp;&nbsp;</option>
						</select>
					</div>

					<div class="form-group mt-4">
						<button type="submit" class="btn btn-primary btn-block" name="submit">Search</button>
					</div>
				</form>
			</div>
		</div>

		<div class="row">
			<div class="col mt-2 pt-2 pr-3 text-justify" id="student-info">
				<?php

				$contains_student_list = FALSE;

				if (checkIsPOSTS(['session', 'office'])) {
					$session = $_POST['session'];
					$office_name = $_POST['office'];

					$query = "SELECT * FROM student_info s, user_info u WHERE u.iduser_info=s.user_info_iduser_info AND 
							session_info_idsession_info=(SELECT idsession_info FROM session_info WHERE session_name=? AND office_info_idoffice_info=
								(SELECT idoffice_info FROM office_info WHERE office_short_name=?))
							ORDER BY CAST(SUBSTRING(student_roll, 4, 2) AS INT) DESC, CAST(SUBSTRING(student_roll, 6, 5) AS INT)";
					$stmt = $db->query($query, [$session, $office_name], "ss");
					$result = $stmt->get_result();
					$stmt->close();

					$contains_student_list = true;
				}

				if ($contains_student_list) {
				?>
					<form id="postStudentView" action="view.php" method="post">
						<input type="hidden" name="selectedStudent" id="selectedStudent">
						<table id="table-student" class='table table-bordered table-condensed table-hover text-center'>
							<thead class='thead-light'>
								<tr>
									<th>SL</th>
									<th>Student ID</th>
									<th>Student Name</th>
									<th>Student Contact Number</th>
									<th>Student Email</th>
								</tr>
							</thead>
							<tbody class="text-center">
								<?php
								if ($result->num_rows > 0) {
									$sl = 1;
									while ($row = $result->fetch_assoc()) {
								?>
										<tr class='clickable-table-row' onclick='postFileInfo(this);'>
											<td><?= $sl ?></td>
											<td><?= $row['student_roll'] ?></td>
											<td><?= ucwords(strtolower($row['user_name'])) ?></td>
											<td><?= $row['user_mobile'] ?></td>
											<td><?= $row['user_email'] ?></td>
										</tr>
										<?php
										$sl++;
										?>
									<?php
									}
									?>

									<script>
										function postFileInfo(clickedRow) {
											var filetd = $(clickedRow).find('td').eq(1).html();
											if (!(filetd.trim() === null)) {
												$('#selectedStudent').val(filetd);
												$('#postStudentView').submit();
											}
										}
									</script>
								<?php
								} else {
								?>
									<tr>
										<td colspan="5">No student</td>
									</tr>
								<?php
								}
								?>
							</tbody>
						</table>
					</form>
				<?php
				}
				?>
			</div>
		</div>
	</div>

	<script src="<?= $cwd; ?>js/jquery.min.js"></script>
	<script>
		$(document).ready(function() {
			$('#office').on('change', function() {
				let office_short_name_v = $(this).val();
				if (office_short_name_v) {
					$.ajax({
						type: "POST",
						url: "getSession.php",
						data: {
							office_short_name: office_short_name_v
						},
						success: function(html) {
							$('#session').html(html);
						}
					});
				} else {
					$('#session').html("<option value='' selected disabled hidden>None</option>");
				}
			});
		})
	</script>

	<script>
		document.addEventListener('DOMContentLoaded', function() {
			let table1 = new DataTable('#table-student');
		});
	</script>

</body>

<?php require_once('footer.php'); ?>