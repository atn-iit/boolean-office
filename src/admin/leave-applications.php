<?php
	$title = "Applications";

	$cwd = '../';
	require_once 'welcome.php';

	$admin_id = $_SESSION['admin_id'];
	$admin_name = $_SESSION['admin_name'];

	require_once 'header.php';

	if (checkIsPOSTS(['id', 'action'])) {
		$sessdata = array();
		$status = '';
		if ($_POST['action'] === 'approve') {
			$status = 'Approved';
		} else if ($_POST['action'] === 'reject') {
			$status = 'Not Approved';
		}

		if (!empty($status)) {
			// Check leave application
			$query = "SELECT * FROM leave_apply_info WHERE idleave_apply_info=? AND leave_apply_status='Created'";
			try {
				$stmt = $db->query($query, [$_POST['id']], 'i');
				$result = $stmt->get_result();
				$stmt->close();

				if ($result->num_rows > 0) {
					// Update the status
					$query = "UPDATE leave_apply_info SET leave_apply_status=? WHERE idleave_apply_info=?";
					$stmt = $db->query($query, [$status, $_POST['id']], 'si');
					$affected_rows = $stmt->affected_rows;
					$stmt->close();

					if ($affected_rows > 0) {
						$sessdata['type'] = 'success';
						$sessdata['message'] = 'Updated application status.';
					} else {
						$sessdata['type'] = 'warning';
						$sessdata['message'] = 'Can not update status.';
					}
				} else {
					$sessdata['type'] = 'warning';
					$sessdata['message'] = 'Invalid application';
				}
			} catch (Exception $ex) {
				error_log($ex->getMessage());
				$sessdata['type'] = 'danger';
				$sessdata['message'] = 'Query error!!!<br>Can not update application status.';
			}
		} else {
			$sessdata['type'] = 'warning';
			$sessdata['message'] = 'Invalid action';
		}
		$_SESSION['admin_sessdata'] = $sessdata;
	}

?>

<body class="bg-white" id="page-top" data-spy="scroll" data-target=".fixed-top">
<?php
	require_once('navbar.php');

	if (checkSessionValue('admin_sessdata')) {
		$sessdata = $_SESSION['admin_sessdata'];
		?>
		<div class="row no-gutters">
			<div class="col-lg-5 col-md-12 ml-auto">
				<div class="alert alert-<?php echo $sessdata['type']; ?> alert-dismissible fade show"
						 role="alert">
					<strong><?php echo $sessdata['message']; ?></strong>
				</div>
			</div>
		</div>
		<?php
		unset($_SESSION['admin_sessdata']);
	}
?>

<div class="container">
	<div class="row mb-3">
		<div class="col mt-2 pt-2 pr-3 text-justify">
			<div class="text-center">
				<h4>Leave Applications</h4>
			</div>
			<form id="form-applications" class="mb-5" action="" method="post">
				<input type="hidden" name="id" id="id">
				<input type="hidden" name="action" id="action">
				<table id="table-applications" class="table table-bordered table-condensed text-center">
					<thead class="thead-light">
					<tr>
						<th>Applied at</th>
						<th>Applied for</th>
						<th>From</th>
						<th>To</th>
						<th>Office</th>
						<th>Name</th>
						<th>Mobile</th>
						<th>File</th>
						<th>Leave Status</th>
						<th></th>
						<th></th>
					</tr>
					</thead>
					<tbody>
					<?php
						try {
							$query = "SELECT * FROM leave_apply_info l, leave_type_info li, teacher_info t, user_info u 
											WHERE l.teachers_info_idteachers_info=t.idteacher_info 
												AND t.user_info_iduser_info=u.iduser_info 
												AND li.idleave_type_info=l.leave_type_info_idleave_type_info 
												AND t.teacher_role!='None' ORDER BY l.leave_applied_at DESC";
							$stmt = $db->query($query);
							$result = $stmt->get_result();
							$stmt->close();
						} catch (Exception $ex) {
							error_log($ex->getMessage());
						}
						function getOfficeName($db, $id): string {
							$ret = $id . "";
							try {
								$query = "SELECT * FROM office_info WHERE idoffice_info=?";
								$stmt = $db->query($query, [$id], "i");
								$result = $stmt->get_result();
								$stmt->close();

								$row = $result->fetch_assoc();
								$ret = $row['office_short_name'];
							} catch (Exception $ex) {
								error_log($ex);
							}
							return $ret;
						}
						if ($result->num_rows > 0) {
						while ($row = $result->fetch_assoc()) { ?>
							<tr>
								<td><?= $row['leave_applied_at'] ?></td>
								<td><?= $row['leave_type_name'] ?></td>
								<td><?= $row['leave_apply_from'] ?></td>
								<td><?= $row['leave_apply_to'] ?></td>
								<td><?= getOfficeName($db, $row['office_info_idoffice_info']) ?></td>
								<td><?= $row['user_name'] ?></td>
								<td><?= $row['user_mobile'] ?></td>
								<td><a href="uploads/applications/<?= $row['leave_apply_pdf'];?>" target="blank"><img
											src="../res/pdf.png" width="32px" alt="pdf"></a></td>
								<td><?= $row['leave_apply_status'] === "Created" ? "Pending" : $row['leave_apply_status']  ?></td>
								<?php
									if (($row['leave_apply_status'] !== 'Approved' && $row['leave_apply_status'] !== 'Not Approved')) {
										?>
										<td>
											<button type='button' class='btn bg-primary text-white'
															onclick="postApplicationInfo(<?= '\'' . $row['idleave_apply_info'] . '\',\'approve\''; ?>,this)">
												Approve
											</button>
										</td>
										<td>
											<button type='button' class='btn bg-danger text-white'
															onclick="postApplicationInfo(<?= '\'' . $row['idleave_apply_info'] . '\',\'reject\''; ?>,this)">
												Reject
											</button>
										</td>
									<?php
									} else { 
										echo "<td colspan='2'></td>";
									}
									?>
							</tr>
						<?php } ?>

							<script>
								function postApplicationInfo(id, action, item) {
									if (id.trim() !== null && action.trim() !== null) {
										if (action.trim() === "reject") {
											if (!confirm('Are you sure?')) {
												item.preventDefault();
												return;
											}
										}
										$('#id').val(id);
										$('#action').val(action);
										$('#form-applications').submit();
									}
								}
							</script>
						<?php } else { ?>
							<tr><td colspan="11">No applications</td></tr>
						<?php
						}
					?>
					</tbody>
				</table>
			</form>
		</div>
	</div>
</div>
</body>

<script>
	document.addEventListener('DOMContentLoaded', function () {
		let table1 = new DataTable('#table-applications', {"order": [[0, "desc"]]});
	});
</script>

<?php require('footer.php'); ?>
