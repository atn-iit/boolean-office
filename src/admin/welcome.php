<?php
	if (basename($_SERVER['PHP_SELF']) === basename(__FILE__)) {
		header($_SERVER["SERVER_PROTOCOL"] . " 403 Forbidden");
		exit('Direct access not allowed');
	}

	session_start();

	if (!isset($cwd)) {
		$cwd = '';
	}

	require_once $cwd . 'custom-functions.php';
	require_once 'check-signin.php';
	require_once $cwd . 'dbconn.php';

	// If session value found
	$isSignPage = (isset($title) and !empty($title) and $title === 'SignIn');
	if (checkIsSignIn()) {
		if ($isSignPage) {
			header('Location: index.php');
		}
	} else {
		if ($isSignPage) {
			return;
		}

		header('Location: signin.php');
	}
