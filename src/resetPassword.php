<?php

	$title = "Reset Password";
	require_once 'header.php';

	session_start();

	$sessData = !empty($_SESSION['sessData']) ? $_SESSION['sessData'] : '';
	if (!empty($sessData['status']['msg'])) {
		$statusMsg = $sessData['status']['msg'];
		$statusMsgType = $sessData['status']['type'];
		unset($_SESSION['sessData']['status']);
	}
?>

	<body>
	<div class="container">
		<div class="row justify-content-center mb-3">
			<div class="col-md-5 mt-4 pt-4 pl-2 pr-3 text-justify">
				<form class="shadow-lg rounded-xl p-5 mb-5 bg-white" action="userAccount.php" method="POST">
					<div class="col-form-label text-center pb-4">
						<h4>Reset Your Password</h4>
					</div>
					<div class="form-group form-floating">
						<input type="password" name="user_password" id="user_password" class="form-control"
									 placeholder="New Password" autofocus required>
						<label for="user_password">Enter new password</label>
					</div>
					<div class="form-group form-floating">
						<input type="password" name="user_confirm_password" id="user_confirm_password" class="form-control"
									 placeholder="Confirm Password" required>
						<label for="user_confirm_password">Confirm password</label>
					</div>
					<div class="form-group form-floating">
						<input type="text" class="form-control" id="reset_code" name="reset_code" placeholder="Reset Code"
									 maxlength="32" required>
						<label for="reset_code">Enter Reset Code</label>
					</div>
					<?php if (!empty($statusMsg)) { ?>
						<span class="small text-break text-<?php echo ($statusMsgType === 'error') ? 'danger' : 'success'; ?>">
						<?php echo $statusMsg; ?>
					</span>
					<?php } ?>

					<input type="submit" name="resetSubmit" class="btn btn-primary btn-block" value="Continue">
				</form>
			</div>
		</div>
	</div>
	</body>

	<script>
		let password = document.getElementById("user_password"),
			confirm_password = document.getElementById("user_confirm_password");

		function validatePassword() {
			if (password.value !== confirm_password.value) {
				confirm_password.setCustomValidity("Passwords Don't Match");
			} else {
				confirm_password.setCustomValidity('');
			}
		}

		password.onchange = validatePassword;
		confirm_password.onkeyup = validatePassword;
	</script>

<?php require_once('footer.php'); ?>