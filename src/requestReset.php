<?php

	$title = "Request Reset Password";
	require_once 'header.php';
?>

	<body>
	<div class="container">
		<div class="row justify-content-center mb-3">
			<div class="col-md-4 mt-4 pt-4 pl-2 pr-3 text-justify">
				<form class="shadow-lg rounded-xl p-5 mb-5 bg-white" action="userAccount.php" method="POST">
					<div class="col-form-label text-center pb-4">
						<h4>Enter the Email of Your Account to<br>Reset New Password</h4>
					</div>
					<div class="form-group form-floating">
						<input type="text" class="form-control" id="user_email" name="user_email" placeholder="Enter User Email"
									 autofocus required>
						<label for="user_email">User email</label>
					</div>
					<input type="submit" name="forgotSubmit" class="btn btn-primary btn-block" value="Continue">
				</form>
			</div>
		</div>
	</div>
	</body>

<?php require_once('footer.php'); ?>