<?php
	$title = "Home";
	$cwd = '../';
	require_once $cwd . 'welcome.php';
	require_once '../dbconn.php';
	require_once 'header.php';
?>

	<body class="bg-white" id="page-top" data-spy="scroll" data-target=".fixed-top">
	<?php
		require_once 'navbar.php';

		function getOfficeName($db, $id): string
		{
			$ret = $id . "";
			try {
				$query = "SELECT * FROM office_info WHERE idoffice_info=?";
				$stmt = $db->query($query, [$id], "i");
				$result = $stmt->get_result();
				$stmt->close();

				$row = $result->fetch_assoc();
				$ret = $row['office_short_name'];
			} catch (Exception $ex) {
				error_log($ex);
			}
			return $ret;
		}

	?>

	<div class="container">
		<div class="row justify-content-center">
			<div class="mt-1 pt-1 pl-2 pr-3">
				<div class="row">
					<h1 class="col text-center"><b>Welcome to our Library</b></h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col m-2 p-2 mt-4 pt-4">
				<h4>Published Announcements</h4>
			</div>
		</div>
		<div class="row">
			<div class="col mt-2 mb-4 pb-3">
				<table class="table table-condensed text-center">
					<thead>
					<tr>
						<th>Created at</th>
						<th>Message</th>
						<th>Announcement from</th>
						<th>Status</th>
					</tr>
					</thead>
					<tbody>
					<?php
						$limit_count = 10;

						function do_query($sql, $status)
						{
							global $db;
							$current_date = date('Y-m-d');

							$count = 0;
							try {
								$stmt = $db->query($sql, [$current_date], '');
								$result = $stmt->get_result();
								$count = $result->num_rows;
								if ($result->num_rows > 0) {
									while ($row = $result->fetch_assoc()) {
										?>
										<tr>
											<td><?= $row['news_added']; ?></td>
											<td><?= $row['news_message']; ?></td>
											<td><?= getOfficeName($db, $row['officer_info_office_info_idoffice_info']); ?></td>
											<td><?= $status ?></td>
										</tr>
										<?php
									}
								}
							} catch (Exception $ex) {
								throw new Exception($ex->getMessage());
							}
							return $count;
						}

						$sql = "SELECT * FROM news_info WHERE news_valid_for>=? ORDER BY news_added DESC LIMIT $limit_count;";
						try {
							// Fetch new news
							$data_count = do_query($sql, 'New');
							$limit_count -= $data_count;

							if ($limit_count > 0) { // Fetch some outdated news
								$sql = "SELECT * FROM news_info WHERE news_valid_for<? ORDER BY news_added DESC LIMIT $limit_count;";
								$limit_count -= do_query($sql, 'Outdated');
							}

							if ($data_count === 0 && $limit_count === 10) {
								echo "<tr><td colspan='3'>No announchment</td></tr>";
							}
						} catch (Exception $ex) {
							error_log($ex->getMessage());
							echo "<tr><td colspan='3'>Error!!!</td></tr>";
						}
					?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	</body>

<?php
	require_once 'footer.php';
?>