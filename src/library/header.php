<?php
	if (basename($_SERVER['PHP_SELF']) === basename(__FILE__)) {
		header($_SERVER["SERVER_PROTOCOL"] . " 403 Forbidden");
		exit('Direct access not allowed');
	}
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">

	<!-- Metadata -->
	<meta name="application-name" content="Boolean Office - Online library management system">
	<meta name="keywords" content="IIT,Office,Automation,Library Management">

	<!-- Page resizing -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Page icon -->
	<link rel="icon" href="../res/nstu.png" type="image/png">
	<link rel="shortcut icon" href="../res/favicon.ico" type="image/jpg"/>

	<!-- Bootstrap core 4.4.1 -->
	<link rel="stylesheet" href="../css/bootstrap.min.css">

	<!-- Custom stylesheet -->
	<link rel="stylesheet" href="../css/style.css">

	<title><?php if (isset($title)) echo $title . ' | '; ?>Online Library Management System</title>
</head>
